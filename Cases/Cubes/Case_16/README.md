# $`\overleftarrow{R}_{21}J_{32}J_{34}\overleftarrow{R}_{41}J_{26}\overleftarrow{R}_{37}\overrightarrow{R}_{15}J_{48}J_{65}\overrightarrow{R}_{76}\overleftarrow{S}_{78}J_{85}`$

<table>
    <tbody>
        <tr>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
            <td align="center"> <img src="four_outflow_r_two_inflow_s_outflow_r_orbit.ogv">
            </td>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
        </tr>
    </tbody>
</table>

## Equations

Wave | Wave relation | Density relation | x-Velocities | y-Velocities | z-Velocities | Pressure relation
:----|:--------------|:-----------------|:-------------|:-------------|:-------------|:------------------
$`\overleftarrow{R}_{21}`$ | $`u_1 - u_2 = \Phi_{21}`$ | $`\frac{p_2}{p_1} = \left( \frac{\rho_2}{\rho_1} \right)^\gamma`$ | $`u_2 < u_1`$ | $`v_2 = v_1`$ | $`w_2 = w_1`$ | $`p_2 > p_1`$
$`J_{32}`$ | | $`\rho_3 <=> \rho_2`$ | $`u_3 <=> u_2`$ | $`v_3 = v_2`$ | $`w_3 <=> w_2`$ | $`p_3 = p_2`$
$`J_{34}`$ | | $`\rho_3 <=> \rho_4`$ | $`u_3 = u_4`$ | $`v_3 <=> v_4`$ | $`w_3 <=> w_4`$ | $`p_3 = p_4`$
$`\overleftarrow{R}_{41}`$ | $`v_1 - v_4 = \Phi_{41}`$ | $`\frac{p_4}{p_1} = \left( \frac{\rho_4}{\rho_1} \right)^\gamma`$ | $`u_4 = u_1`$ | $`v_4 < v_1`$ | $`w_4 = w_1`$ | $`p_4 > p_1`$
$`J_{26}`$ | | $`\rho_2 <=> \rho_6`$ | $`u_2 <=> u_6`$ | $`v_2 <=> v_6`$ | $`w_2 = w_6`$ | $`p_2 = p_6`$
$`\overleftarrow{R}_{37}`$ | $`w_7 - w_3 = \Phi_{37}`$ | $`\frac{p_3}{p_7} = \left( \frac{\rho_3}{\rho_7} \right)^\gamma`$ | $`u_3 = u_7`$ | $`v_3 = v_7`$ | $`w_3 < w_7`$ | $`p_3 > p_7`$
$`\overrightarrow{R}_{15}`$ | $`w_1 - w_5 = \Phi_{15}`$ | $`\frac{p_1}{p_5} = \left( \frac{\rho_1}{\rho_5} \right)^\gamma`$ | $`u_1 = u_5`$ | $`v_1 = v_5`$ | $`w_1 < w_5`$ | $`p_1 < p_5`$
$`J_{48}`$ | | $`\rho_4 <=> \rho_8`$ | $`u_4 <=> u_8`$ | $`v_4 <=> v_8`$ | $`w_4 = w_8`$ | $`p_4 = p_8`$
$`J_{65}`$ | | $`\rho_6 <=> \rho_5`$ | $`u_6 = u_5`$ | $`v_6 <=> v_5`$ | $`w_6 <=> w_5`$ | $`p_6 = p_5`$
$`\overrightarrow{R}_{76}`$ | $`v_7 - v_6 = \Phi_{76}`$ | $`\frac{p_7}{p_6} = \left( \frac{\rho_7}{\rho_6} \right)^\gamma`$ | $`u_7 = u_6`$ | $`v_7 < v_6`$ | $`w_7 = w_6`$ | $`p_7 < p_6`$
$`\overleftarrow{S}_{78}`$ | $`u_7 - u_8 = \Psi_{78}`$ | $`\frac{\rho_7}{\rho_8} = \Pi_{78}`$ | $`u_7 > u_8`$ | $`v_7 = v_8`$ | $`w_7 = w_8`$ | $`p_7 < p_8`$
$`J_{85}`$ | | $`\rho_8 <=> \rho_5`$ | $`u_8 <=> u_5`$ | $`v_8 = v_5`$ | $`w_8 <=> w_5`$ | $`p_8 = p_5`$


![](four_outflow_r_two_inflow_s_outflow_r_slice_+1_+1_+1.ogv) | ![](four_outflow_r_two_inflow_s_outflow_r_slice_+1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

![](four_outflow_r_two_inflow_s_outflow_r_slice_-1_+1_+1.ogv) | ![](four_outflow_r_two_inflow_s_outflow_r_slice_-1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

## Calculation

The calculation of a possible set of initial states for this case can be found [here](four_outflow_r_two_inflow_s_outflow_r.pdf).

## Case setup
We run the case using a three-dimensional ALPACA and [inputfile](four_outflow_r_two_inflow_s_outflow_r.xml) provided in this folder.

## Final State
![](four_outflow_r_two_inflow_s_outflow_r_cube_+1_+1_+1.png) | ![](four_outflow_r_two_inflow_s_outflow_r_cube_+1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](four_outflow_r_two_inflow_s_outflow_r_cube_+1_-1_+1.png) | ![](four_outflow_r_two_inflow_s_outflow_r_cube_+1_-1_-1.png)
:------------------------------------:|:------------------------------------:

![](four_outflow_r_two_inflow_s_outflow_r_cube_-1_+1_+1.png) | ![](four_outflow_r_two_inflow_s_outflow_r_cube_-1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](four_outflow_r_two_inflow_s_outflow_r_cube_-1_-1_+1.png) | ![](four_outflow_r_two_inflow_s_outflow_r_cube_-1_-1_-1.png)
:------------------------------------:|:------------------------------------:
