# $`\overleftarrow{S}_{21}\overleftarrow{S}_{32}\overleftarrow{S}_{34}\overleftarrow{S}_{41}\overleftarrow{S}_{26}\overleftarrow{S}_{37}\overrightarrow{S}_{15}\overleftarrow{S}_{48}\overrightarrow{S}_{65}\overleftarrow{S}_{76}\overleftarrow{S}_{78}\overrightarrow{S}_{85}`$

<table>
    <tbody>
        <tr>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
            <td align="center"> <img src="three_allleft_s_three_leftright_s_6l2rl1lr_orbit.ogv">
            </td>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
        </tr>
    </tbody>
</table>

## Equations

Wave | Wave relation | Density relation | x-Velocities | y-Velocities | z-Velocities | Pressure relation
:----|:--------------|:-----------------|:-------------|:-------------|:-------------|:------------------
$`\overleftarrow{S}_{21}`$ | $`u_2 - u_1 = \Psi_{21}`$ | $`\frac{\rho_2}{\rho_1} = \Pi_{21}`$ | $`u_2 > u_1`$ | $`v_2 = v_1`$ | $`w_2 = w_1`$ | $`p_2 < p_1`$
$`\overleftarrow{S}_{32}`$ | $`v_3 - v_2 = \Psi_{32}`$ | $`\frac{\rho_3}{\rho_2} = \Pi_{32}`$ | $`u_3 = u_2`$ | $`v_3 > v_2`$ | $`w_3 = w_2`$ | $`p_3 < p_2`$
$`\overleftarrow{S}_{34}`$ | $`u_3 - u_4 = \Psi_{34}`$ | $`\frac{\rho_3}{\rho_4} = \Pi_{34}`$ | $`u_3 > u_4`$ | $`v_3 = v_4`$ | $`w_3 = w_4`$ | $`p_3 < p_4`$
$`\overleftarrow{S}_{41}`$ | $`v_4 - v_1 = \Psi_{41}`$ | $`\frac{\rho_4}{\rho_1} = \Pi_{41}`$ | $`u_4 = u_1`$ | $`v_4 > v_1`$ | $`w_4 = w_1`$ | $`p_4 < p_1`$
$`\overleftarrow{S}_{26}`$ | $`w_2 - w_6 = \Psi_{26}`$ | $`\frac{\rho_2}{\rho_6} = \Pi_{26}`$ | $`u_2 = u_6`$ | $`v_2 = v_6`$ | $`w_2 > w_6`$ | $`p_2 < p_6`$
$`\overleftarrow{S}_{37}`$ | $`w_3 - w_7 = \Psi_{37}`$ | $`\frac{\rho_3}{\rho_7} = \Pi_{37}`$ | $`u_3 = u_7`$ | $`v_3 = v_7`$ | $`w_3 > w_7`$ | $`p_3 < p_7`$
$`\overrightarrow{S}_{15}`$ | $`w_1 - w_5 = \Psi_{15}`$ | $`\frac{\rho_1}{\rho_5} = \Pi_{15}`$ | $`u_1 = u_5`$ | $`v_1 = v_5`$ | $`w_1 > w_5`$ | $`p_1 > p_5`$
$`\overleftarrow{S}_{48}`$ | $`w_4 - w_8 = \Psi_{48}`$ | $`\frac{\rho_4}{\rho_8} = \Pi_{48}`$ | $`u_4 = u_8`$ | $`v_4 = v_8`$ | $`w_4 > w_8`$ | $`p_4 < p_8`$
$`\overrightarrow{S}_{65}`$ | $`u_6 - u_5 = \Psi_{65}`$ | $`\frac{\rho_6}{\rho_5} = \Pi_{65}`$ | $`u_6 > u_5`$ | $`v_6 = v_5`$ | $`w_6 = w_5`$ | $`p_6 > p_5`$
$`\overleftarrow{S}_{76}`$ | $`v_7 - v_6 = \Psi_{76}`$ | $`\frac{\rho_7}{\rho_6} = \Pi_{76}`$ | $`u_7 = u_6`$ | $`v_7 > v_6`$ | $`w_7 = w_6`$ | $`p_7 < p_6`$
$`\overleftarrow{S}_{78}`$ | $`u_7 - u_8 = \Psi_{78}`$ | $`\frac{\rho_7}{\rho_8} = \Pi_{78}`$ | $`u_7 > u_8`$ | $`v_7 = v_8`$ | $`w_7 = w_8`$ | $`p_7 < p_8`$
$`\overrightarrow{S}_{85}`$ | $`v_8 - v_5 = \Psi_{85}`$ | $`\frac{\rho_8}{\rho_5} = \Pi_{85}`$ | $`u_8 = u_5`$ | $`v_8 > v_5`$ | $`w_8 = w_5`$ | $`p_8 > p_5`$


![](three_allleft_s_three_leftright_s_6l2rl1lr_slice_+1_+1_+1.ogv) | ![](three_allleft_s_three_leftright_s_6l2rl1lr_slice_+1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

![](three_allleft_s_three_leftright_s_6l2rl1lr_slice_-1_+1_+1.ogv) | ![](three_allleft_s_three_leftright_s_6l2rl1lr_slice_-1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

## Calculation

The calculation of a possible set of initial states for this case can be found [here](three_allleft_s_three_leftright_s_6l2rl1lr.pdf).

## Case setup
We run the case using a three-dimensional ALPACA and [inputfile](three_allleft_s_three_leftright_s_6l2rl1lr.xml) provided in this folder.

## Final State
![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_+1_+1_+1.png) | ![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_+1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_+1_-1_+1.png) | ![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_+1_-1_-1.png)
:------------------------------------:|:------------------------------------:

![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_-1_+1_+1.png) | ![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_-1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_-1_-1_+1.png) | ![](three_allleft_s_three_leftright_s_6l2rl1lr_cube_-1_-1_-1.png)
:------------------------------------:|:------------------------------------:
