# $`J_{21}J_{32}J_{34}J_{41}J_{26}J_{37}J_{15}J_{48}J_{65}J_{76}J_{78}J_{85}`$

<table>
    <tbody>
        <tr>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </td>
            <td align="center"> <img src="six_allj_all_clockwise_orbit.ogv">
            </td>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </td>
        </tr>
    </tbody>
</table>

## Equations

Wave | Wave relation | Density relation | x-Velocities | y-Velocities | z-Velocities | Pressure relation
:----|:--------------|:-----------------|:-------------|:-------------|:-------------|:------------------
$`J_{21}`$ | | $`\rho_2 <=> \rho_1`$ | $`u_2 = u_1`$ | $`v_2 <=> v_1`$ | $`w_2 <=> w_1`$ | $`p_2 = p_1`$
$`J_{32}`$ | | $`\rho_3 <=> \rho_2`$ | $`u_3 <=> u_2`$ | $`v_3 = v_2`$ | $`w_3 <=> w_2`$ | $`p_3 = p_2`$
$`J_{34}`$ | | $`\rho_3 <=> \rho_4`$ | $`u_3 = u_4`$ | $`v_3 <=> v_4`$ | $`w_3 <=> w_4`$ | $`p_3 = p_4`$
$`J_{41}`$ | | $`\rho_4 <=> \rho_1`$ | $`u_4 <=> u_1`$ | $`v_4 = v_1`$ | $`w_4 <=> w_1`$ | $`p_4 = p_1`$
$`J_{26}`$ | | $`\rho_2 <=> \rho_6`$ | $`u_2 <=> u_6`$ | $`v_2 <=> v_6`$ | $`w_2 = w_6`$ | $`p_2 = p_6`$
$`J_{37}`$ | | $`\rho_3 <=> \rho_7`$ | $`u_3 <=> u_7`$ | $`v_3 <=> v_7`$ | $`w_3 = w_7`$ | $`p_3 = p_7`$
$`J_{15}`$ | | $`\rho_1 <=> \rho_5`$ | $`u_1 <=> u_5`$ | $`v_1 <=> v_5`$ | $`w_1 = w_5`$ | $`p_1 = p_5`$
$`J_{48}`$ | | $`\rho_4 <=> \rho_8`$ | $`u_4 <=> u_8`$ | $`v_4 <=> v_8`$ | $`w_4 = w_8`$ | $`p_4 = p_8`$
$`J_{65}`$ | | $`\rho_6 <=> \rho_5`$ | $`u_6 = u_5`$ | $`v_6 <=> v_5`$ | $`w_6 <=> w_5`$ | $`p_6 = p_5`$
$`J_{76}`$ | | $`\rho_7 <=> \rho_6`$ | $`u_7 <=> u_6`$ | $`v_7 = v_6`$ | $`w_7 <=> w_6`$ | $`p_7 = p_6`$
$`J_{78}`$ | | $`\rho_7 <=> \rho_8`$ | $`u_7 = u_8`$ | $`v_7 <=> v_8`$ | $`w_7 <=> w_8`$ | $`p_7 = p_8`$
$`J_{85}`$ | | $`\rho_8 <=> \rho_5`$ | $`u_8 <=> u_5`$ | $`v_8 = v_5`$ | $`w_8 <=> w_5`$ | $`p_8 = p_5`$


![](six_allj_all_clockwise_slice_+1_+1_+1.ogv) | ![](six_allj_all_clockwise_slice_+1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

![](six_allj_all_clockwise_slice_-1_+1_+1.ogv) | ![](six_allj_all_clockwise_slice_-1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

## Calculation

The calculation of a possible set of initial states for this case can be found [here](six_allj_all_clockwise.pdf).

## Case setup
We run the case using a three-dimensional ALPACA and [inputfile](six_allj_all_clockwise.xml) provided in this folder.

## Final State
![](six_allj_all_clockwise_cube_+1_+1_+1.png) | ![](six_allj_all_clockwise_cube_+1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](six_allj_all_clockwise_cube_+1_-1_+1.png) | ![](six_allj_all_clockwise_cube_+1_-1_-1.png)
:------------------------------------:|:------------------------------------:

![](six_allj_all_clockwise_cube_-1_+1_+1.png) | ![](six_allj_all_clockwise_cube_-1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](six_allj_all_clockwise_cube_-1_-1_+1.png) | ![](six_allj_all_clockwise_cube_-1_-1_-1.png)
:------------------------------------:|:------------------------------------:
