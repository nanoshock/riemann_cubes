# $`\overrightarrow{S}_{21}\overleftarrow{S}_{32}\overleftarrow{S}_{34}\overrightarrow{S}_{41}\overleftarrow{R}_{26}\overrightarrow{R}_{37}\overleftarrow{R}_{15}\overrightarrow{R}_{48}\overleftarrow{S}_{65}\overrightarrow{S}_{76}\overrightarrow{S}_{78}\overleftarrow{S}_{85}`$
<table>
    <tbody>
        <tr>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
            <td align="center"> <img src="four_leftright_r_s_two_leftright_s_orbit.ogv">
            </td>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
        </tr>
    </tbody>
</table>

## Equations

Wave | Wave relation | x-Velocities | y-Velocities | z-Velocities | Pressure relation
:----|:--------------|:-------------|:-------------|:-------------|:------------------
$`\overrightarrow{S}_{21}`$ | $`u_2 - u_1 = \Psi_{21}`$ | $`u_2 > u_1`$ | $`v_2 = v_1`$ | $`w_2 = w_1`$ | $`p_2 > p_1`$
$`\overleftarrow{S}_{32}`$ | $`v_3 - v_2 = \Psi_{32}`$ | $`u_3 = u_2`$ | $`v_3 > v_2`$ | $`w_3 = w_2`$ | $`p_3 < p_2`$
$`\overleftarrow{S}_{34}`$ | $`u_3 - u_4 = \Psi_{34}`$ | $`u_3 > u_4`$ | $`v_3 = v_4`$ | $`w_3 = w_4`$ | $`p_3 < p_4`$
$`\overrightarrow{S}_{41}`$ | $`v_4 - v_1 = \Psi_{41}`$ | $`u_4 = u_1`$ | $`v_4 > v_1`$ | $`w_4 = w_1`$ | $`p_4 > p_1`$
$`\overleftarrow{R}_{26}`$ | $`w_6 - w_2 = \Phi_{26}`$ | $`u_2 = u_6`$ | $`v_2 = v_6`$ | $`w_2 < w_6`$ | $`p_2 > p_6`$
$`\overrightarrow{R}_{37}`$ | $`w_3 - w_7 = \Phi_{37}`$ | $`u_3 = u_7`$ | $`v_3 = v_7`$ | $`w_3 < w_7`$ | $`p_3 < p_7`$
$`\overrightarrow{R}_{15}`$ | $`w_1 - w_5 = \Phi_{15}`$ | $`u_1 = u_5`$  | $`v_1 = v_5`$ | $`w_1 < w_5`$ | $`p_1 < p_5`$
$`\overleftarrow{R}_{48}`$ | $`w_8 - w_4 = \Phi_{48}`$ | $`u_4 = u_8`$ | $`v_4 = v_8`$ | $`w_4 < w_8`$ | $`p_4 > p_8`$
$`\overleftarrow{S}_{65}`$ | $`u_6 - u_5 = \Psi_{65}`$ | $`u_6 > u_5`$ | $`v_6 = v_5`$ | $`w_6 = w_5`$ | $`p_6 < p_5`$
$`\overrightarrow{S}_{76}`$ | $`v_7 - v_6 = \Psi_{76}`$ | $`u_7 = u_6`$ | $`v_7 > v_6`$ | $`w_7 = w_6`$ | $`p_7 > p_6`$
$`\overrightarrow{S}_{78}`$ | $`u_7 - u_8 = \Psi_{78}`$ | $`u_7 > u_8`$ | $`v_7 = v_8`$ | $`w_7 = w_8`$ | $`p_7 > p_8`$
$`\overleftarrow{S}_{85}`$ | $`v_8 - v_5 = \Psi_{85}`$ | $`u_8 = u_5`$ | $`v_8 > v_5`$ | $`w_8 = w_5`$ | $`p_8 < p_5`$

All pressures and densities are related via the isentropic equation of state, $`p_i = K \rho_i^\gamma`$, for $`i \in [1,8]`$. $`K`$ is a constant which we set to $`K=1`$.

![](four_leftright_r_s_two_leftright_s_slice_+1_+1_+1.ogv) | ![](four_leftright_r_s_two_leftright_s_slice_+1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

![](four_leftright_r_s_two_leftright_s_slice_-1_+1_+1.ogv) | ![](four_leftright_r_s_two_leftright_s_slice_-1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

## Calculation

The calculation of a possible set of initial states for this case can be found [here](four_leftright_r_s_two_leftright_s.pdf).

## Case setup
We run the case using a three-dimensional ALPACA and [inputfile](four_leftright_r_s_two_leftright_s.xml) provided in this folder.

## Final State
![](four_leftright_r_s_two_leftright_s_cube_+1_+1_+1.png) | ![](four_leftright_r_s_two_leftright_s_cube_+1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](four_leftright_r_s_two_leftright_s_cube_+1_-1_+1.png) | ![](four_leftright_r_s_two_leftright_s_cube_+1_-1_-1.png)
:------------------------------------:|:------------------------------------:

![](four_leftright_r_s_two_leftright_s_cube_-1_+1_+1.png) | ![](four_leftright_r_s_two_leftright_s_cube_-1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](four_leftright_r_s_two_leftright_s_cube_-1_-1_+1.png) | ![](four_leftright_r_s_two_leftright_s_cube_-1_-1_-1.png)
:------------------------------------:|:------------------------------------:
