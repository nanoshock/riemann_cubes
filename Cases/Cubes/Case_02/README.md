# $`\overleftarrow{R}_{21}\overrightarrow{R}_{32}\overrightarrow{R}_{34}\overleftarrow{R}_{41}\overleftarrow{R}_{26}\overrightarrow{R}_{37}\overrightarrow{R}_{15}\overleftarrow{R}_{48}\overrightarrow{R}_{65}\overleftarrow{R}_{76}\overleftarrow{R}_{78}\overrightarrow{R}_{85}`$

<table>
    <tbody>
        <tr>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
            <td align="center"> <img src="six_leftright_r_orbit.ogv">
            </td>
            <td align="center">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span>&nbsp;&nbsp;</span>
            </td>
        </tr>
    </tbody>
</table>

## Equations

Wave | Wave relation | Density relation | x-Velocities | y-Velocities | z-Velocities | Pressure relation
:----|:--------------|:-----------------|:-------------|:-------------|:-------------|:------------------
$`\overleftarrow{R}_{21}`$ | $`u_1 - u_2 = \Phi_{21}`$ | $`\frac{p_2}{p_1} = \left( \frac{\rho_2}{\rho_1} \right)^\gamma`$ | $`u_2 < u_1`$ | $`v_2 = v_1`$ | $`w_2 = w_1`$ | $`p_2 > p_1`$
$`\overrightarrow{R}_{32}`$ | $`v_3 - v_2 = \Phi_{32}`$ | $`\frac{p_3}{p_2} = \left( \frac{\rho_3}{\rho_2} \right)^\gamma`$ | $`u_3 = u_2`$ | $`v_3 < v_2`$ | $`w_3 = w_2`$ | $`p_3 < p_2`$
$`\overrightarrow{R}_{34}`$ | $`u_3 - u_4 = \Phi_{34}`$ | $`\frac{p_3}{p_4} = \left( \frac{\rho_3}{\rho_4} \right)^\gamma`$ | $`u_3 < u_4`$ | $`v_3 = v_4`$ | $`w_3 = w_4`$ | $`p_3 < p_4`$
$`\overleftarrow{R}_{41}`$ | $`v_1 - v_4 = \Phi_{41}`$ | $`\frac{p_4}{p_1} = \left( \frac{\rho_4}{\rho_1} \right)^\gamma`$ | $`u_4 = u_1`$ | $`v_4 < v_1`$ | $`w_4 = w_1`$ | $`p_4 > p_1`$
$`\overleftarrow{R}_{26}`$ | $`w_6 - w_2 = \Phi_{26}`$ | $`\frac{p_2}{p_6} = \left( \frac{\rho_2}{\rho_6} \right)^\gamma`$ | $`u_2 = u_6`$ | $`v_2 = v_6`$ | $`w_2 < w_6`$ | $`p_2 > p_6`$
$`\overrightarrow{R}_{37}`$ | $`w_3 - w_7 = \Phi_{37}`$ | $`\frac{p_3}{p_7} = \left( \frac{\rho_3}{\rho_7} \right)^\gamma`$ | $`u_3 = u_7`$ | $`v_3 = v_7`$ | $`w_3 < w_7`$ | $`p_3 < p_7`$
$`\overrightarrow{R}_{15}`$ | $`w_1 - w_5 = \Phi_{15}`$ | $`\frac{p_1}{p_5} = \left( \frac{\rho_1}{\rho_5} \right)^\gamma`$ | $`u_1 = u_5`$ | $`v_1 = v_5`$ | $`w_1 < w_5`$ | $`p_1 < p_5`$
$`\overleftarrow{R}_{48}`$ | $`w_8 - w_4 = \Phi_{48}`$ | $`\frac{p_4}{p_8} = \left( \frac{\rho_4}{\rho_8} \right)^\gamma`$ | $`u_4 = u_8`$ | $`v_4 = v_8`$ | $`w_4 < w_8`$ | $`p_4 > p_8`$
$`\overrightarrow{R}_{65}`$ | $`u_6 - u_5 = \Phi_{65}`$ | $`\frac{p_6}{p_5} = \left( \frac{\rho_6}{\rho_5} \right)^\gamma`$ | $`u_6 < u_5`$ | $`v_6 = v_5`$ | $`w_6 = w_5`$ | $`p_6 < p_5`$
$`\overleftarrow{R}_{76}`$ | $`v_6 - v_7 = \Phi_{76}`$ | $`\frac{p_7}{p_6} = \left( \frac{\rho_7}{\rho_6} \right)^\gamma`$ | $`u_7 = u_6`$ | $`v_7 < v_6`$ | $`w_7 = w_6`$ | $`p_7 > p_6`$
$`\overleftarrow{R}_{78}`$ | $`u_8 - u_7 = \Phi_{78}`$ | $`\frac{p_7}{p_8} = \left( \frac{\rho_7}{\rho_8} \right)^\gamma`$ | $`u_7 < u_8`$ | $`v_7 = v_8`$ | $`w_7 = w_8`$ | $`p_7 > p_8`$
$`\overrightarrow{R}_{85}`$ | $`v_8 - v_5 = \Phi_{85}`$ | $`\frac{p_8}{p_5} = \left( \frac{\rho_8}{\rho_5} \right)^\gamma`$ | $`u_8 = u_5`$ | $`v_8 < v_5`$ | $`w_8 = w_5`$ | $`p_8 < p_5`$


![](six_leftright_r_slice_+1_+1_+1.ogv) | ![](six_leftright_r_slice_+1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

![](six_leftright_r_slice_-1_+1_+1.ogv) | ![](six_leftright_r_slice_-1_-1_+1.ogv)
:-------------------------------------:|:--------------------------------------:

## Calculation

The calculation of a possible set of initial states for this case can be found [here](six_leftright_r.pdf).

## Case setup
We run the case using a three-dimensional ALPACA and [inputfile](six_leftright_r.xml) provided in this folder.

## Final State
![](six_leftright_r_cube_+1_+1_+1.png) | ![](six_leftright_r_cube_+1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](six_leftright_r_cube_+1_-1_+1.png) | ![](six_leftright_r_cube_+1_-1_-1.png)
:------------------------------------:|:------------------------------------:

![](six_leftright_r_cube_-1_+1_+1.png) | ![](six_leftright_r_cube_-1_+1_-1.png)
:------------------------------------:|:------------------------------------:

![](six_leftright_r_cube_-1_-1_+1.png) | ![](six_leftright_r_cube_-1_-1_-1.png)
:------------------------------------:|:------------------------------------:
