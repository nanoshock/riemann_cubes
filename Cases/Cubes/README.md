# List of all cube cases

| | polytropic cases |
| :------|:------------------------- |
| Case 01 | $`\overrightarrow{R}_{21}\overrightarrow{R}_{32}\overrightarrow{R}_{34}\overrightarrow{R}_{41}\overrightarrow{R}_{26}\overrightarrow{R}_{37}\overrightarrow{R}_{15}\overrightarrow{R}_{48}\overrightarrow{R}_{65}\overrightarrow{R}_{76}\overrightarrow{R}_{78}\overrightarrow{R}_{85}`$ |
| Case 02 | $`\overleftarrow{R}_{21}\overrightarrow{R}_{32}\overrightarrow{R}_{34}\overleftarrow{R}_{41}\overleftarrow{R}_{26}\overrightarrow{R}_{37}\overrightarrow{R}_{15}\overleftarrow{R}_{48}\overrightarrow{R}_{65}\overleftarrow{R}_{76}\overleftarrow{R}_{78}\overrightarrow{R}_{85}`$ |
| Case 03 | $`\overleftarrow{R}_{21}\overrightarrow{R}_{32}\overleftarrow{R}_{34}\overrightarrow{R}_{41}\overleftarrow{R}_{26}\overleftarrow{R}_{37}\overrightarrow{R}_{15}\overrightarrow{R}_{48}\overrightarrow{R}_{65}\overrightarrow{R}_{76}\overrightarrow{R}_{78}\overrightarrow{R}_{85}`$ |
| Case 04 | $`\overrightarrow{R}_{21}\overrightarrow{R}_{32}\overrightarrow{R}_{34}\overrightarrow{R}_{41}\overrightarrow{R}_{26}\overrightarrow{R}_{37}\overleftarrow{R}_{15}\overrightarrow{R}_{48}\overleftarrow{R}_{65}\overrightarrow{R}_{76}\overrightarrow{R}_{78}\overleftarrow{R}_{85}`$ |
| Case 05 | $`\overleftarrow{R}_{21}\overleftarrow{R}_{32}\overleftarrow{R}_{34}\overleftarrow{R}_{41}\overleftarrow{R}_{26}\overleftarrow{R}_{37}\overrightarrow{R}_{15}\overleftarrow{R}_{48}\overrightarrow{R}_{65}\overleftarrow{R}_{76}\overleftarrow{R}_{78}\overrightarrow{R}_{85}`$ |
| Case 06 | $`\overleftarrow{S}_{21}\overleftarrow{S}_{32}\overleftarrow{S}_{34}\overleftarrow{S}_{41}\overleftarrow{S}_{26}\overleftarrow{S}_{37}\overleftarrow{S}_{15}\overleftarrow{S}_{48}\overleftarrow{S}_{65}\overleftarrow{S}_{76}\overleftarrow{S}_{78}\overleftarrow{S}_{85}`$ |
| Case 07 | $`\overleftarrow{S}_{21}\overrightarrow{S}_{32}\overrightarrow{S}_{34}\overleftarrow{S}_{41}\overleftarrow{S}_{26}\overrightarrow{S}_{37}\overrightarrow{S}_{15}\overleftarrow{S}_{48}\overrightarrow{S}_{65}\overleftarrow{S}_{76}\overleftarrow{S}_{78}\overrightarrow{S}_{85}`$ |
| Case 08 | $`\overrightarrow{S}_{21}\overleftarrow{S}_{32}\overrightarrow{S}_{34}\overleftarrow{S}_{41}\overrightarrow{S}_{26}\overrightarrow{S}_{37}\overleftarrow{S}_{15}\overleftarrow{S}_{48}\overleftarrow{S}_{65}\overleftarrow{S}_{76}\overleftarrow{S}_{78}\overleftarrow{S}_{85}`$ |
| Case 09 | $`\overleftarrow{S}_{21}\overleftarrow{S}_{32}\overleftarrow{S}_{34}\overleftarrow{S}_{41}\overleftarrow{S}_{26}\overleftarrow{S}_{37}\overrightarrow{S}_{15}\overleftarrow{S}_{48}\overrightarrow{S}_{65}\overleftarrow{S}_{76}\overleftarrow{S}_{78}\overrightarrow{S}_{85}`$ |
| Case 10 | $`\overrightarrow{S}_{21}\overrightarrow{S}_{32}\overrightarrow{S}_{34}\overrightarrow{S}_{41}\overrightarrow{S}_{26}\overrightarrow{S}_{37}\overleftarrow{S}_{15}\overrightarrow{S}_{48}\overleftarrow{S}_{65}\overrightarrow{S}_{76}\overrightarrow{S}_{78}\overleftarrow{S}_{85}`$ |
| Case 11 | $`\overleftarrow{R}_{21}J_{32}\overrightarrow{S}_{34}J_{41}J_{26}\overrightarrow{S}_{37}\overleftarrow{S}_{15}\overleftarrow{S}_{48}J_{65}\overrightarrow{R}_{76}\overleftarrow{S}_{78}J_{85}`$ |
| Case 12 | $`\overleftarrow{R}_{21}\overrightarrow{R}_{32}\overrightarrow{R}_{34}\overleftarrow{R}_{41}J_{26}\overleftarrow{S}_{37}J_{15}J_{48}\overleftarrow{R}_{65}J_{76}J_{78}\overrightarrow{S}_{85}`$ |
| Case 13 | $`\overleftarrow{R}_{21}J_{32}\overrightarrow{S}_{34}J_{41}J_{26}J_{37}J_{15}J_{48}\overrightarrow{S}_{65}J_{76}\overleftarrow{R}_{78}J_{85}`$ |
| Case 14 | $`\overleftarrow{R}_{21}\overrightarrow{R}_{32}J_{34}J_{41}\overleftarrow{R}_{26}J_{37}J_{15}\overrightarrow{S}_{48}J_{65}J_{76}\overrightarrow{S}_{78}\overleftarrow{S}_{85}`$ |
| Case 15 | $`\overleftarrow{R}_{21}\overleftarrow{S}_{32}J_{34}J_{41}\overrightarrow{S}_{26}J_{37}J_{15}\overleftarrow{S}_{48}J_{65}J_{76}\overleftarrow{S}_{78}\overrightarrow{S}_{85}`$ |
| Case 16 | $`\overleftarrow{R}_{21}J_{32}J_{34}\overleftarrow{R}_{41}J_{26}\overleftarrow{R}_{37}\overrightarrow{R}_{15}J_{48}J_{65}\overrightarrow{R}_{76}\overleftarrow{S}_{78}J_{85}`$ |
| Case 17 | $`J_{21}J_{32}J_{34}J_{41}J_{26}J_{37}J_{15}J_{48}J_{65}J_{76}J_{78}J_{85}`$ |
| Case 18 | $`J_{21}J_{32}J_{34}J_{41}\overrightarrow{R}_{26}\overrightarrow{R}_{37}\overrightarrow{R}_{15}\overrightarrow{R}_{48}J_{65}J_{76}J_{78}J_{85}`$ |
| Case 19 | $`J_{21}J_{32}J_{34}J_{41}\overleftarrow{S}_{26}\overleftarrow{S}_{37}\overleftarrow{S}_{15}\overleftarrow{S}_{48}J_{65}J_{76}J_{78}J_{85}`$

| | isentropic cases |
| :------|:------------------------- |
| Case i01 | $`\overleftarrow{R}_{21}\overrightarrow{R}_{32}\overrightarrow{R}_{34}\overleftarrow{R}_{41}\overrightarrow{S}_{26}\overleftarrow{S}_{37}\overleftarrow{S}_{15}\overrightarrow{S}_{48}\overrightarrow{R}_{65}\overleftarrow{R}_{76}\overleftarrow{R}_{78}\overrightarrow{R}_{85}`$ |
| Case i02 | $`\overrightarrow{S}_{21}\overleftarrow{S}_{32}\overleftarrow{S}_{34}\overrightarrow{S}_{41}\overleftarrow{R}_{26}\overrightarrow{R}_{37}\overleftarrow{R}_{15}\overrightarrow{R}_{48}\overleftarrow{S}_{65}\overrightarrow{S}_{76}\overrightarrow{S}_{78}\overleftarrow{S}_{85}`$ |