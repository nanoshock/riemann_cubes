# $`\overrightarrow{R}_{BA}\overrightarrow{S}_{CB}\overleftarrow{R}_{CD}\overleftarrow{S}_{DA}`$

![](isentropic_shockwave_rarefaction_leftright_allinone.ogv) | ![](isentropic_shockwave_rarefaction_leftright_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overrightarrow{R}_{BA}: & \upsilon_B - \upsilon_A = \Phi_{BA}, & p_A = K \rho_A^\gamma, & \upsilon_B < \upsilon_A, & \nu_B = \nu_A, & p_B < p_A. \\
\overrightarrow{S}_{CB}: & \nu_C - \nu_B = \Psi_{CB}, & p_B = K \rho_B^\gamma, & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C > p_B. \\
\overleftarrow{R}_{CD}: & \upsilon_D - \upsilon_C = \Phi_{CD}, & p_C = K \rho_C^\gamma, & \upsilon_C < \upsilon_D, & \nu_C = \nu_D, & p_C > p_D. \\
\overleftarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & p_D = K \rho_D^\gamma, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`K = 1`$, $`p_A = 1.0`$, $`\rho_A = 1.0`$, $`\rho_B = 0.5`$, $`\upsilon_A = 0.0`$, $`\nu_A = 0.0`$. According to Theorem four of Schulz-Rinne $`p_A = p_C`$, $`p_B = p_D`$, and, hence, $`\rho_A = \rho_C`$, $`\rho_B = \rho_D`$. the remaining non-trivial quantites become
```math
\begin{aligned}
    p_B &= K \rho_B^\gamma \approx 0.3789291416276, \\
    \upsilon_B &= \upsilon_A + \Phi_{BA} = \frac{2\sqrt{\gamma}}{\gamma - 1}\left( \sqrt{\frac{p_B}{\rho_B}} - \sqrt{\frac{p_A}{\rho_A}} \right) \approx -0.765833195417434, \\
    \nu_D &= \nu_A + \Psi_{DA} = \frac{\left( p_D - p_A \right)\left( \rho_D - \rho_A \right)}{\rho_D\rho_A} \approx 0.788080489780327.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](isentropic_shockwave_rarefaction_leftright.xml) provided in this folder.

## Final State

![Density Field](isentropic_shockwave_rarefaction_leftright_allinone.png) | ![Density Contours](isentropic_shockwave_rarefaction_leftright_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
