# $`\overrightarrow{J}_{BA}\overrightarrow{J}_{CB}\overleftarrow{J}_{CD}\overleftarrow{J}_{DA}`$

![](all_slipline_clockwise_motion_allinone.ogv) | ![](all_slipline_clockwise_motion_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{aligned}
    \upsilon_A = \upsilon_B &> \upsilon_C = \upsilon_D. \\
    \nu_A = \nu_D &< \nu_C = \nu_B. \\
    p_A = p_B &= p_C = p_D.
\end{aligned}
```

## Calculations
We set $`p_A = 1.0`$, $`\rho_A = 1.0`$, $`\rho_B = 2.0`$, $`\rho_C = 1.0`$, $`\rho_D = 3.0`$, $`\upsilon_A = 0.75`$, $`\upsilon_C = -0.75`$, $`\nu_A = -0.5`$ and $`\nu_B = 0.5`$.

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](all_slipline_clockwise_motion.xml) provided in this folder.

## Final State

![Density Field](all_slipline_clockwise_motion_allinone.png) | ![Density Contours](all_slipline_clockwise_motion_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
