# $`\overrightarrow{S}_{BA}J_{CB}J_{CD}\overrightarrow{S}_{DA}`$

![](inflow_shockwave_allinone.ogv) | ![](inflow_shockwave_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overrightarrow{S}_{BA}: & \upsilon_B - \upsilon_A = \Psi_{BA}, & \frac{\rho_B}{\rho_A} = \Pi_{BA}, & \upsilon_B > \upsilon_A, & \nu_B = \nu_A, & p_B > p_A. \\
J_{CB}: & & \rho_C \mathrel{\char`≠} \rho_B,  & \upsilon_C \mathrel{\char`≠} \upsilon_B, & \nu_C = \nu_B, & p_C = p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overrightarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & \frac{\rho_D}{\rho_A} = \Pi_{DA}, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D > p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 0.4`$, $`p_B = 1.0`$, $`\rho_B = 1.0`$, $`\rho_C = 0.8`$, $`\upsilon_A = 0.0`$ and $`\nu_A = 0.0`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_A &= \frac{\rho_B}{\Pi_{BA}} = \rho_B \left(\frac{\frac{p_B}{p_A}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_B}{p_A}} \right)^{-1} \approx 0.53125, \\
    \rho_D &= \rho_A \Pi_{DA} = \rho_A \frac{\frac{p_D}{p_A}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_D}{p_A}} = 1, \\
    \upsilon_B &= \upsilon_A + \Psi_{BA} = \upsilon_A + \sqrt{\frac{(p_B-p_A)(\rho_B-\rho_A)}{\rho_B \rho_A}} \approx 0.727606875, \\
    \nu_D &= \nu_A + \Psi_{DA} = \nu_A + \sqrt{\frac{(p_D-p_A)(\rho_D-\rho_A)}{\rho_D \rho_A}} \approx 0.727606875.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](inflow_shockwave.xml) provided in this folder.

## Final State

![Density Field](inflow_shockwave_allinone.png) | ![Density Contours](inflow_shockwave_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
