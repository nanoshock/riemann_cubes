# $`J_{BA}\overleftarrow{S}_{CB}J_{CD}\overleftarrow{S}_{DA}`$

![](opposing_shockwave_plusminus_allinone.ogv) | ![](opposing_shockwave_plusminus_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
J_{BA}: & & \rho_B \mathrel{\char`≠} \rho_A, & \upsilon_B = \upsilon_A, & \nu_B \mathrel{\char`≠} \nu_A, & p_B = p_A. \\
\overleftarrow{S}_{CB}: & \nu_C - \nu_B = \Psi_{CB}, & \frac{\rho_C}{\rho_B} = \Pi_{CB},  & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C < p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overleftarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & \frac{\rho_D}{\rho_A} = \Pi_{DA}, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4,\, p_A = 1.5,\, p_C = 0.4,\, \rho_A = 0.5,\, \rho_B = 1.5,\, \upsilon_A = 0.0,\, \nu_A = -0.3`$ and $`\nu_B = 0.3`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_D &= \rho_A \Pi_{DA} = \rho_A \frac{\frac{p_D}{p_A}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_D}{p_A}} \approx 0.207446809, \\
    \rho_C &= \rho_B \Pi_{CB} = \rho_B \frac{\frac{p_C}{p_B}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_C}{p_B}} \approx 0.622340426, \\
    \nu_D &= \nu_A + \Psi_{DA} = \nu_A + \sqrt{\frac{(p_D-p_A)(\rho_D-\rho_A)}{\rho_D \rho_A}} \approx 1.461409692, \\
    \nu_C &= \nu_B + \Psi_{CB} = \nu_B + \sqrt{\frac{(p_C-p_B)(\rho_C-\rho_B)}{\rho_C \rho_B}} \approx 1.316950360.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](opposing_shockwave_plusminus.xml) provided in this folder.

## Final State

![Density Field](opposing_shockwave_plusminus_allinone.png) | ![Density Contours](opposing_shockwave_plusminus_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
