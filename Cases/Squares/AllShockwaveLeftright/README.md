# $`\overleftarrow{S}_{BA}\overrightarrow{S}_{CB}\overrightarrow{S}_{CD}\overleftarrow{S}_{DA}`$

![](all_shockwave_leftright_allinone.ogv) | ![](all_shockwave_leftright_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overleftarrow{S}_{BA}: & \upsilon_B - \upsilon_A = \Psi_{BA}, & \frac{\rho_B}{\rho_A} = \Pi_{BA}, & \upsilon_B > \upsilon_A, & \nu_B = \nu_A, & p_B < p_A. \\
\overrightarrow{S}_{CB}: & \nu_C - \nu_B = \Psi_{CB}, & \frac{\rho_C}{\rho_B} = \Pi_{CB}, & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C > p_B. \\
\overrightarrow{S}_{CD}: & \upsilon_C - \upsilon_D = \Psi_{CD}, & \frac{\rho_C}{\rho_D} = \Pi_{CD}, & \upsilon_C > \upsilon_D, & \nu_C = \nu_D, & p_C > p_D. \\
\overleftarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & \frac{\rho_D}{\rho_A} = \Pi_{DA}, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.1`$, $`p_B = 0.35`$, $`\rho_A = 1.1`$, $`\upsilon_A = 0.0`$, and $`\nu_A = 0.0`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_B &= \rho_A \Pi_{BA} = \rho_A \frac{ \frac{p_B}{p_A} + \frac{ \gamma - 1}{ \gamma + 1} }{ 1 + \frac{ \gamma -1 }{ \gamma + 1} \frac{p_B}{p_A} } = \frac{11}{10} \frac{ \frac{\frac{7}{20}}{\frac{11}{10}} + \frac{1}{6} }{ 1 + \frac{1}{6} \frac{\frac{7}{20}}{\frac{11}{10}} } = \frac{352}{695} \approx 0.506474820143885, \\
   \upsilon_B &= \upsilon_A + \Psi_{BA} = \sqrt{ \frac{\left( p_B - p_A \right) \left( \rho_B - \rho_A \right) }{\rho_B \rho_A} } = \sqrt{ \frac{\left( \frac{7}{20} - \frac{11}{10} \right) \left( \frac{352}{695} - \frac{11}{10} \right) }{\frac{352}{695} \frac{11}{10}} } = \sqrt{\frac{1125}{1408}} \approx 0.893871177417743, \\
   \upsilon_B - \upsilon_A &= \nu_D - \nu_A = \nu_D = \upsilon_B.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](all_shockwave_leftright.xml) provided in this folder.

## Final State

![Density Field](all_shockwave_leftright_allinone.png) | ![Density Contours](all_shockwave_leftright_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
