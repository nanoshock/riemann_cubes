# $`J_{BA}\overrightarrow{R}_{CB}J_{CD}\overrightarrow{R}_{DA}`$

![](opposing_rarefaction_minusplus_allinone.ogv) | ![](opposing_rarefaction_minusplus_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
J_{BA}: & & \rho_B \mathrel{\char`≠} \rho_A, & \upsilon_B = \upsilon_A, & \nu_B \mathrel{\char`≠} \nu_A, & p_B = p_A. \\
\overrightarrow{R}_{CB}: & \nu_C - \nu_B = \Phi_{CB}, & \frac{p_C}{p_B} = \left( \frac{\rho_C}{\rho_B} \right)^\gamma,  & \upsilon_C = \upsilon_B, & \nu_C < \nu_B, & p_C < p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overrightarrow{R}_{DA}: & \nu_D - \nu_A = \Phi_{DA}, & \frac{p_D}{p_A} = \left( \frac{\rho_D}{\rho_A} \right)^\gamma, & \upsilon_D = \upsilon_A, & \nu_D < \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.0`$, $`p_C = 0.4`$, $`\rho_A = 1.0`$, $`\rho_B = 2.0`$, $`\upsilon_A = 0.0`$, $`\nu_A = 0.35`$ and $`\nu_B = 0.25`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_D &= \rho_A \left( \frac{p_D}{p_A} \right)^{\frac{1}{\gamma}} \approx 0.519705289, \\
    \rho_C &= \rho_B \left( \frac{p_C}{p_B} \right)^{\frac{1}{\gamma}} \approx 1.039410578, \\
    \nu_D &= \nu_A + \Phi_{DA} = \nu_A + \frac{2 \sqrt{\gamma}}{\gamma-1} \left( \sqrt{\frac{p_D}{\rho_D}} - \sqrt{\frac{p_A}{\rho_A}} \right) \approx -0.375863575, \\
    \nu_C &= \nu_B + \Phi_{CB} = \nu_B + \frac{2 \sqrt{\gamma}}{\gamma-1} \left( \sqrt{\frac{p_C}{\rho_C}} - \sqrt{\frac{p_B}{\rho_B}} \right) \approx -0.263263057. \\
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](opposing_rarefaction_minusplus.xml) provided in this folder.

## Final State

![Density Field](opposing_rarefaction_minusplus_allinone.png) | ![Density Contours](opposing_rarefaction_minusplus_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
