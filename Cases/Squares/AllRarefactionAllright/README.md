# $`\overrightarrow{R}_{BA}\overrightarrow{R}_{CB}\overrightarrow{R}_{CD}\overrightarrow{R}_{DA}`$

![](all_rarefaction_allright_allinone.ogv) | ![](all_rarefaction_allright_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overrightarrow{R}_{BA}: & \upsilon_B - \upsilon_A = \Phi_{BA}, & \frac{p_B}{p_A} = \left( \frac{\rho_B}{\rho_A} \right)^\gamma, & \upsilon_B > \upsilon_A, & \nu_B = \nu_A, & p_B < p_A. \\
\overrightarrow{R}_{CB}: & \nu_C - \nu_B = \Phi_{CB}, & \frac{p_C}{p_B} = \left( \frac{\rho_C}{\rho_B} \right)^\gamma, & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C < p_B. \\
\overrightarrow{R}_{CD}: & \upsilon_C - \upsilon_D = \Phi_{CD}, & \frac{p_C}{p_D} = \left( \frac{\rho_C}{\rho_D} \right)^\gamma, & \upsilon_C < \upsilon_D, & \nu_C = \nu_D, & p_C < p_D. \\
\overrightarrow{R}_{DA}: & \nu_D - \nu_A = \Phi_{DA}, & \frac{p_D}{p_A} = \left( \frac{\rho_D}{\rho_A} \right)^\gamma, & \upsilon_D = \upsilon_A, & \nu_D <> \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.0`$, $`p_B = 0.4`$, $`p_D = 0.15`$, $`\rho_A = 1.0`$, $`\upsilon_A = 0.0`$, and $`\nu_A = 0.0`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
   \rho_B &= \rho_A \left( \frac{p_B}{p_A} \right)^{1/\gamma} = \sqrt[1.4]{\frac{2}{5}} \approx 0.519705289043764, \\
   \rho_D &= \rho_A \left( \frac{p_D}{p_A} \right)^{1/\gamma} = \sqrt[1.4]{\frac{3}{20}} \approx 0.257925188415773, \\
   \upsilon_B &= \upsilon_A + \Phi_{BA} = \frac{2\sqrt{1.4}}{0.4} \left( \sqrt{\frac{0.4}{\sqrt[1.4]{0.4}}} - 1 \right) = \sqrt{35} \left( \sqrt[7]{\frac{2}{5}} - 1 \right) \approx -0.725863575730743, \\
   \nu_D &= \nu_A + \Phi_{DA} = \frac{2\sqrt{1.4}}{0.4} \left( \sqrt{\frac{0.15}{\sqrt[1.4]{0.15}}} - 1 \right) = \sqrt{35} \left( \sqrt[7]{\frac{3}{20}} - 1  \right) \approx -1.40445707948429, \\
   \upsilon_C - \upsilon_D &= \Phi_{CD} = \frac{2\sqrt{\gamma}}{\gamma - 1} \left( \sqrt{\frac{p_C}{\rho_C}} - \sqrt{\frac{p_D}{\sqrt[\gamma]{p_D}}} \right),
   \end{aligned}
```
```math
\begin{aligned}
   \rightarrow p_C &= \rho_C \left( \frac{\upsilon_C}{ \frac{2\sqrt{\gamma}}{\gamma -1 } } + \sqrt{\frac{p_D}{\sqrt[\gamma]{p_D}}} \right)^2, \\
   \rho_C &= \rho_D \left( \frac{p_C}{p_D} \right)^{1/\gamma} = \sqrt[\gamma]{\rho_C \left( \frac{\upsilon_C}{ \frac{2\sqrt{\gamma}}{\gamma -1 } } + \sqrt{\frac{p_D}{\sqrt[\gamma]{p_D}}} \right)^2 } \rightarrow  \rho_C = \sqrt[0.4]{ \left( \sqrt[7]{\frac{2}{5}} + \sqrt[7]{\frac{3}{20}} -1 \right)^2 } \approx 0.107298778982674, \\
   \frac{\rho_C}{\rho_B} &= \left( \frac{p_C}{p_B} \right)^{1/\gamma} \rightarrow p_C = p_B \left( \frac{\rho_C}{\rho_B} \right)^\gamma = p_B \left( \frac{\rho_C}{\sqrt[\gamma]{p_B}} \right)^\gamma = \left( \sqrt[7]{\frac{2}{5}} + \sqrt[7]{\frac{3}{20}} -1 \right)^7 \approx 0.0439372318492184.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](all_rarefaction_allright.xml) provided in this folder.

## Final State

![Density Field](all_rarefaction_allright_allinone.png) | ![Density Contours](all_rarefaction_allright_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
