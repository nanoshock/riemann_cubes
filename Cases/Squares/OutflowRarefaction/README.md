# $`\overleftarrow{R}_{BA}J_{CB}J_{CD}\overleftarrow{R}_{DA}`$

![](outflow_rarefaction_allinone.ogv) | ![](outflow_rarefaction_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overleftarrow{R}_{BA}: & \upsilon_A - \upsilon_B = \Phi_{BA}, & \frac{p_B}{p_A} = \left( \frac{\rho_B}{\rho_A} \right)^\gamma, & \upsilon_B < \upsilon_A, & \nu_B = \nu_A, & p_B > p_A. \\
J_{CB}: & & \rho_C \mathrel{\char`≠} \rho_B,  & \upsilon_C \mathrel{\char`≠} \upsilon_B, & \nu_C = \nu_B, & p_C = p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overleftarrow{R}_{DA}: & \nu_A - \nu_D = \Phi_{DA}, & \frac{p_D}{p_A} = \left( \frac{\rho_D}{\rho_A} \right)^\gamma, & \upsilon_D = \upsilon_A, & \nu_D < \nu_A, & p_D > p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 0.4`$, $`p_B = 1.0`$, $`\rho_B = 1.0`$, $`\rho_C = 0.8`$, $`\upsilon_A = 0.1`$ and $`\nu_A = 0.1`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_A &= \rho_B \left( \frac{p_A}{p_B} \right)^{\frac{1}{\gamma}} \approx 0.519705289, \\
    \rho_D &= \rho_A \left( \frac{p_D}{p_A} \right)^{\frac{1}{\gamma}} = 1, \\
    \upsilon_B &= \upsilon_A - \Phi_{BA} = \upsilon_A - \frac{2 \sqrt{\gamma}}{\gamma-1} \left( \sqrt{\frac{p_B}{\rho_B}} - \sqrt{\frac{p_A}{\rho_A}} \right) \approx -0.625863576, \\
    \nu_D &= \nu_A - \Phi_{DA} = \nu_A - \frac{2 \sqrt{\gamma}}{\gamma-1} \left( \sqrt{\frac{p_D}{\rho_D}} - \sqrt{\frac{p_A}{\rho_A}} \right) \approx -0.625863576.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](outflow_rarefaction.xml) provided in this folder.

## Final State

![Density Field](outflow_rarefaction_allinone.png) | ![Density Contours](outflow_rarefaction_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
