# $`\overleftarrow{R}_{BA}J_{CB}J_{CD}\overrightarrow{S}_{DA}`$

![](inflow_shockwave_outflow_rarefaction_allinone.ogv) | ![](inflow_shockwave_outflow_rarefaction_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overleftarrow{R}_{BA}: & \upsilon_A - \upsilon_B = \Phi_{BA}, & \frac{p_B}{p_A} = \left( \frac{\rho_B}{\rho_A} \right)^\gamma, & \upsilon_B < \upsilon_A, & \nu_B = \nu_A, & p_B > p_A. \\
J_{CB}: & & \rho_C \mathrel{\char`≠} \rho_B,  & \upsilon_C \mathrel{\char`≠} \upsilon_B, & \nu_C = \nu_B, & p_C = p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overrightarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & \frac{\rho_D}{\rho_A} = \Pi_{DA}, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D > p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 0.4`$, $`p_B = 1.0`$, $`\rho_C = 0.8`$, $`\rho_D = 1.0`$, $`\upsilon_A = 0.1`$ and $`\nu_A = 0.1`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_A &= \rho_D \left( \Pi_{DA} \right) ^{-1} = \rho_D \left( \frac{\frac{p_D}{p_A}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_D}{p_A}} \right) ^{-1} \approx 0.53125, \\
    \rho_B &= \rho_A \left( \frac{p_B}{p_A} \right)^{\frac{1}{\gamma}} \approx 1.022213957, \\
    \upsilon_B &= \upsilon_A - \Phi_{BA} = \upsilon_A - \frac{2 \sqrt{\gamma}}{\gamma-1} \left( \sqrt{\frac{p_B}{\rho_B}} - \sqrt{\frac{p_A}{\rho_A}} \right) \approx -0.617933305, \\
    \nu_D &= \nu_A + \Psi_{DA} = \nu_A + \sqrt{\frac{(p_D-p_A)(\rho_D-\rho_A)}{\rho_D \rho_A}} \approx 0.827606875.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](inflow_shockwave_outflow_rarefaction.xml) provided in this folder.

## Final State

![Density Field](inflow_shockwave_outflow_rarefaction_allinone.png) | ![Density Contours](inflow_shockwave_outflow_rarefaction_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
