# $`\overrightarrow{R}_{BA}\overleftarrow{R}_{CB}\overleftarrow{R}_{CD}\overrightarrow{R}_{DA}`$

![](all_rarefaction_leftright_allinone.ogv) | ![](all_rarefaction_leftright_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overrightarrow{R}_{BA}: & \upsilon_B - \upsilon_A = \Phi_{BA}, & \frac{p_B}{p_A} = \left( \frac{\rho_B}{\rho_A} \right)^\gamma, & \upsilon_B > \upsilon_A, & \nu_B = \nu_A, & p_B < p_A. \\
\overleftarrow{R}_{CB}: & \nu_B - \nu_C = \Phi_{CB}, & \frac{p_C}{p_B} = \left( \frac{\rho_C}{\rho_B} \right)^\gamma, & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C > p_B. \\
\overleftarrow{R}_{CD}: & \upsilon_D - \upsilon_C = \Phi_{CD}, & \frac{p_C}{p_D} = \left( \frac{\rho_C}{\rho_D} \right)^\gamma, & \upsilon_C < \upsilon_D, & \nu_C = \nu_D, & p_C > p_D. \\
\overrightarrow{R}_{DA}: & \nu_D - \nu_A = \Phi_{DA}, & \frac{p_D}{p_A} = \left( \frac{\rho_D}{\rho_A} \right)^\gamma, & \upsilon_D = \upsilon_A, & \nu_D < \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.0`$, $`p_B = 0.4`$, $`\rho_A = 1.0`$, $`\upsilon_A = 0.0`$, and $`\nu_A = 0.0`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
   \rho_B &= \rho_A \left( \frac{p_B}{p_A} \right)^{1/\gamma} = \sqrt[1.4]{\frac{2}{5}} \approx 0.519705289043764, \\
   \Phi_{BA} &= -\Phi_{CD} = \sqrt{ \frac{p_B}{\rho_B} } - \sqrt{ \frac{p_A}{\rho_A} } = - \sqrt{ \frac{p_C}{\rho_C} } + \sqrt{ \frac{p_D}{\rho_D} }, \\
   \Phi_{DA} &= -\Phi_{CB} \Rightarrow \sqrt{ \frac{p_D}{\rho_D} } - \sqrt{ \frac{p_A}{\rho_A} } = \sqrt{ \frac{p_B}{\rho_B} } - \sqrt{ \frac{p_A}{\rho_A} } - \sqrt{ \frac{p_D}{\rho_D} } - \sqrt{ \frac{p_B}{\rho_B} } \rightarrow \frac{p_D}{\rho_D} = \frac{p_B}{\rho_B}, \\
   p_D &= p_A \left( \frac{\rho_D}{\rho_A} \right)^\gamma, \\
   \Rightarrow \rho_D^{\gamma - 1} &= \rho_B^{\gamma - 1} \rightarrow \rho_D = \rho_B \rightarrow p_D = p_B \rightarrow p_C = p_A \rightarrow \rho_C = \rho_A, \\
   \upsilon_B &= \upsilon_A + \Phi_{BA} \approx −0.725863575730743, \\
   \nu_D &= \nu_A + \Phi_{DA} \approx −0.725863575730743.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](all_rarefaction_leftright.xml) provided in this folder.

## Final State

![Density Field](all_rarefaction_leftright_allinone.png) | ![Density Contours](all_rarefaction_leftright_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
