# $`J_{BA}\overleftarrow{S}_{CB}J_{CD}\overrightarrow{R}_{DA}`$

![](opposing_rarefaction_shockwave_plusminus_allinone.ogv) | ![](opposing_rarefaction_shockwave_plusminus_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
J_{BA}: & & \rho_B \mathrel{\char`≠} \rho_A, & \upsilon_B = \upsilon_A, & \nu_B \mathrel{\char`≠} \nu_A, & p_B = p_A. \\
\overleftarrow{S}_{CB}: & \nu_C - \nu_B = \Psi_{CB}, & \frac{\rho_C}{\rho_B} = \Pi_{CB},  & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C < p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overrightarrow{R}_{DA}: & \nu_D - \nu_A = \Phi_{DA}, & \frac{p_D}{p_A} = \left( \frac{\rho_D}{\rho_A} \right)^\gamma, & \upsilon_D = \upsilon_A, & \nu_D < \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.0`$, $`p_C = 0.4`$, $`\rho_A = 1.0`$, $`\rho_B = 2.0`$, $`\upsilon_A = 0.0`$, $`\nu_A = 0.3`$ and $`\nu_B = -0.3`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_D &= \rho_A \left( \frac{p_D}{p_A} \right) ^{\frac{1}{\gamma}} \approx 0.519705289, \\
    \rho_C &= \rho_B \Pi_{CB} = \rho_B \frac{\frac{p_C}{p_B}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_C}{p_B}} \approx  1.0625, \\
    \nu_D &= \nu_A + \Phi_{DA} = \nu_A + \frac{2 \sqrt{\gamma}}{\gamma-1} \left( \sqrt{\frac{p_D}{\rho_D}} - \sqrt{\frac{p_A}{\rho_A}} \right) \approx -0.425863576, \\
    \nu_C &= \nu_B + \Psi_{CB} = \nu_B + \sqrt{\frac{(p_C-p_B)(\rho_C-\rho_B)}{\rho_C \rho_B}} \approx 0.214495755.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](opposing_rarefaction_shockwave_plusminus.xml) provided in this folder.

## Final State

![Density Field](opposing_rarefaction_shockwave_plusminus_allinone.png) | ![Density Contours](opposing_rarefaction_shockwave_plusminus_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
