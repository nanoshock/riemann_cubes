# $`\overleftarrow{S}_{BA}J_{CB}J_{CD}\overleftarrow{S}_{DA}`$

![](outflow_shockwave_allinone.ogv) | ![](outflow_shockwave_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overleftarrow{S}_{BA}: & \upsilon_B - \upsilon_A = \Psi_{BA}, & \frac{\rho_B}{\rho_A} = \Pi_{BA}, & \upsilon_B > \upsilon_A, & \nu_B = \nu_A, & p_B < p_A. \\
J_{CB}: & & \rho_C \mathrel{\char`≠} \rho_B,  & \upsilon_C \mathrel{\char`≠} \upsilon_B, & \nu_C = \nu_B, & p_C = p_B. \\
J_{CD}: & & \rho_C \mathrel{\char`≠} \rho_D, & \upsilon_C = \upsilon_D, & \nu_C \mathrel{\char`≠} \nu_D, & p_C = p_D. \\
\overleftarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & \frac{\rho_D}{\rho_A} = \Pi_{DA}, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.0`$, $`p_B = 0.4`$, $`\rho_A = 1.0`$, $`\rho_C = 0.8`$, $`\upsilon_A = 0.1`$ and $`\nu_A = 0.0`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_B &= \rho_A \Pi_{BA} = \rho_A \frac{\frac{p_B}{p_A}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_B}{p_A}} \approx 0.53125, \\
    \rho_D &= \rho_A \Pi_{DA} = \rho_A \frac{\frac{p_D}{p_A}+\frac{\gamma-1}{\gamma+1}}{1+\frac{\gamma-1}{\gamma+1}\frac{p_D}{p_A}} \approx 0.53125, \\
    \upsilon_B &= \upsilon_A + \Psi_{BA} = \upsilon_A + \sqrt{\frac{(p_B-p_A)(\rho_B-\rho_A)}{\rho_B \rho_A}} \approx 0.827606875, \\
    \nu_D &= \nu_A + \Psi_{DA} = \nu_A + \sqrt{\frac{(p_D-p_A)(\rho_D-\rho_A)}{\rho_D \rho_A}} \approx 0.727606875.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](outflow_shockwave.xml) provided in this folder.

## Final State

![Density Field](outflow_shockwave_allinone.png) | ![Density Contours](outflow_shockwave_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
