# $`\overleftarrow{S}_{BA}\overleftarrow{S}_{CB}\overleftarrow{S}_{CD}\overleftarrow{S}_{DA}`$

![](all_shockwave_allleft_allinone.ogv) | ![](all_shockwave_allleft_contour.ogv)
:----------------------------------------:|:----------------------------------------:

## Equations
```math
\begin{array}{llllll}
\overleftarrow{S}_{BA}: & \upsilon_B - \upsilon_A = \Psi_{BA}, & \frac{\rho_B}{\rho_A} = \Pi_{BA}, & \upsilon_B > \upsilon_A, & \nu_B = \nu_A, & p_B < p_A. \\
\overleftarrow{S}_{CB}: & \nu_C - \nu_B = \Psi_{CB}, & \frac{\rho_C}{\rho_B} = \Pi_{CB}, & \upsilon_C = \upsilon_B, & \nu_C > \nu_B, & p_C < p_B. \\
\overleftarrow{S}_{CD}: & \upsilon_C - \upsilon_D = \Psi_{CD}, & \frac{\rho_C}{\rho_D} = \Pi_{CD}, & \upsilon_C > \upsilon_D, & \nu_C = \nu_D, & p_C < p_D. \\
\overleftarrow{S}_{DA}: & \nu_D - \nu_A = \Psi_{DA}, & \frac{\rho_D}{\rho_A} = \Pi_{DA}, & \upsilon_D = \upsilon_A, & \nu_D > \nu_A, & p_D < p_A.
\end{array}
```

## Calculations
We set $`\gamma = 1.4`$, $`p_A = 1.5`$, $`p_B = 0.3`$, $`\rho_A = 1.5`$, $`\upsilon_A = 0.0`$, and $`\nu_A = 0.0`$. Hence, the remaining non-trivial quantites become
```math
\begin{aligned}
    \rho_B &= \rho_A \Pi_{BA} = \rho_A \frac{ \frac{p_B}{p_A} + \frac{ \gamma - 1}{ \gamma + 1} }{ 1 + \frac{ \gamma -1 }{ \gamma + 1} \frac{p_B}{p_A} } = 1.5 \frac{ \frac{ 0.3 }{ 1.5 } + \frac{ 0.4 }{ 2.4 } }{ 1 + \frac{ 0.4 }{ 2.4 } \frac{ 0.3 }{ 1.5 } } = \frac{3}{2} \frac{11}{30} \frac{30}{31} = \frac{33}{62} \approx 0.532258064516129, \\
   \upsilon_B &= \upsilon_A + \Psi_{BA} = \sqrt{ \frac{\left( p_B - p_A \right) \left( \rho_B - \rho_A \right) }{\rho_B \rho_A} } = \sqrt{\frac{ -\frac{6}{5}\left( \frac{33}{62} - \frac{3}{2}\right)}{\frac{33}{62}\frac{3}{2}}} = \sqrt{\frac{16}{11}} \approx 1.20604537831105, \\
   p_D &= p_B, \quad \rho_D = \rho_B, \quad \upsilon_C = \upsilon_B, \quad \upsilon_D = \upsilon_A, \quad \nu_B = \nu_A, \\
   \rho_C &= \rho_B \Pi_{CB} = \rho_B \frac{ \frac{p_C}{p_B} + \frac{ \gamma - 1}{ \gamma + 1} }{ 1 + \frac{ \gamma -1 }{ \gamma + 1} \frac{p_C}{p_B} }  \rightarrow \rho_C = \rho_B \frac{ p_B + 6 p_C }{ 6 p_B + p_C }, \\
\end{aligned}
```
```math
\begin{aligned}
   \upsilon_C - \upsilon_D &= \Psi_{CD} = \sqrt{ \frac{\left( p_C - p_D \right) \left( \rho_C - \rho_D \right) }{\rho_C \rho_D} } = \sqrt{\frac{16}{11}} = \sqrt{ \frac{\left( p_C - p_D \right) \left( \rho_B\frac{ p_B + 6 p_C }{ 6 p_B + p_C } - \rho_D \right) }{\rho_B\frac{ p_B + 6 p_C }{ 6 p_B + p_C } \rho_D} } \\
   &= \frac{32}{62}\frac{16}{11}\frac{60 p_C + 3}{18 +10 p_C} = p_C \frac{60 p_C + 3}{18 + 10 p_C} - p_C - \frac{3}{10} \frac{60 p_C + 3 }{18+10 p_C} + \frac{3}{10}, \\
\end{aligned}
```
```math
\begin{aligned}
   &\rightarrow 15500 p_C^2 - 23700 p_C + 675 = 0 \rightarrow \\
   p_C &= \frac{ 23700 \pm 22800}{31000} \rightarrow p_{C,1} = \frac{3}{2}, \quad \underline{p_{C,2} = \frac{9}{310}} \approx 0.0290322580645161, \\
   \rho_C &= \rho_B \frac{ p_B + 6 p_C }{ 6 p_B + p_C } = \frac{33}{62}  \frac{ \frac{3}{10} + 6 \frac{9}{310} }{ 6 \frac{3}{10} + \frac{9}{310} } = \frac{33}{62} \frac{147}{310} \frac{310}{567} = \frac{77}{558} \approx 0.137992831541219, \\
   \nu_C &= \nu_D = \upsilon_B - \upsilon_A + \nu_A = \upsilon_B.
\end{aligned}
```

## Case setup
We run the case using a two-dimensional ALPACA and [inputfile](all_shockwave_allleft.xml) provided in this folder.

## Final State

![Density Field](all_shockwave_allleft_allinone.png) | ![Density Contours](all_shockwave_allleft_contour.png)
:------------------------------------------------------:|:--------------------------------------------------------:
