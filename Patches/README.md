# Patches

For the simulation of the presented polytropic cases we use the HLLC Riemann solver. This solver is implemented in ALPACA, but not selected as default. The patch `hllc_switch.patch` changes this setting. Furthermore, we provide patches to switch to the HLL Riemann solver and the Roe-M Riemann solver. For the Roe Riemann solver we do not provide a patch, as it is selected as default.
The `isentropic.patch` adapts the settings to simulate the isentropic cases.

To use the patches the Alpaca submodule needs to be properly initialized. Then, the patches can simply be applied by running `git apply <name>.patch`, with the 'name' of the respective patch inserted accordingly.
