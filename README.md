# A Systematic Analysis of Three-dimensional Riemann Problems for Verification of Compressible-flow Solvers

This git repository accompanies the publication with the same title by Hoppe et al. in [Computers and fluids](https://doi.org/10.1016/j.compfluid.2024.106298) and the related data publication (submitted to Data in Brief).

Note:
- The cases numberings differ from the paper. The case strings, however, allow an easy look-up.
- The subdirectories hold their own READMEs with addtional instructions for the respective part of the repository

Content:
- The `Cases` directory contains the initial conditions and results of the 2D and 3D cases.
- The `Combinatorics` and `Evaluation` directories contain programs to identify all existing wave patterns and find suitable initial conditions.
- The `Alpaca` submodule leads to the git repository of the open-source compressible flow solver ALPACA, which also provides detailed installation instructions and example cases. Running
```
git submodule update --init --recursive
```
includes the submodule at the correct commit.
- The `Patches` directory contains patch-files to adapt the settings of Alpaca to the isentropic case and to switch to different Riemann solvers.
- The `Postprocessing` directory bundles scripts and instruction for the postprocessing of the raw simulation data.
- The `Catch2` submodule contains an open-source library for unit tests which is neccesary to build the `Combinatorics` program.
