%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function r = FourLeftrightRSTwoLeftrightS( octant1, octant2, octant3, octant4, octant5, octant6, octant7, octant8, gamma, isentropic )

[r_21, msg_21] = ShockRight(       octant2, octant1, gamma, isentropic, 1 );
[r_32, msg_32] = ShockLeft(        octant3, octant2, gamma, isentropic, 2 );
[r_34, msg_34] = ShockLeft(        octant3, octant4, gamma, isentropic, 1 );
[r_41, msg_41] = ShockRight(       octant4, octant1, gamma, isentropic, 2 );

[r_26, msg_26] = RarefactionLeft(  octant2, octant6, gamma, isentropic, 3 );
[r_37, msg_37] = RarefactionRight( octant3, octant7, gamma, isentropic, 3 );
[r_15, msg_15] = RarefactionRight( octant1, octant5, gamma, isentropic, 3 );
[r_48, msg_48] = RarefactionLeft(  octant4, octant8, gamma, isentropic, 3 );

[r_65, msg_65] = ShockLeft(        octant6, octant5, gamma, isentropic, 1 );
[r_76, msg_76] = ShockRight(       octant7, octant6, gamma, isentropic, 2 );
[r_78, msg_78] = ShockRight(       octant7, octant8, gamma, isentropic, 1 );
[r_85, msg_85] = ShockLeft(        octant8, octant5, gamma, isentropic, 2 );

if( max( abs( r_21 ) ) > 1e-15 ) warning( msg_21 + " - 21 not fulfilled" ); end
if( max( abs( r_32 ) ) > 1e-15 ) warning( msg_32 + " - 32 not fulfilled" ); end
if( max( abs( r_34 ) ) > 1e-15 ) warning( msg_34 + " - 34 not fulfilled" ); end
if( max( abs( r_41 ) ) > 1e-15 ) warning( msg_41 + " - 41 not fulfilled" ); end

if( max( abs( r_26 ) ) > 1e-15 ) warning( msg_26 + " - 26 not fulfilled" ); end
if( max( abs( r_37 ) ) > 1e-15 ) warning( msg_37 + " - 37 not fulfilled" ); end
if( max( abs( r_15 ) ) > 1e-15 ) warning( msg_15 + " - 15 not fulfilled" ); end
if( max( abs( r_48 ) ) > 1e-15 ) warning( msg_48 + " - 48 not fulfilled" ); end

if( max( abs( r_65 ) ) > 1e-15 ) warning( msg_65 + " - 65 not fulfilled" ); end
if( max( abs( r_76 ) ) > 1e-15 ) warning( msg_76 + " - 76 not fulfilled" ); end
if( max( abs( r_78 ) ) > 1e-15 ) warning( msg_78 + " - 78 not fulfilled" ); end
if( max( abs( r_85 ) ) > 1e-15 ) warning( msg_85 + " - 85 not fulfilled" ); end

r = [r_21; r_32; r_34; r_41; r_26; r_37; r_15; r_48; r_65; r_76; r_78; r_85];
end
