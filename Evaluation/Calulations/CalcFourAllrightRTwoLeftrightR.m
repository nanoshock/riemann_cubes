%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;

Octant5.p = 1;
Octant6.p = 2/5;
Octant8.p = 3/20;

Octant5.rho = 1;

Octant5.u = 0;

Octant5.v = 0;

Octant5.w = 0;

%% Calculate all other values
Octant1.p = Octant6.p;
Octant2.p = Octant5.p;
Octant3.p = Octant8.p;

Octant6.rho = Octant5.rho * nthroot( Octant6.p / Octant5.p, gamma );
Octant8.rho = Octant5.rho * nthroot( Octant8.p / Octant5.p, gamma );
Octant1.rho = Octant6.rho;
Octant2.rho = Octant5.rho;
Octant3.rho = Octant8.rho;

Octant6.u = Octant5.u + PhiLR( Octant6.p, Octant6.rho, Octant5.p, Octant5.rho, gamma );
Octant1.u = Octant5.u;
Octant2.u = Octant6.u;
Octant3.u = Octant6.u;
Octant4.u = Octant5.u;
Octant7.u = Octant6.u;
Octant8.u = Octant5.u;

Octant8.v = Octant5.v + PhiLR( Octant8.p, Octant8.rho, Octant5.p, Octant5.rho, gamma );
Octant1.v = Octant5.v;
Octant2.v = Octant5.v;
Octant3.v = Octant8.v;
Octant4.v = Octant8.v;
Octant6.v = Octant5.v;
Octant7.v = Octant8.v;

Octant1.w = Octant5.w + PhiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho, gamma );
Octant2.w = Octant1.w;
Octant3.w = Octant1.w;
Octant4.w = Octant1.w;
Octant6.w = Octant5.w;
Octant7.w = Octant5.w;
Octant8.w = Octant5.w;

Octant7.rho = nthroot( ( Octant7.u / ( 2*sqrt( gamma ) / ( gamma-1 ) ) + ...
    sqrt( Octant8.p / nthroot( Octant8.p, gamma ) ) )^2, gamma-1 );
Octant7.p = Octant7.rho^gamma;
Octant4.p = Octant7.p;
Octant4.rho = Octant7.rho;
