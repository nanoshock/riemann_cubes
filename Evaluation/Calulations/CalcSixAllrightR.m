%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;

Octant1.p = 1.0;
Octant2.p = 2/5;
Octant4.p = 3/20;
Octant5.p = 2189/687;

Octant1.rho = 1.0;

Octant1.u = 0.0;

Octant1.v = 0.0;

Octant5.w = 0.0;

%% Calculate all other values
Octant6.p = 1.477526;
Octant7.p = 0.249189;
Octant8.p = 0.661374;

Octant2.rho = Octant1.rho * nthroot( Octant2.p / Octant1.p, gamma );
Octant4.rho = Octant1.rho * nthroot( Octant4.p / Octant1.p, gamma );
Octant5.rho = Octant1.rho / nthroot( Octant1.p / Octant5.p, gamma );
Octant6.rho = Octant2.rho / nthroot( Octant2.p / Octant6.p, gamma );
Octant7.rho = Octant3.rho / nthroot( Octant3.p / Octant7.p, gamma );
Octant8.rho = Octant4.rho / nthroot( Octant4.p / Octant8.p, gamma);

Octant2.u = Octant1.u + PhiLR( Octant2.p, Octant2.rho, Octant1.p, Octant1.rho, gamma );
Octant3.u = Octant2.u;
Octant4.u = Octant1.u;
Octant5.u = Octant1.u;
Octant6.u = Octant2.u;
Octant7.u = Octant2.u;
Octant8.u = Octant1.u;

Octant4.v = Octant1.v + PhiLR( Octant4.p, Octant4.rho, Octant1.p, Octant1.rho, gamma );
Octant2.v = Octant1.v;
Octant3.v = Octant4.v;
Octant5.v = Octant1.v;
Octant6.v = Octant5.v;
Octant7.v = Octant4.v;
Octant8.v = Octant4.v;

Octant1.w = Octant5.w + PhiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho, gamma );
Octant2.w = Octant1.w;
Octant3.w = Octant1.w;
Octant4.w = Octant1.w;
Octant6.w = Octant5.w;
Octant7.w = Octant5.w;
Octant8.w = Octant5.w;

Octant3.rho = nthroot( ( Octant3.u/ ((2*sqrt(gamma)) / (gamma-1)) + ...
    sqrt( Octant4.p / nthroot( Octant4.p, gamma ) ) )^2, gamma-1 );
Octant3.p = Octant3.rho * (Octant3.u / ((2*sqrt(gamma)) / (gamma-1)) + ...
    sqrt(Octant4.p / nthroot( Octant4.p, gamma )) )^2;





