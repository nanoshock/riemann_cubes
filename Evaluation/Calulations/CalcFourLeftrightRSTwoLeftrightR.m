%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;
K = 1.0;

Octant5.p   = 1.0;

Octant5.rho = 1.0;
Octant8.rho = 0.5;

Octant5.u   = 0.0;

Octant5.v   = 0.0; 

Octant1.w = 0;

%% Calculate all other values
Octant8.p   = K*Octant8.rho^gamma;
Octant7.p   = Octant5.p;
Octant6.p   = Octant8.p;
Octant1.p   = Octant6.p;
Octant2.p   = Octant7.p;
Octant3.p   = Octant6.p;
Octant4.p   = Octant7.p;

Octant1.rho = nthroot( Octant1.p, gamma ) / K;
Octant2.rho = nthroot( Octant2.p, gamma ) / K;
Octant3.rho = nthroot( Octant3.p, gamma ) / K;
Octant4.rho = nthroot( Octant4.p, gamma ) / K;
Octant6.rho = nthroot( Octant6.p, gamma ) / K;
Octant7.rho = nthroot( Octant7.p, gamma ) / K;

Octant6.u = Octant5.u + PhiLR( Octant6.p, Octant6.rho, Octant5.p, Octant5.rho, gamma );

Octant1.u = Octant5.u;
Octant2.u = Octant6.u;
Octant7.u = Octant6.u;
Octant3.u = Octant7.u;
Octant8.u = Octant5.u;
Octant4.u = Octant8.u;

Octant8.v = Octant5.v + PhiLR( Octant8.p, Octant8.rho, Octant5.p, Octant5.rho, gamma );

Octant4.v = Octant8.v;
Octant7.v = Octant8.v;
Octant6.v = Octant5.v;
Octant1.v = Octant5.v;
Octant2.v = Octant6.v;
Octant3.v = Octant7.v;

Octant5.w = Octant1.w - PsiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho );
Octant2.w = Octant1.w;
Octant3.w = Octant2.w;
Octant4.w = Octant3.w;
Octant6.w = Octant5.w;
Octant7.w = Octant6.w;
Octant8.w = Octant7.w;

