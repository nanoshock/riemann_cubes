%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;

Octant1.p = 0.4;
Octant5.p = 1.0;

Octant5.rho = 2.0;
Octant6.rho = 1.0;
Octant7.rho = 2.0;
Octant8.rho = 0.4;

Octant1.u = -0.15;
Octant3.u = 0.15;

Octant1.v = -0.10;
Octant2.v = 0.1;

Octant5.w =  0.3;
Octant6.w = -0.3;
Octant7.w =  0.3;
Octant8.w = -0.3;

%% Calculate all other values
Octant2.p = Octant1.p;
Octant3.p = Octant1.p;
Octant4.p = Octant1.p;
Octant6.p = Octant5.p;
Octant7.p = Octant5.p;
Octant8.p = Octant5.p;

Octant1.rho = Octant5.rho * PiLR( Octant1.p, Octant5.p, gamma );
Octant2.rho = Octant6.rho * PiLR( Octant2.p, Octant6.p, gamma );
Octant3.rho = Octant7.rho * PiLR( Octant3.p, Octant7.p, gamma );
Octant4.rho = Octant8.rho * PiLR( Octant4.p, Octant8.p, gamma );

Octant2.u = Octant1.u;
Octant5.u = Octant1.u;
Octant6.u = Octant1.u;
Octant4.u = Octant3.u;
Octant7.u = Octant3.u;
Octant8.u = Octant3.u;

Octant4.v = Octant1.v;
Octant5.v = Octant1.v;
Octant8.v = Octant1.v;
Octant3.v = Octant2.v;
Octant6.v = Octant2.v;
Octant7.v = Octant2.v;

Octant1.w = Octant5.w + PsiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho );
Octant2.w = Octant6.w + PsiLR( Octant2.p, Octant2.rho, Octant6.p, Octant6.rho );
Octant3.w = Octant7.w + PsiLR( Octant3.p, Octant3.rho, Octant7.p, Octant7.rho );
Octant4.w = Octant8.w + PsiLR( Octant4.p, Octant4.rho, Octant8.p, Octant8.rho );
