%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;

Octant1.p   = 1123.0 / 2539.0;
Octant2.p   = 1.0;
Octant7.p   = 0.4;

Octant2.rho = 0.8;
Octant8.rho = 1.0;

Octant1.u   = 0.1;
Octant7.u   = 0.1;

Octant1.v   = 0.1;
Octant7.v   = 0.1;

Octant1.w   = -0.1;
Octant7.w   = -0.1;

%% Calculate all other values
Octant3.p   = Octant2.p;
Octant4.p   = Octant3.p;
Octant6.p   = Octant2.p;
Octant5.p   = Octant6.p;
Octant8.p   = Octant5.p;

Octant1.rho = Octant2.rho / nthroot( Octant2.p / Octant1.p, gamma );
Octant7.rho = Octant8.rho * PiLR( Octant7.p, Octant8.p, gamma );
Octant3.rho = Octant7.rho * nthroot( Octant3.p / Octant7.p, gamma );
Octant4.rho = Octant1.rho * nthroot( Octant4.p / Octant1.p, gamma );
Octant5.rho = Octant1.rho / nthroot( Octant1.p / Octant5.p, gamma );
Octant6.rho = Octant7.rho / nthroot( Octant7.p / Octant6.p, gamma );

Octant2.u = Octant1.u - PhiLR( Octant2.p, Octant2.rho, Octant1.p, Octant1.rho, gamma );
Octant4.u = Octant1.u;
Octant3.u = Octant4.u;
Octant5.u = Octant1.u;
Octant6.u = Octant5.u;
Octant8.u = Octant7.u - PsiLR( Octant7.p, Octant7.rho, Octant8.p, Octant8.rho );

Octant2.v = Octant1.v;
Octant3.v = Octant2.v;
Octant4.v = Octant1.v - PhiLR( Octant4.p, Octant4.rho, Octant1.p, Octant1.rho, gamma );
Octant5.v = Octant1.v;
Octant6.v = Octant7.v - PhiLR( Octant7.p, Octant7.rho, Octant6.p, Octant6.rho, gamma );
Octant8.v = Octant5.v;

Octant2.w = Octant1.w;
Octant3.w = Octant7.w - PhiLR( Octant3.p, Octant3.rho, Octant7.p, Octant7.rho, gamma );
Octant4.w = Octant1.w;
Octant5.w = Octant1.w - PhiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho, gamma );
Octant6.w = Octant7.w;
Octant8.w = Octant7.w;
