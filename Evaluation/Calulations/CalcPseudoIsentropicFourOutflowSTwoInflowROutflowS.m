%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gamma = 1.4;

Octant2.p   = 1.0;
Octant5.p   = 0.4;
Octant8.p   = 673.0/630.0;
%Octant2.rho = 1.0;
Octant8.rho = 1663.0/1361.0;

Octant2.u   = 0.1;
Octant8.u   = 0.1;

Octant2.v   = 0.1;
Octant8.v   = 0.1;

Octant2.w   = -0.1;
Octant8.w   = -0.1;

Octant1.p   = Octant5.p;
Octant4.p   = Octant1.p;
Octant3.p   = Octant4.p;
Octant6.p   = Octant5.p;
Octant7.p   = Octant3.p;

%Octant1.rho = Octant2.rho / nthroot( Octant2.p / Octant1.p , gamma );
%Octant3.rho = Octant2.rho * PiLR( Octant3.p, Octant2.p, gamma );
Octant4.rho = Octant8.rho * PiLR( Octant4.p, Octant8.p, gamma );
Octant5.rho = Octant8.rho / PiLR( Octant8.p, Octant5.p, gamma );
%Octant6.rho = Octant2.rho / PiLR( Octant2.p, Octant6.p, gamma );
Octant7.rho = Octant8.rho * PiLR( Octant7.p, Octant8.p, gamma );

Octant3.rho = Octant4.rho;
Octant2.rho = Octant3.rho / PiLR( Octant3.p, Octant2.p, gamma );
Octant1.rho = Octant2.rho / nthroot( Octant2.p / Octant1.p , gamma );
Octant6.rho = Octant2.rho / PiLR( Octant2.p, Octant6.p, gamma );


Octant1.u = Octant2.u + PhiLR( Octant2.p, Octant2.rho, Octant1.p, Octant1.rho, gamma );
Octant3.u = Octant2.u;
Octant4.u = Octant3.u;
Octant5.u = Octant8.u;
Octant6.u = Octant5.u;
Octant7.u = Octant8.u + PsiLR( Octant7.p, Octant7.rho, Octant8.p, Octant8.rho );

Octant1.v = Octant2.v;
Octant3.v = Octant2.v + PsiLR( Octant3.p, Octant3.rho, Octant2.p, Octant2.rho );
Octant4.v = Octant1.v;
Octant5.v = Octant8.v - PsiLR( Octant8.p, Octant8.rho, Octant5.p, Octant5.rho );
Octant6.v = Octant2.v;
Octant7.v = Octant8.v;

Octant1.w = Octant2.w;
Octant3.w = Octant2.w;
Octant4.w = Octant8.w + PsiLR( Octant4.p, Octant4.rho, Octant8.p, Octant8.rho );
Octant5.w = Octant1.w;
Octant6.w = Octant2.w - PsiLR( Octant2.p, Octant2.rho, Octant6.p, Octant6.rho );
Octant7.w = Octant8.w;
