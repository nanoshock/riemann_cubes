%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gamma = 1.4;

Octant3.p = 1.1;
Octant3.rho = 1.1;

Octant4.p = 0.35;
Octant4.rho = Octant3.rho / PiLR( Octant3.p, Octant4.p, gamma );

Octant1.p = Octant4.p;
Octant2.p = Octant3.p;
Octant6.p = Octant2.p;
Octant5.p = Octant6.p;
Octant8.p = Octant5.p;

Octant8.rho = Octant3.rho;

Octant7.p = 7.0 / 20.0;
Octant7.rho = 352.0 / 695.0;

Octant6.rho = Octant7.rho / nthroot( Octant7.p / Octant6.p, gamma );

Octant4.rho = Octant7.rho;

Octant3.v = 0;
Octant4.v = Octant3.v;

Octant1.rho =  ( 352.0 * ( 7.0^(6.0/7.0) * 22.0^(1.0/7.0) - 7.0) * ( 7.0^(6.0/7.0) * 22.0^(1.0/7.0) - 7.0) ) / 1125;
Octant2.rho = Octant1.rho * nthroot( Octant2.p / Octant1.p, gamma );
Octant5.rho = Octant1.rho / PiLR( Octant1.p, Octant5.p, gamma );


% Here we try to flip the signs of the sliplines
Octant2.u = -0.0;
Octant4.u = -0.0;
Octant3.u = Octant4.u + PsiLR( Octant3.p, Octant3.rho, Octant4.p, Octant4.rho );
Octant1.u = Octant2.u + PhiLR( Octant2.p, Octant2.rho, Octant1.p, Octant1.rho, gamma );
Octant5.u = Octant1.u;
Octant6.u = Octant5.u;
Octant7.u = Octant6.u;
Octant8.u = Octant4.u;

Octant1.v = Octant4.v;
Octant2.v = Octant1.v;
Octant5.v = Octant1.v;
Octant8.v = Octant5.v;
Octant7.v = Octant8.v;
Octant6.v = Octant7.v - PhiLR( Octant7.p, Octant7.rho, Octant6.p, Octant6.rho, gamma );

Octant8.w = 0.0;
Octant7.w = Octant8.w;
Octant6.w = Octant7.w;
Octant2.w = Octant6.w;
Octant1.w = Octant2.w;
Octant3.w = Octant7.w + PsiLR( Octant3.p, Octant3.rho, Octant7.p, Octant7.rho );
Octant4.w = Octant3.w;
Octant5.w = Octant1.w - PsiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho );

slips_opps = [diff( [Octant8.w Octant5.w] ), diff( [Octant4.w Octant1.w] )];
slips_ropps = [diff( [Octant3.u Octant2.u] ), diff( [Octant4.u Octant1.u] )]
ThreeOutRinSOneOpposingRSOneOpposingSLeftrightS( Octant1, Octant2, Octant3, Octant4, Octant5, Octant6, Octant7, Octant8, gamma, false, true );
