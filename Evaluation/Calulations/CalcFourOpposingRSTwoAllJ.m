%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;

rho_shock = 1.5;
rho_raref = 1.0;
p_high = 1.0;
p_low = 0.4;

Octant1.p = p_low;
Octant2.p = p_low;
Octant3.p = p_low;
Octant4.p = p_low;
Octant5.p = p_high;
Octant6.p = p_high;
Octant7.p = p_high;
Octant8.p = p_high;

Octant5.rho = rho_shock;
Octant6.rho = rho_raref;
Octant7.rho = rho_shock;
Octant8.rho = rho_raref;

Octant1.u = 0.1;
Octant3.u = -0.1;

Octant1.v = -0.15;
Octant2.v = 0.15;

Octant5.w = -0.2;
Octant6.w = -0.3;
Octant7.w = -0.5;
Octant8.w =  1.2;

%% Calculate all other values
Octant1.rho = Octant5.rho * PiLR( Octant1.p, Octant5.p, gamma );
Octant2.rho = Octant6.rho * nthroot( Octant4.p / Octant6.p, gamma );
Octant3.rho = Octant7.rho * PiLR( Octant3.p, Octant7.p, gamma );
Octant4.rho = Octant8.rho * nthroot( Octant4.p / Octant8.p, gamma );

Octant2.u = Octant1.u;
Octant5.u = Octant1.u;
Octant6.u = Octant1.u;
Octant4.u = Octant3.u;
Octant7.u = Octant3.u;
Octant8.u = Octant3.u;

Octant4.v = Octant1.v;
Octant5.v = Octant1.v;
Octant8.v = Octant1.v;
Octant3.v = Octant2.v;
Octant6.v = Octant2.v;
Octant7.v = Octant2.v;

Octant1.w = Octant5.w + PsiLR( Octant1.p, Octant1.rho, Octant5.p, Octant5.rho );
Octant2.w = Octant6.w + PhiLR( Octant2.p, Octant2.rho, Octant6.p, Octant6.rho, gamma );
Octant3.w = Octant7.w + PsiLR( Octant3.p, Octant3.rho, Octant7.p, Octant7.rho );
Octant4.w = Octant8.w + PhiLR( Octant4.p, Octant4.rho, Octant8.p, Octant8.rho, gamma );
