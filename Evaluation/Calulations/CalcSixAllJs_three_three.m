%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given Values
gamma = 1.4;

p = 1.0;

rho_high = 2;
rho_low = 0.5;
rho_middle = 1.0;

Octant1.p = p;
Octant2.p = p;
Octant3.p = p;
Octant4.p = p;
Octant5.p = p;
Octant6.p = p;
Octant7.p = p;
Octant8.p = p;

Octant1.rho = rho_middle;
Octant2.rho = rho_low;
Octant3.rho = rho_high;
Octant4.rho = rho_low;
Octant5.rho = rho_low;
Octant6.rho = rho_high;
Octant7.rho = rho_low;
Octant8.rho = rho_middle;

Octant1.u =  0.25;
Octant3.u = -0.25;
Octant5.u = -0.25;
Octant7.u =  0.25;

Octant1.v = -0.25;
Octant2.v =  0.25;
Octant5.v = -0.5;
Octant6.v =  0.5;

Octant1.w = -0.5;
Octant2.w = -0.25;
Octant3.w =  0.25;
Octant4.w = -0.25;

%% Calculate all other values
Octant2.u = Octant1.u;
Octant4.u = Octant3.u;
Octant6.u = Octant5.u;
Octant8.u = Octant7.u;

Octant4.v = Octant1.v;
Octant3.v = Octant2.v;
Octant7.v = Octant6.v;
Octant8.v = Octant5.v;

Octant5.w = Octant1.w;
Octant6.w = Octant2.w;
Octant7.w = Octant3.w;
Octant8.w = Octant4.w;
