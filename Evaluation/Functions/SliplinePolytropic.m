%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function r = SliplinePolytropic( octant_left, octant_right, direction )
 
 [upsilon_l, nu_l, omega_l] = DirectionOrderedVelocities( octant_left,  direction );
 [upsilon_r, nu_r, omega_r] = DirectionOrderedVelocities( octant_right, direction );

 r( 1 ) = octant_left.p - octant_right.p;
 if( abs( octant_left.rho - octant_right.rho ) < 1e-04 ) r( 2 ) = 10000; else r( 2 ) = 0; end
 r( 3 ) = upsilon_l - upsilon_r;
 
 if( abs( nu_l - nu_r ) < 1e-04 && abs( omega_l - omega_r ) < 1e-04 )
     r( 4 ) = 10000;
 else
     r( 4 ) = 0;
 end
 r( 5 ) = 0; % Needed for concatination with Shockwaves or Rarefactions
 r( 6 ) = 0; % Needed for concatination with Shockwaves or Rarefactions
 if( octant_left.p < 0 )    r(  7 ) = 10000; else r(  7 ) = 0; end
 if( octant_right.p < 0 )   r(  8 ) = 10000; else r(  8 ) = 0; end
 if( octant_left.rho < 0 )  r(  9 ) = 10000; else r(  9 ) = 0; end
 if( octant_right.rho < 0 ) r( 10 ) = 10000; else r( 10 ) = 0; end
end

