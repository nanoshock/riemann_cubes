%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   %
% Fluid Mechanics, Technical University of Munich.                                       %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% LICENSE                                                                                %
%                                                                                        %
% Copyright (C) 2021 Nikolaus A. Adams and contributors                                  %
%                                                                                        %
% This program is free software: you can redistribute it and/or modify it under          %
% the terms of the GNU General Public License as published by the Free Software          %
% Foundation version 3.                                                                  %
%                                                                                        %
% This program is distributed in the hope that it will be useful, but WITHOUT ANY        %
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        %
% PARTICULAR PURPOSE. See the GNU General Public License for more details.               %
%                                                                                        %
% You should have received a copy of the GNU General Public License along with           %
% this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% CONTACT                                                                                %
%                                                                                        %
% nanoshock@aer.mw.tum.de                                                                %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                        %
% Munich, September 15th, 2020                                                           %
%                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [r, desciption] = RarefactionRight( octant_left, octant_right, gamma, isentropic, direction )

 [upsilon_l, nu_l, omega_l] = DirectionOrderedVelocities( octant_left,  direction );
 [upsilon_r, nu_r, omega_r] = DirectionOrderedVelocities( octant_right, direction );

  r( 1 ) = ( upsilon_l - upsilon_r ) - PhiLR( octant_left.p, octant_left.rho, octant_right.p, octant_right.rho, gamma );
  r( 2 ) = RarefactionPressureDensityRelation( octant_left.p, octant_left.rho, octant_right.p, octant_right.rho, gamma, isentropic );
  if( upsilon_l < upsilon_r ) r( 3 ) = 0; else r( 3 ) = 10000; end
  r( 4 ) = nu_l - nu_r;
  r( 5 ) = omega_l - omega_r;
  if( octant_left.p < octant_right.p ) r( 6 ) = 0; else r( 6 ) = 10000; end
  if( octant_left.p < 0 )    r(  7 ) = 10000; else r(  7 ) = 0; end
  if( octant_right.p < 0 )   r(  8 ) = 10000; else r(  8 ) = 0; end
  if( octant_left.rho < 0 )  r(  9 ) = 10000; else r(  9 ) = 0; end
  if( octant_right.rho < 0 ) r( 10 ) = 10000; else r( 10 ) = 0; end
  
  if( isentropic )
    desciption = "Isentropic ";
  else
    desciption = "";
  end
 
  desciption = append( desciption, "Rarfaction Right" );
end


