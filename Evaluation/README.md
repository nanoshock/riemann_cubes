# Evaluation

This directory contains a collection of MATLAB files, which can be used to find inital conditions for the found cases. Note, MATLAB is properity software. In order to run the scripts a respective license has to be obtained.

## Directory Structure
The subdirectories are organized as follows:
- Functions: Contains the re-occuring functions used in the calculations, eg. $`\Psi, \Pi,`$ or $`\Phi`$.
- Calculations: Contains scripts that compute the inital conditions of the cases provided in the repository according to the desciption of the respective case.
- CaseCheckers: Contains scripts that verify if a set of inital conditions satisfies the equations of the respective case.
