cmake_minimum_required( VERSION 3.10 )

project( Riemaenner )

set( CMAKE_CXX_STANDARD 17 )
set( STANDARD_FLAGS "-m64 -g" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${STANDARD_FLAGS}" )


INCLUDE_DIRECTORIES( Src )

INCLUDE_DIRECTORIES(../Catch2/single_include/catch2)

file( GLOB_RECURSE SOURCE_FILES "Src/*.cpp" )
list( FILTER SOURCE_FILES EXCLUDE REGEX ".*test_.*.cpp$" )

add_executable( combinator ${SOURCE_FILES} )
target_compile_definitions( combinator PUBLIC TEST_VIRTUAL=)
target_compile_options( combinator PUBLIC -O3 )
target_compile_options( combinator PRIVATE -Wall -Wextra -pedantic )

file( GLOB_RECURSE TEST_FILES "Src/*.cpp" )
list( FILTER TEST_FILES EXCLUDE REGEX "Src/main.cpp$")

add_executable( testsuit ${TEST_FILES} )
target_compile_definitions( testsuite PUBLIC TEST_VIRTUAL=virtual)
target_compile_options( testsuit PUBLIC -O0 )
target_compile_options( testsuit PRIVATE -Wall -Wextra -pedantic )
