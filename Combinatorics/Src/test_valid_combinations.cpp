/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <numeric>
#include <catch.hpp>

#include "valid_combinations_rarefaction_only.h"
#include "valid_combinations.h"

#include "relation_checker.h"

SCENARIO( "Stored rarefaction only combinations are proper" ) {
   GIVEN( "Rarefaction combinations" ) {
      auto const [count, found, valid] = StoredValidRarefactionCombinations();
      THEN( "4096 combinations have be tested" ) {
         REQUIRE( count == 4096);
      }
      THEN( "38 combinations are valid" ) {
         REQUIRE( found == 38 );
      }
      WHEN( "The relations are rechecked" ) {
         bool const correct = std::accumulate( std::cbegin( valid ),
                                               std::cend( valid ),
                                               true,
                                               []( bool const partial_result, auto const& relation ) { return partial_result && std::get<0>( CheckRelation( relation ) ); }
                                             );
         THEN( "All relations pass" ) {
            REQUIRE( correct );
         }
      }
   }
}

SCENARIO( "Stored combinations are proper", "[.long]" ) {
   GIVEN( "Combinations" ) {
      auto const [count, found, valid] = StoredValidCombinations();
      THEN( "244140625 combinations have be tested" ) {
         REQUIRE( count == 244140625);
      }
      THEN( "9161 combinations are valid" ) {
         REQUIRE( found == 9161 );
      }
      WHEN( "The relations are rechecked" ) {
         bool const correct = std::accumulate( std::cbegin( valid ),
                                               std::cend( valid ),
                                               true,
                                               []( bool const partial_result, auto const& relation ) { return partial_result && std::get<0>( CheckRelation( relation ) ); }
                                             );
         THEN( "All relations pass" ) {
            REQUIRE( correct );
         }
      }
   }
}
