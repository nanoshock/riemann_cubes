/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <vector>
#include <cstdint>
#include "relation_face_map.h"

/* Relations:
   0: W_21  1: W_32  2: W_34  3: W_41   4: W_26   5: W_37
   6: W_15  7: W_48  8: W_65  9: W_76  10: W_78  11: W_85

   Faces:
   West:   W_76 W_37 W_32 W_26
   East:   W_85 W_48 W_41 W_15
   South:  W_78 W_37 W_34 W_48
   North:  W_65 W_26 W_21 W_15
   Bottom: W_21 W_32 W_34 W_41
   Top:    W_65 W_76 W_78 W_85
*/

face_t MapRelationToFace( relation_t const& relation, FaceName const face ) {
   switch( face ) {
      case FaceName::West : {
         return {relation[9], relation[5], relation[1], relation[4]};
      }
      case FaceName::East : {
         return {relation[11], relation[7], relation[3], relation[6]};
      }
      case FaceName::South : {
         return {relation[10], relation[5], relation[2], relation[7]};
      }
      case FaceName::North : {
         return {relation[8], relation[4], relation[0], relation[6]};
      }
      case FaceName::Bottom : {
         return {relation[0], relation[1], relation[2], relation[3]};
      }
      case FaceName::Top : {
         return {relation[8], relation[9], relation[10], relation[11]};
      }
      default : {
         return {42};
      }
   }
}