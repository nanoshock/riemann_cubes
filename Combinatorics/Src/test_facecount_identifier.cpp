/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "facecount_identifier.h"

#include "cube_generator.h"
#include "face_types.h"

SCENARIO( "Groups are correctly identified" ) {
    GIVEN( "Two relations of different groups" ) {
        auto const six_right_r = GenerateValidCube( ValidCubeName::SixRightR );
        auto const six_left_s  = GenerateValidCube( ValidCubeName::SixLeftS  );
        WHEN( "The relations are grouped" ) {
            std::vector<relation_t> combination = { six_right_r, six_left_s };
            auto const groups = FacecountsInCombinations( combination );
            THEN( "We get the two expected groups" ) {
                std::vector<FaceType> const face_count_six_right_r( 6, FaceType::AllRightR );
                std::vector<FaceType> const face_count_six_left_s(  6, FaceType::AllLeftS );
                REQUIRE( groups.size() == 2 );
                REQUIRE( groups[0] == face_count_six_right_r );
                REQUIRE( groups[1] == face_count_six_left_s );
            }
        }
    }
    GIVEN( "Three identical (two but rotated) relations" ) {
        auto const six_right_r_one = GenerateValidCube( ValidCubeName::SixRightR );
        auto const six_right_r_two = GenerateValidCube( ValidCubeName::SixRightR );
        auto const six_left_r      = GenerateValidCube( ValidCubeName::SixLeftR );
        WHEN( "The relations are grouped" ) {
            std::vector<relation_t> combination = { six_right_r_one, six_right_r_two, six_left_r };
            auto const groups = FacecountsInCombinations( combination );
            THEN( "We get a single group" ) {
                std::vector<FaceType> const face_count( 6, FaceType::AllRightR );
                REQUIRE( groups.size() == 1 );
                REQUIRE( groups[0] == face_count );
            }
        }
    }
}