/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "relation_facecount_map.h"

#include <vector>
#include "cube_generator.h"
#include "face_types.h"

SCENARIO( "Known relations are correctly mapped to face counts" ) {
    GIVEN( "Some known relations" ) {
        auto const six_right_r                       = GenerateValidCube( ValidCubeName::SixRightR );
        auto const four_right_r_two_leftright_r      = GenerateValidCube( ValidCubeName::FourRightRTwoLeftRightR );
        auto const three_right_r_three_leftright_r   = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
        auto const six_leftright_r                   = GenerateValidCube( ValidCubeName::SixLeftRightR );
        auto const six_left_s                        = GenerateValidCube( ValidCubeName::SixLeftS );
        auto const four_left_s_two_leftright_s       = GenerateValidCube( ValidCubeName::FourLeftSTwoLeftRightS );
        auto const three_left_s_three_leftright_s    = GenerateValidCube( ValidCubeName::ThreeLeftSThreeLeftRightS );
        auto const six_leftright_s                   = GenerateValidCube( ValidCubeName::SixLeftRightS );
        auto const six_all_j                         = GenerateValidCube( ValidCubeName::SixAllJ );
        auto const four_leftright_rs_tow_leftright_s = GenerateValidCube( ValidCubeName::FourLeftRightRSTwoLeftRightS );
        auto const four_opposing_s_two_left_s        = GenerateValidCube( ValidCubeName::FourOppSTwoLeftS );
        WHEN( "They are mapped to a facecount" ) {
            auto const six_right_r_mapped                       = MapRelationToFaceCount( six_right_r );
            auto const four_right_r_two_leftright_r_mapped      = MapRelationToFaceCount( four_right_r_two_leftright_r );
            auto const three_right_r_three_leftright_r_mapped   = MapRelationToFaceCount( three_right_r_three_leftright_r );
            auto const six_leftright_r_mapped                   = MapRelationToFaceCount( six_leftright_r );
            auto const six_left_s_mapped                        = MapRelationToFaceCount( six_left_s );
            auto const four_left_s_two_leftright_s_mapped       = MapRelationToFaceCount( four_left_s_two_leftright_s );
            auto const three_left_s_three_leftright_s_mapped    = MapRelationToFaceCount( three_left_s_three_leftright_s );
            auto const six_leftright_s_mapped                   = MapRelationToFaceCount( six_leftright_s );
            auto const six_all_j_mapped                         = MapRelationToFaceCount( six_all_j );
            auto const four_leftright_rs_tow_leftright_s_mapped = MapRelationToFaceCount( four_leftright_rs_tow_leftright_s );
            auto const four_opposing_s_two_left_s_mapped        = MapRelationToFaceCount( four_opposing_s_two_left_s );
            THEN( "The map fits the expectaionts" ) {
                std::vector<FaceType> const six_right_r_expected                       = { FaceType::AllRightR,   FaceType::AllRightR,   FaceType::AllRightR,
                                                                                           FaceType::AllRightR,   FaceType::AllRightR,   FaceType::AllRightR };
                std::vector<FaceType> const four_right_r_two_leftright_r_expected      = { FaceType::AllRightR,   FaceType::AllRightR,   FaceType::AllRightR,
                                                                                           FaceType::AllRightR,   FaceType::LeftrightR,  FaceType::LeftrightR};
                std::vector<FaceType> const three_right_r_three_leftright_r_expected   = { FaceType::AllRightR,   FaceType::AllRightR,   FaceType::AllRightR,
                                                                                           FaceType::LeftrightR,  FaceType::LeftrightR,  FaceType::LeftrightR};
                std::vector<FaceType> const six_leftright_r_expected                   = { FaceType::LeftrightR,  FaceType::LeftrightR,  FaceType::LeftrightR,
                                                                                           FaceType::LeftrightR,  FaceType::LeftrightR,  FaceType::LeftrightR};
                std::vector<FaceType> const six_left_s_expected                        = { FaceType::AllLeftS,    FaceType::AllLeftS,    FaceType::AllLeftS,
                                                                                           FaceType::AllLeftS,    FaceType::AllLeftS,    FaceType::AllLeftS};
                std::vector<FaceType> const four_left_s_two_leftright_s_expected       = { FaceType::AllLeftS,    FaceType::AllLeftS,    FaceType::AllLeftS,
                                                                                           FaceType::AllLeftS,    FaceType::LeftrightS,  FaceType::LeftrightS};
                std::vector<FaceType> const three_left_s_three_leftright_s_expected    = { FaceType::AllLeftS,    FaceType::AllLeftS,    FaceType::AllLeftS,
                                                                                           FaceType::LeftrightS,  FaceType::LeftrightS,  FaceType::LeftrightS};
                std::vector<FaceType> const six_leftright_s_expected                   = { FaceType::LeftrightS,  FaceType::LeftrightS,  FaceType::LeftrightS,
                                                                                           FaceType::LeftrightS,  FaceType::LeftrightS,  FaceType::LeftrightS};
                std::vector<FaceType> const six_all_j_expected                         = { FaceType::AllJ,        FaceType::AllJ,        FaceType::AllJ,
                                                                                           FaceType::AllJ,        FaceType::AllJ,        FaceType::AllJ};
                std::vector<FaceType> const four_leftright_rs_tow_leftright_s_expected = { FaceType::LeftrightRS, FaceType::LeftrightRS, FaceType::LeftrightRS,
                                                                                           FaceType::LeftrightRS, FaceType::LeftrightS,  FaceType::LeftrightS};
                std::vector<FaceType> const four_opposing_s_two_left_s_expected        = { FaceType::AllLeftS,    FaceType::AllLeftS,    FaceType::OpposingSJ,
                                                                                           FaceType::OpposingSJ, FaceType::OpposingSJ,   FaceType::OpposingSJ};
                REQUIRE( six_right_r_mapped                       == six_right_r_expected );
                REQUIRE( four_right_r_two_leftright_r_mapped      == four_right_r_two_leftright_r_expected );
                REQUIRE( three_right_r_three_leftright_r_mapped   == three_right_r_three_leftright_r_expected );
                REQUIRE( six_leftright_r_mapped                   == six_leftright_r_expected );
                REQUIRE( six_left_s_mapped                        == six_left_s_expected );
                REQUIRE( four_left_s_two_leftright_s_mapped       == four_left_s_two_leftright_s_expected );
                REQUIRE( three_left_s_three_leftright_s_mapped    == three_left_s_three_leftright_s_expected );
                REQUIRE( six_leftright_s_mapped                   == six_leftright_s_expected );
                REQUIRE( six_all_j_mapped                         == six_all_j_expected );
                REQUIRE( four_leftright_rs_tow_leftright_s_mapped == four_leftright_rs_tow_leftright_s_expected );
                REQUIRE( four_opposing_s_two_left_s_mapped        == four_opposing_s_two_left_s_expected );
            }
        }
    }
}