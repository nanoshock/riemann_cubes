/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "relation_facecount_map.h"

#include <algorithm>
#include "facename_facetype_map.h"
#include "relation_face_map.h"
#include "face_checker.h"

namespace{
    std::vector<FaceName> all_faces = { FaceName::West, FaceName::East, FaceName::South,
                                        FaceName::North, FaceName::Bottom, FaceName::Top
    };
}

std::vector<FaceType> MapRelationToFaceCount( relation_t const& relation ) {
    std::vector<FaceType> face_count;
    face_count.reserve( 6 );
    for( FaceName const side : all_faces ) {
        auto const face = MapRelationToFace( relation, side );
        auto const face_name = std::get<1>( CheckFace( face ) );
        face_count.push_back(  MapFaceNameToFaceType( face_name ) );
    }
    std::sort( std::begin( face_count ), std::end( face_count ) );
    return face_count;
}