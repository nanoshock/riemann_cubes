/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#ifndef GROUP_NAMES_H
#define GROUP_NAMES_H

enum class GroupName {
   SixAllRightR
   ,TwoLeftrightRFourAllRightR
   ,ThreeLeftrightRThreeAllRightR
   ,ThreeInflowRJThreeAllRightR
   ,ThreeOutflowRJThreeAllRightR
   ,OneOutflowRJTwoInflowRJOneLeftrightRTwoAllRightR
   ,TwoOutflowRJOneInflowRJOneLeftrightRTwoAllRightR
   ,TwoOpposingRJTwoInflowRJTwoAllRightR
   ,TwoOpposingRSJTwoInflowRJTwoAllRightR
   ,TwoOpposingRJTwoOutflowRJTwoAllRightR
   ,TwoOpposingRSJTwoOutflowRJTwoAllRightR
   ,FourOpposingRJTwoAllRightR
   ,FourOpposingRSJOneAllLeftSOneAllRightR
   ,TwoOutflowRJTwoInflowRJOneAllJOneAllRightR
   ,TwoOutRInSJTwoInflowRJOneAllJOneAllRightR
   ,TwoInROutSJTwoOutflowRJOneAllJOneAllRightR
   ,TwoOutRInSJTwoInROutSJOneAllJOneAllRightR
   ,TwoOpposingRJOneOutflowRJTwoInflowRJOneAllRightR
   ,OneOpposingRSJOneOutRInSJOneOpposingRJTwoInflowRJOneAllRightR
   ,TwoOpposingRSJOneInflowSJTwoInflowRJOneAllRightR
   ,TwoOpposingRJTwoOutflowRJOneInflowRJOneAllRightR
   ,TwoOutRInSJTwoOpposingRJOneInflowRJOneAllRightR
   ,OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneAllRightR
   ,TwoOpposingRSJOneOutflowSJTwoOutflowRJOneAllRightR
   ,TwoInROutSJTwoOpposingRJOneOutflowRJOneAllRightR
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllRightR
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllRightR
   ,TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllRightR
   ,TwoOpposingRSJTwoInROutSJOneInflowSJOneAllRightR
   ,SixLeftrightR
   ,ThreeInflowRJThreeLeftrightR
   ,ThreeOutflowRJThreeLeftrightR
   ,FourLeftrightRSTwoLeftrightR
   ,TwoOpposingRJOneOutflowRJOneInflowRJTwoLeftrightR
   ,TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightR
   ,FourOpposingRJTwoLeftrightR
   ,TwoInROutSJOneInflowRJTwoLeftrightRSOneLeftrightR
   ,TwoOutRInSJOneOutflowRJTwoLeftrightRSOneLeftrightR
   ,OneOutRInSJOneInROutSJTwoOpposingRJOneLeftrightRSOneLeftrightR
   ,TwoOpposingRSJTwoOpposingRJOneLeftrightRSOneLeftrightR
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightRSOneLeftrightR
   ,FourOpposingRSJOneLeftrightSOneLeftrightR
   ,FourInflowRJOneAllJOneLeftrightR
   ,TwoInROutSJTwoInflowRJOneAllJOneLeftrightR
   ,FourOutflowRJOneAllJOneLeftrightR
   ,TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightR
   ,FourInROutSJOneAllJOneLeftrightR
   ,FourOutRInSJOneAllJOneLeftrightR
   ,TwoOpposingRJThreeInflowRJOneLeftrightR
   ,OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightR
   ,TwoOpposingRSJOneOutflowSJTwoInflowRJOneLeftrightR
   ,TwoInROutSJTwoOpposingRJOneInflowRJOneLeftrightR
   ,TwoOpposingRJThreeOutflowRJOneLeftrightR
   ,OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightR
   ,TwoOpposingRSJOneInflowSJTwoOutflowRJOneLeftrightR
   ,TwoOutRInSJTwoOpposingRJOneOutflowRJOneLeftrightR
   ,OneOpposingRSJThreeInROutSJOneOpposingRJOneLeftrightR
   ,OneOpposingRSJThreeOutRInSJOneOpposingRJOneLeftrightR
   ,TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightR
   ,TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightR
   ,TwoLeftrightSFourLeftrightRS
   ,TwoInROutSJOneOutflowSJOneLeftrightSTwoLeftrightRS
   ,TwoOutRInSJOneInflowSJOneLeftrightSTwoLeftrightRS
   ,TwoOpposingSJOneOutflowRJOneInflowRJTwoLeftrightRS
   ,TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightRS
   ,OneInflowSJOneOutflowSJTwoOpposingRJTwoLeftrightRS
   ,TwoOpposingSJTwoOpposingRJTwoLeftrightRS
   ,TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightRS
   ,FourOpposingRSJTwoLeftrightRS
   ,OneOutRInSJOneInROutSJTwoOpposingSJOneLeftrightSOneLeftrightRS
   ,TwoOpposingRSJTwoOpposingSJOneLeftrightSOneLeftrightRS
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightSOneLeftrightRS
   ,TwoInROutSJTwoInflowRJOneAllJOneLeftrightRS
   ,TwoInROutSJOneOutflowSJOneInflowRJOneAllJOneLeftrightRS
   ,TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightRS
   ,TwoOutRInSJOneInflowSJOneOutflowRJOneAllJOneLeftrightRS
   ,TwoInROutSJTwoOutflowSJOneAllJOneLeftrightRS
   ,TwoOutRInSJTwoInflowSJOneAllJOneLeftrightRS
   ,OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightRS
   ,OneOpposingRSJOneInROutSJOneOutflowSJOneOpposingRJOneInflowRJOneLeftrightRS
   ,TwoInROutSJOneOpposingSJOneOpposingRJOneInflowRJOneLeftrightRS
   ,OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJOneInflowRJOneLeftrightRS
   ,TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightRS
   ,OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightRS
   ,OneOpposingRSJOneOutRInSJOneInflowSJOneOpposingRJOneOutflowRJOneLeftrightRS
   ,TwoOutRInSJOneOpposingSJOneOpposingRJOneOutflowRJOneLeftrightRS
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowRJOneLeftrightRS
   ,TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightRS
   ,TwoInROutSJOneOpposingSJOneOutflowSJOneOpposingRJOneLeftrightRS
   ,TwoOutRInSJOneOpposingSJOneInflowSJOneOpposingRJOneLeftrightRS
   ,OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightRS
   ,TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightRS
   ,OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightRS
   ,TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightRS
   ,SixAllLeftS
   ,TwoLeftrightSFourAllLeftS
   ,ThreeLeftrightSThreeAllLeftS
   ,ThreeOutflowSJThreeAllLeftS
   ,ThreeInflowSJThreeAllLeftS
   ,OneInflowSJTwoOutflowSJOneLeftrightSTwoAllLeftS
   ,TwoInflowSJOneOutflowSJOneLeftrightSTwoAllLeftS
   ,TwoOpposingSJTwoOutflowSJTwoAllLeftS
   ,TwoOpposingRSJTwoOutflowSJTwoAllLeftS
   ,TwoOpposingSJTwoInflowSJTwoAllLeftS
   ,TwoOpposingRSJTwoInflowSJTwoAllLeftS
   ,FourOpposingSJTwoAllLeftS
   ,TwoInflowSJTwoOutflowSJOneAllJOneAllLeftS
   ,TwoOutRInSJTwoOutflowSJOneAllJOneAllLeftS
   ,TwoInROutSJTwoInflowSJOneAllJOneAllLeftS
   ,TwoOutRInSJTwoInROutSJOneAllJOneAllLeftS
   ,TwoOpposingRSJTwoInflowSJOneInflowRJOneAllLeftS
   ,TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllLeftS
   ,TwoOpposingRSJTwoOutflowSJOneOutflowRJOneAllLeftS
   ,TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllLeftS
   ,TwoOpposingSJOneInflowSJTwoOutflowSJOneAllLeftS
   ,OneOpposingRSJOneOutRInSJOneOpposingSJTwoOutflowSJOneAllLeftS
   ,TwoOpposingSJTwoInflowSJOneOutflowSJOneAllLeftS
   ,TwoOutRInSJTwoOpposingSJOneOutflowSJOneAllLeftS
   ,OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneAllLeftS
   ,TwoInROutSJTwoOpposingSJOneInflowSJOneAllLeftS
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllLeftS
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllLeftS
   ,SixLeftrightS
   ,ThreeOutflowSJThreeLeftrightS
   ,ThreeInflowSJThreeLeftrightS
   ,TwoOpposingSJOneInflowSJOneOutflowSJTwoLeftrightS
   ,TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightS
   ,FourOpposingSJTwoLeftrightS
   ,FourOutflowSJOneAllJOneLeftrightS
   ,TwoInROutSJTwoOutflowSJOneAllJOneLeftrightS
   ,FourInflowSJOneAllJOneLeftrightS
   ,TwoOutRInSJTwoInflowSJOneAllJOneLeftrightS
   ,FourInROutSJOneAllJOneLeftrightS
   ,FourOutRInSJOneAllJOneLeftrightS
   ,TwoOpposingRSJTwoOutflowSJOneInflowRJOneLeftrightS
   ,TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightS
   ,TwoOpposingRSJTwoInflowSJOneOutflowRJOneLeftrightS
   ,TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightS
   ,TwoOpposingSJThreeOutflowSJOneLeftrightS
   ,OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightS
   ,TwoInROutSJTwoOpposingSJOneOutflowSJOneLeftrightS
   ,TwoOpposingSJThreeInflowSJOneLeftrightS
   ,OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightS
   ,TwoOutRInSJTwoOpposingSJOneInflowSJOneLeftrightS
   ,OneOpposingRSJThreeInROutSJOneOpposingSJOneLeftrightS
   ,OneOpposingRSJThreeOutRInSJOneOpposingSJOneLeftrightS
   ,SixAllJ
   ,ThreeInflowRJThreeAllJ
   ,TwoInROutSJOneInflowRJThreeAllJ
   ,ThreeOutflowRJThreeAllJ
   ,TwoOutRInSJOneOutflowRJThreeAllJ
   ,ThreeOutflowSJThreeAllJ
   ,TwoInROutSJOneOutflowSJThreeAllJ
   ,ThreeInflowSJThreeAllJ
   ,TwoOutRInSJOneInflowSJThreeAllJ
   ,TwoOpposingRJTwoInflowRJTwoAllJ
   ,OneOpposingRSJOneInROutSJOneOpposingRJOneInflowRJTwoAllJ
   ,TwoOpposingRSJOneOutflowSJOneInflowRJTwoAllJ
   ,TwoOpposingRJTwoOutflowRJTwoAllJ
   ,OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoAllJ
   ,TwoOpposingRSJOneInflowSJOneOutflowRJTwoAllJ
   ,FourOpposingRJTwoAllJ
   ,TwoOpposingRSJTwoOpposingRJTwoAllJ
   ,TwoInROutSJOneOpposingSJOneOpposingRJTwoAllJ
   ,TwoOutRInSJOneOpposingSJOneOpposingRJTwoAllJ
   ,TwoOpposingRSJOneOpposingSJOneOpposingRJTwoAllJ
   ,TwoOpposingSJTwoOutflowSJTwoAllJ
   ,OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJTwoAllJ
   ,TwoOpposingSJTwoInflowSJTwoAllJ
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoAllJ
   ,FourOpposingSJTwoAllJ
   ,TwoOpposingRSJTwoOpposingSJTwoAllJ
   ,TwoOpposingRSJTwoInROutSJTwoAllJ
   ,TwoOpposingRSJTwoOutRInSJTwoAllJ
   ,FourOpposingRSJTwoAllJ
   ,TwoOpposingRJOneOutflowRJTwoInflowRJOneAllJ
   ,TwoOpposingRSJOneOutflowRJTwoInflowRJOneAllJ
   ,TwoOpposingRJTwoOutflowRJOneInflowRJOneAllJ
   ,TwoOpposingRSJTwoOutflowRJOneInflowRJOneAllJ
   ,OneOpposingRSJOneInROutSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ
   ,OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ
   ,OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ
   ,OneOutRInSJOneInROutSJTwoOpposingRJOneInflowRJOneAllJ
   ,OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneInflowRJOneAllJ
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowSJOneInflowRJOneAllJ
   ,TwoOutRInSJTwoOpposingSJOneInflowRJOneAllJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowRJOneAllJ
   ,TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllJ
   ,OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneAllJ
   ,OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneAllJ
   ,OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowRJOneAllJ
   ,TwoInROutSJTwoOpposingSJOneOutflowRJOneAllJ
   ,TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneAllJ
   ,TwoOutRInSJOneOutflowSJTwoOpposingRJOneAllJ
   ,TwoInROutSJOneInflowSJTwoOpposingRJOneAllJ
   ,OneOpposingRSJOneInROutSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ
   ,OneOpposingRSJOneOutRInSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllJ
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllJ
   ,TwoOpposingSJOneInflowSJTwoOutflowSJOneAllJ
   ,TwoOpposingRSJOneInflowSJTwoOutflowSJOneAllJ
   ,TwoOpposingSJTwoInflowSJOneOutflowSJOneAllJ
   ,TwoOpposingRSJTwoInflowSJOneOutflowSJOneAllJ
   ,OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ
   ,OneOutRInSJOneInROutSJTwoOpposingSJOneOutflowSJOneAllJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneAllJ
   ,TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllJ
   ,OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneAllJ
   ,TwoOpposingRSJTwoInROutSJOneInflowSJOneAllJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneAllJ
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllJ
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllJ
   ,SixInflowRJ
   ,TwoInROutSJFourInflowRJ
   ,ThreeOutflowRJThreeInflowRJ
   ,TwoOutRInSJOneOutflowRJThreeInflowRJ
   ,ThreeOutflowSJThreeInflowRJ
   ,TwoInROutSJOneOutflowSJThreeInflowRJ
   ,ThreeInflowSJThreeInflowRJ
   ,TwoOutRInSJOneInflowSJThreeInflowRJ
   ,TwoOpposingRJTwoOutflowRJTwoInflowRJ
   ,OneOutRInSJOneInROutSJTwoOutflowRJTwoInflowRJ
   ,OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoInflowRJ
   ,TwoOutRInSJOneOutflowSJOneOutflowRJTwoInflowRJ
   ,FourInROutSJTwoInflowRJ
   ,TwoOpposingRSJTwoOutRInSJTwoInflowRJ
   ,TwoInROutSJThreeOutflowRJOneInflowRJ
   ,OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneInflowRJ
   ,TwoInROutSJOneInflowSJTwoOutflowRJOneInflowRJ
   ,OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneInflowRJ
   ,OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneOutflowRJOneInflowRJ
   ,OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneInflowRJ
   ,OneOutRInSJOneInROutSJOneOpposingSJOneOpposingRJOneOutflowRJOneInflowRJ
   ,OneOutRInSJOneInROutSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ
   ,TwoOpposingRSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ
   ,TwoOutRInSJTwoInROutSJOneOutflowRJOneInflowRJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneInflowRJ
   ,TwoOutRInSJOneOpposingSJOneOutflowSJOneOpposingRJOneInflowRJ
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneInflowRJ
   ,TwoInROutSJThreeOutflowSJOneInflowRJ
   ,TwoOutRInSJOneInflowSJTwoOutflowSJOneInflowRJ
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneInflowRJ
   ,FourInROutSJOneOutflowSJOneInflowRJ
   ,ThreeOutRInSJOneInROutSJOneOutflowSJOneInflowRJ
   ,TwoInROutSJThreeInflowSJOneInflowRJ
   ,TwoOutRInSJTwoInROutSJOneInflowSJOneInflowRJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneInflowRJ
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneInflowRJ
   ,SixOutflowRJ
   ,TwoOutRInSJFourOutflowRJ
   ,ThreeOutflowSJThreeOutflowRJ
   ,TwoInROutSJOneOutflowSJThreeOutflowRJ
   ,ThreeInflowSJThreeOutflowRJ
   ,TwoOutRInSJOneInflowSJThreeOutflowRJ
   ,TwoOpposingRSJTwoInROutSJTwoOutflowRJ
   ,FourOutRInSJTwoOutflowRJ
   ,TwoInROutSJOneOpposingSJOneInflowSJOneOpposingRJOneOutflowRJ
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneOutflowRJ
   ,TwoOutRInSJThreeOutflowSJOneOutflowRJ
   ,TwoInROutSJTwoInflowSJOneOutflowSJOneOutflowRJ
   ,OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOutflowRJ
   ,TwoOutRInSJTwoInROutSJOneOutflowSJOneOutflowRJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneOutflowRJ
   ,TwoOutRInSJThreeInflowSJOneOutflowRJ
   ,OneOutRInSJThreeInROutSJOneInflowSJOneOutflowRJ
   ,FourOutRInSJOneInflowSJOneOutflowRJ
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneOutflowRJ
   ,TwoOutRInSJTwoInROutSJTwoOpposingRJ
   ,OneOutRInSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOpposingRJ
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOutflowSJOneOpposingRJ
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneInflowSJOneOpposingRJ
   ,SixOutflowSJ
   ,TwoInROutSJFourOutflowSJ
   ,ThreeInflowSJThreeOutflowSJ
   ,TwoOutRInSJOneInflowSJThreeOutflowSJ
   ,TwoOpposingSJTwoInflowSJTwoOutflowSJ
   ,OneOutRInSJOneInROutSJTwoInflowSJTwoOutflowSJ
   ,OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoOutflowSJ
   ,FourInROutSJTwoOutflowSJ
   ,TwoOpposingRSJTwoOutRInSJTwoOutflowSJ
   ,TwoInROutSJThreeInflowSJOneOutflowSJ
   ,OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneOutflowSJ
   ,OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneOutflowSJ
   ,TwoOutRInSJTwoInROutSJOneInflowSJOneOutflowSJ
   ,TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneOutflowSJ
   ,OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneOutflowSJ
   ,SixInflowSJ
   ,TwoOutRInSJFourInflowSJ
   ,TwoOpposingRSJTwoInROutSJTwoInflowSJ
   ,FourOutRInSJTwoInflowSJ
   ,OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneInflowSJ
   ,TwoOutRInSJTwoInROutSJTwoOpposingSJ
   ,ThreeOutRInSJThreeInROutSJ
   ,TwoOpposingRSJTwoOutRInSJTwoInROutSJ
};

#endif //GROUP_NAMES_H