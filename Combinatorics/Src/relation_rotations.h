/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#ifndef RELATION_ROTATIONS_H
#define RELATION_ROTATIONS_H

#include <vector>
#include <cstdint>
#include "relation_aliases.h"

namespace RelationRotation {
   relation_t Bottom( relation_t const& relation );
   relation_t Bottom90( relation_t const& relation );
   relation_t Bottom180( relation_t const& relation );
   relation_t Bottom270( relation_t const& relation );

   relation_t Top( relation_t const& relation );
   relation_t Top90( relation_t const& relation );
   relation_t Top180( relation_t const& relation );
   relation_t Top270( relation_t const& relation );

   relation_t West( relation_t const& relation );
   relation_t West90( relation_t const& relation );
   relation_t West180( relation_t const& relation );
   relation_t West270( relation_t const& relation );

   relation_t East( relation_t const& relation );
   relation_t East90( relation_t const& relation );
   relation_t East180( relation_t const& relation );
   relation_t East270( relation_t const& relation );

   relation_t North( relation_t const& relation );
   relation_t North90( relation_t const& relation );
   relation_t North180( relation_t const& relation );
   relation_t North270( relation_t const& relation );

   relation_t South( relation_t const& relation );
   relation_t South90( relation_t const& relation );
   relation_t South180( relation_t const& relation );
   relation_t South270( relation_t const& relation );
}

#endif //RELATION_ROTATIONS_H