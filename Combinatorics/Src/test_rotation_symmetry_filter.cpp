/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "rotation_symmetry_filter.h"

#include "valid_combinations_rarefaction_only.h"
#include "relation_groupname_sorter.h"

#include "cube_generator.h"

SCENARIO( "Trivial set are correctly filtered" ) {
   GIVEN( "An empty set" ) {
      std::vector<relation_t> empty_set;
      WHEN( "The set is filtered for rotations" ) {
         auto const filtered = FilterOutRotations( empty_set );
         THEN( "The set is unchanged" ) {
            REQUIRE( filtered.size() == 0 );
         }
      }
   }
   GIVEN( "A set with just one element" ) {
      auto const arbitrary_cube = GenerateValidCube( ValidCubeName::ThreeLeftSThreeLeftRightS );
      std::vector<relation_t> single_element_set = { GenerateValidCube( ValidCubeName::ThreeLeftSThreeLeftRightS ) };
      WHEN( "The set is filtered for rotations" ) {
         auto const filtered = FilterOutRotations( single_element_set );
         THEN( "The set is unchanged" ) {
            REQUIRE( filtered.size() == 1 );
            REQUIRE( filtered.front() == arbitrary_cube );
         }
      }
   }
}

SCENARIO( "Cases are filtered-out if they are a rotation" ) {
   GIVEN( "A set of rotations only" ) {
      auto const sorted_groups = SortRelationsByGroupName( std::get<2>( StoredValidRarefactionCombinations() ) );
      auto const rotations_only = sorted_groups.at( GroupName::SixAllRightR );
      WHEN( "The set is filtered for rotations" ) {
         auto const filtered = FilterOutRotations( rotations_only );
         THEN( "The set contains only one element" ) {
            REQUIRE( filtered.size() == 1 );
         }
      }
   }
   GIVEN( "A set with different relations" ) {
      std::vector<relation_t> const set_with_differences = { GenerateValidCube( ValidCubeName::SixLeftR ),
                                                                                 GenerateValidCube( ValidCubeName::SixLeftRightR ),
                                                                               };
      WHEN( "The set is filtered for rotations" ) {
         auto const filtered = FilterOutRotations( set_with_differences );
         THEN( "The set still contains both elements" ) {
            REQUIRE( filtered.size() == 2 );
         }
      }
   }
   GIVEN( "A group with two distinct types and multiple rotations of these two types" ) {
      auto sorted_groups = SortRelationsByGroupName( std::get<2>( StoredValidRarefactionCombinations() ) );
      auto two_distinct_types_set = sorted_groups.at( GroupName::ThreeLeftrightRThreeAllRightR );
      WHEN( "The set is filtered for rotations" ) {
         auto const filtered = FilterOutRotations( two_distinct_types_set );
         THEN( "The set holds the two distinct relations" ) {
            REQUIRE( filtered.size() == 2 );
         }
      }
   }
}

namespace {
   std::vector<relation_t> symmetries_of_arbitrary_relation = {
      {0,1,2,2, 4,0,1,2, 3,4,0,2},
      {3,4,0,2, 4,1,0,3, 0,1,2,2},
      {1,2,3,1, 1,2,4,0, 2,2,1,4},
      {2,0,0,3, 0,4,2,1, 0,4,3,3},
      {0,1,3,3, 4,1,0,2, 4,2,3,1},
      {2,2,1,0, 2,0,1,4, 2,0,4,3},
      {4,1,0,4, 0,2,3,0, 1,2,2,2},
      {0,2,3,2, 2,1,1,3, 4,4,1,1},
      {0,0,2,2, 4,1,2,2, 3,4,0,1},
      {0,4,3,0, 0,4,3,3, 2,1,0,3}
   };

   std::vector<relation_t> symmetries_of_three_three_r = {
      {0,0,0,0, 0,0,1,0, 1,0,0,1},
      {1,0,0,1, 1,1,0,1, 0,0,0,0},
      {1,0,1,0, 1,0,0,0, 0,1,1,0},
      {0,1,0,1, 0,0,0,1, 0,1,1,0},
      {1,1,1,1, 0,1,0,0, 1,0,0,1},
      {0,0,0,0, 0,0,1,0, 1,0,0,1},
      {0,0,0,0, 0,0,1,0, 1,0,0,1},
      {0,1,1,0, 0,1,1,1, 1,0,1,0},
      {0,0,0,0, 0,0,1,0, 1,0,0,1},
      {0,1,1,0, 1,1,1,0, 0,1,0,1}
   };
}

SCENARIO( "Cases are filtered if they are symmetries" ) {
   GIVEN( "A set of symmertires only" ) {
    auto const symmetries_only = symmetries_of_arbitrary_relation;
      WHEN( "The set is filtered for symmetries" ) {
         auto const filtered = FilterOutSymmetries( symmetries_only );
         THEN( "The set contains only one element" ) {
            REQUIRE( filtered.size() == 1 );
         }
      }
   }
   GIVEN( "A set with different relations" ) {
      std::vector<relation_t> const set_with_differences = { GenerateValidCube( ValidCubeName::SixLeftR ),
                                                                                 GenerateValidCube( ValidCubeName::SixLeftRightR ),
                                                                               };
      WHEN( "The set is filtered for symmetries" ) {
         auto const filtered = FilterOutSymmetries( set_with_differences );
         THEN( "The set still contains both elements" ) {
            REQUIRE( filtered.size() == 2 );
         }
      }
   }
   GIVEN( "A group with two distinct types and multiple symmetries of these two types" ) {
      std::vector<relation_t> group_with_two_types( symmetries_of_arbitrary_relation );
      group_with_two_types.reserve( group_with_two_types.size() + symmetries_of_three_three_r.size() );
      group_with_two_types.insert( std::end( group_with_two_types ), std::cbegin( symmetries_of_three_three_r ), std::cend( symmetries_of_three_three_r ) );
      WHEN( "The set is filtered for symmetries" ) {
         auto const filtered = FilterOutSymmetries( group_with_two_types );
         THEN( "The set holds the two distinct relations" ) {
            REQUIRE( filtered.size() == 2 );
         }
      }
   }
}

SCENARIO( "Cases are filtered if they are a combination of rotation and symmetry" ) {
   GIVEN( "A comlex cube and its rotated symmetry" ) {
      auto const complex_cube = GenerateValidCube( ValidCubeName::ThreeOutRInSJOneOpposingRSJOneOpposingSJOneLeftrightS );
      relation_t top270_symmetry3_of_complex_cube = { 0,4,4,3, 4,0,2,3, 4,2,4,2 };
      WHEN( "We put these two into a set" ) {
         std::vector<relation_t> const both_relations = { complex_cube, top270_symmetry3_of_complex_cube,  };
         THEN( "The filtered list contain just a single entry" ) {
            auto const filtered_list = FilterOutSymmetryRotationCombinations( both_relations );
            REQUIRE( filtered_list.size() == 1 );
         }
      }
   }
}