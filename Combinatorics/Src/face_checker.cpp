/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "face_checker.h"

#include <array>

#include "face_generator.h"

namespace {
   constexpr std::array<ValidFaceName, 65> valid_face_names = {
      ValidFaceName::RrRrRrRr, ValidFaceName::RlRrRlRr, ValidFaceName::RlRlRlRl, ValidFaceName::RrRlRrRl,
      ValidFaceName::RrRlRlRr, ValidFaceName::RlRrRrRl,
      ValidFaceName::RrSrRlSl, ValidFaceName::SrRrSlSl, ValidFaceName::RlSlRrSr, ValidFaceName::SlRlSrRr,
      ValidFaceName::SlSlSlSl, ValidFaceName::SrSlSrSl, ValidFaceName::SrSrSrSr, ValidFaceName::SlSrSlSr,
      ValidFaceName::SlSrSrSl, ValidFaceName::SrSlSlSr,
      ValidFaceName::J_J_J_J_,
      ValidFaceName::RrJ_J_Rr, ValidFaceName::RlRrJ_J_, ValidFaceName::J_RlRlJ_, ValidFaceName::J_J_RrRl,
      ValidFaceName::RlJ_J_Rl, ValidFaceName::RrRlJ_J_, ValidFaceName::J_RrRrJ_, ValidFaceName::J_J_RlRr,
      ValidFaceName::J_RrJ_Rr, ValidFaceName::RlJ_RlJ_, ValidFaceName::J_RlJ_Rl, ValidFaceName::RrJ_RrJ_,
      ValidFaceName::SlJ_J_Sl, ValidFaceName::SrSlJ_J_, ValidFaceName::J_SrSrJ_, ValidFaceName::J_J_SlSr,
      ValidFaceName::SrJ_J_Sr, ValidFaceName::SlSrJ_J_, ValidFaceName::J_SlSlJ_, ValidFaceName::J_J_SrSl,
      ValidFaceName::J_SlJ_Sl, ValidFaceName::SrJ_SrJ_, ValidFaceName::J_SrJ_Sr, ValidFaceName::SlJ_SlJ_,
      ValidFaceName::RrJ_J_Sl, ValidFaceName::SrRrJ_J_, ValidFaceName::J_SrRlJ_, ValidFaceName::J_J_SlRl, ValidFaceName::SlJ_J_Rr, ValidFaceName::RlSlJ_J_, ValidFaceName::J_RlSrJ_, ValidFaceName::J_J_RrSr,
      ValidFaceName::RlJ_J_Sr, ValidFaceName::SlRlJ_J_, ValidFaceName::J_SlRrJ_, ValidFaceName::J_J_SrRr, ValidFaceName::SrJ_J_Rl, ValidFaceName::RrSrJ_J_, ValidFaceName::J_RrSlJ_, ValidFaceName::J_J_RlSl,
      ValidFaceName::J_SlJ_Rr, ValidFaceName::RlJ_SrJ_, ValidFaceName::J_RlJ_Sr, ValidFaceName::SlJ_RrJ_, ValidFaceName::J_SrJ_Rl, ValidFaceName::RrJ_SlJ_, ValidFaceName::J_RrJ_Sl, ValidFaceName::SrJ_RlJ_
   };
}

std::tuple<bool, ValidFaceName> CheckFace( face_t const relation ) {
   for( auto const name :  valid_face_names ) {
      if( relation == GenerateValidFace( name ) ) {
         return std::make_tuple( true, name );
      }
   }
   return std::make_tuple( false, ValidFaceName::Unkown );
}