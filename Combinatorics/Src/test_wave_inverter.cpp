/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "wave_inverter.h"

#include <cstdint>

SCENARIO( "Waves can be correctly inverted" ) {
   GIVEN( "The five wave types" ) {
      wave_t const left_r  = 0;
      wave_t const right_r = 1;
      wave_t const left_s  = 2;
      wave_t const right_s = 3;
      wave_t const j       = 4;
      WHEN( "The waves are inverted" ) {
         wave_t const inverted_left_r  = InvertWave( left_r  );
         wave_t const inverted_right_r = InvertWave( right_r );
         wave_t const inverted_left_s  = InvertWave( left_s  );
         wave_t const inverted_right_s = InvertWave( right_s );
         wave_t const inverted_j       = InvertWave( j       );
         THEN( "The results are as expected" ) {
            wave_t const expected_inverted_left_r  = 1;
            wave_t const expected_inverted_right_r = 0;
            wave_t const expected_inverted_left_s  = 3;
            wave_t const expected_inverted_right_s = 2;
            wave_t const expected_inverted_j       = 4;
            REQUIRE( inverted_left_r  == expected_inverted_left_r  );
            REQUIRE( inverted_right_r == expected_inverted_right_r );
            REQUIRE( inverted_left_s  == expected_inverted_left_s  );
            REQUIRE( inverted_right_s == expected_inverted_right_s );
            REQUIRE( inverted_j       == expected_inverted_j       );
         }
      }
   }
}