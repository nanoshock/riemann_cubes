/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "relation_checker.h"

#include <stdexcept>
#include <algorithm>
#include <tuple>
#include "relation_face_map.h"
#include "face_checker.h"
#include "stringyfier.h"

namespace{
   std::tuple<bool, std::string> InspectFace( relation_t const& relation, FaceName const fn ) {
      auto const [valid, face_name] = CheckFace( MapRelationToFace( relation, fn ) );
      std::string const face_validity = valid ? "  Valid" : "Invalid";
      std::string const message = "   " + FaceNameEnumString( fn, true ) + "-   " + FaceString( MapRelationToFace( relation, fn ) ) + ":   " + face_validity + "\n";
      return std::make_tuple( valid, message );
   }
}

std::tuple<bool, std::string> CheckRelation( relation_t const& relation ) {
   if( relation.size() != 12 ) {
      throw std::invalid_argument( "Relation must have 12 entries" );
   }
   std::string const opening_line = "Testing: " + NiceString( relation ) +"\n";

   std::vector<std::tuple<bool,std::string>> face_results;
   face_results.reserve( 6 );
   face_results.push_back( InspectFace( relation, FaceName::West   ) );
   face_results.push_back( InspectFace( relation, FaceName::East   ) );
   face_results.push_back( InspectFace( relation, FaceName::South  ) );
   face_results.push_back( InspectFace( relation, FaceName::North  ) );
   face_results.push_back( InspectFace( relation, FaceName::Bottom ) );
   face_results.push_back( InspectFace( relation, FaceName::Top    ) );

   auto const [result, message]  = std::all_of( std::cbegin( face_results ),
                                                std::cend( face_results ),
                                                []( std::tuple<bool, std::string> const e ) { return std::get<0>( e ); }
                                              )
                                 ? std::make_tuple( true, "  VALID\n" )
                                 : std::make_tuple( false, "INVALID\n" );

   std::string const face_inspection = [&face_results]() -> std::string {
      std::string result;
      std::for_each( std::cbegin( face_results ), std::cend( face_results ), [&result]( auto const in ){ result += std::get<1>( in ); } );
      return result;
   }();

   return std::make_tuple( result, opening_line + face_inspection + "   ------------------------------------\n" + "                                " + message );
}
