/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "relation_symmetries.h"

#include <vector>
#include <cstdint>
#include "cube_generator.h"

SCENARIO( "Symmetries are correct" ) {
   GIVEN( "A valid and an arbirtray reference relations" ) {
      auto const valid_relation = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
      relation_t arbitrary_relation = {0,1,2,2, 4,0,1,2, 3,4,0,2};
      WHEN( "All nine symmetries are applied" ) {
         auto const valid_symmetry_one = RelationSymmetry::SymmetryOne(   valid_relation );
         auto const valid_symmetry_two = RelationSymmetry::SymmetryTwo(   valid_relation );
         auto const valid_symmetry_thr = RelationSymmetry::SymmetryThree( valid_relation );
         auto const valid_symmetry_fou = RelationSymmetry::SymmetryFour(  valid_relation );
         auto const valid_symmetry_fiv = RelationSymmetry::SymmetryFive(  valid_relation );
         auto const valid_symmetry_six = RelationSymmetry::SymmetrySix(   valid_relation );
         auto const valid_symmetry_sev = RelationSymmetry::SymmetrySeven( valid_relation );
         auto const valid_symmetry_eig = RelationSymmetry::SymmetryEight( valid_relation );
         auto const valid_symmetry_nin = RelationSymmetry::SymmetryNine(  valid_relation );
         auto const arbitrary_symmetry_one = RelationSymmetry::SymmetryOne(   arbitrary_relation );
         auto const arbitrary_symmetry_two = RelationSymmetry::SymmetryTwo(   arbitrary_relation );
         auto const arbitrary_symmetry_thr = RelationSymmetry::SymmetryThree( arbitrary_relation );
         auto const arbitrary_symmetry_fou = RelationSymmetry::SymmetryFour(  arbitrary_relation );
         auto const arbitrary_symmetry_fiv = RelationSymmetry::SymmetryFive(  arbitrary_relation );
         auto const arbitrary_symmetry_six = RelationSymmetry::SymmetrySix(   arbitrary_relation );
         auto const arbitrary_symmetry_sev = RelationSymmetry::SymmetrySeven( arbitrary_relation );
         auto const arbitrary_symmetry_eig = RelationSymmetry::SymmetryEight( arbitrary_relation );
         auto const arbitrary_symmetry_nin = RelationSymmetry::SymmetryNine(  arbitrary_relation );
         THEN( "The obtained symmeties match the expectation" ) {
            relation_t expected_valid_one  = {1,0,0,1, 1,1,0,1, 0,0,0,0};
            relation_t expected_valid_two  = {1,0,1,0, 1,0,0,0, 0,1,1,0};
            relation_t expected_valid_thr  = {0,1,0,1, 0,0,0,1, 0,1,1,0};
            relation_t expected_valid_fou  = {1,1,1,1, 0,1,0,0, 1,0,0,1};
            relation_t expected_valid_fiv  = {0,0,0,0, 0,0,1,0, 1,0,0,1};
            relation_t expected_valid_six  = {0,0,0,0, 0,0,1,0, 1,0,0,1};
            relation_t expected_valid_sev  = {0,1,1,0, 0,1,1,1, 1,0,1,0};
            relation_t expected_valid_eig  = {0,0,0,0, 0,0,1,0, 1,0,0,1};
            relation_t expected_valid_nin  = {0,1,1,0, 1,1,1,0, 0,1,0,1};
            relation_t expected_arbitrary_one = {3,4,0,2, 4,1,0,3, 0,1,2,2};
            relation_t expected_arbitrary_two = {1,2,3,1, 1,2,4,0, 2,2,1,4};
            relation_t expected_arbitrary_thr = {2,0,0,3, 0,4,2,1, 0,4,3,3};
            relation_t expected_arbitrary_fou = {0,1,3,3, 4,1,0,2, 4,2,3,1};
            relation_t expected_arbitrary_fiv = {2,2,1,0, 2,0,1,4, 2,0,4,3};
            relation_t expected_arbitrary_six = {4,1,0,4, 0,2,3,0, 1,2,2,2};
            relation_t expected_arbitrary_sev = {0,2,3,2, 2,1,1,3, 4,4,1,1};
            relation_t expected_arbitrary_eig = {0,0,2,2, 4,1,2,2, 3,4,0,1};
            relation_t expected_arbitrary_nin = {0,4,3,0, 0,4,3,3, 2,1,0,3};
            REQUIRE( valid_symmetry_one == expected_valid_one );
            REQUIRE( valid_symmetry_two == expected_valid_two );
            REQUIRE( valid_symmetry_thr == expected_valid_thr );
            REQUIRE( valid_symmetry_fou == expected_valid_fou );
            REQUIRE( valid_symmetry_fiv == expected_valid_fiv );
            REQUIRE( valid_symmetry_six == expected_valid_six );
            REQUIRE( valid_symmetry_sev == expected_valid_sev );
            REQUIRE( valid_symmetry_eig == expected_valid_eig );
            REQUIRE( valid_symmetry_nin == expected_valid_nin );
            REQUIRE( arbitrary_symmetry_one == expected_arbitrary_one );
            REQUIRE( arbitrary_symmetry_two == expected_arbitrary_two );
            REQUIRE( arbitrary_symmetry_thr == expected_arbitrary_thr );
            REQUIRE( arbitrary_symmetry_fou == expected_arbitrary_fou );
            REQUIRE( arbitrary_symmetry_fiv == expected_arbitrary_fiv );
            REQUIRE( arbitrary_symmetry_six == expected_arbitrary_six );
            REQUIRE( arbitrary_symmetry_sev == expected_arbitrary_sev );
            REQUIRE( arbitrary_symmetry_eig == expected_arbitrary_eig );
            REQUIRE( arbitrary_symmetry_nin == expected_arbitrary_nin );
         }
      }
   }
}