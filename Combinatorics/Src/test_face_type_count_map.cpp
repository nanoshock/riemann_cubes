/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "face_type_count_map.h"

#include "cube_generator.h"
#include "relation_facecount_map.h"

namespace {
   template<typename T, typename S>
   bool AllElementsInMapHaveCountZero( std::unordered_map<T, S> map ) {
      return std::all_of( std::begin( map ), std::end( map ),
                          []( auto const& ele ){ return std::get<1>( ele ) == 0; }
                        );
   }
}

SCENARIO( "Face counts are correctly transformed to type counts" ) {
   GIVEN( "Some arbitrary face count groups" ) {
      std::vector<FaceType> six_all_right_group = MapRelationToFaceCount( GenerateValidCube( ValidCubeName::SixRightR ) );
      std::vector<FaceType> three_left_s_three_leftright_s_group = MapRelationToFaceCount( GenerateValidCube( ValidCubeName::ThreeLeftSThreeLeftRightS ) );
      WHEN( "The groups count map is generated" ) {
         auto count_map1 = CountMapOfGroup( six_all_right_group );
         auto count_map2 = CountMapOfGroup( three_left_s_three_leftright_s_group );
         THEN( "The maps hold the correct counts" ) {
            REQUIRE( count_map1.at( FaceType::AllRightR ) == 6 );
            count_map1.erase( FaceType::AllRightR );
            REQUIRE( AllElementsInMapHaveCountZero( count_map1 ) );
            REQUIRE( count_map2.at( FaceType::AllLeftS   ) == 3 );
            REQUIRE( count_map2.at( FaceType::LeftrightS ) == 3 );
            count_map2.erase( FaceType::AllLeftS );
            count_map2.erase( FaceType::LeftrightS );
            REQUIRE( AllElementsInMapHaveCountZero( count_map2 ) );
         }
      }
   }
}