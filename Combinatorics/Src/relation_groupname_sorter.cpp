/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "relation_groupname_sorter.h"
#include "relation_facecount_map.h"
#include "facecount_groupname_map.h"

namespace {
   void MoveRelationIntoMapAtEndOfGroup( relation_t const& r, std::unordered_map<GroupName, std::vector<relation_t>>& map ) {
      auto const face_count = MapRelationToFaceCount( r );
      GroupName group_name = MapFaceCountToGroupName( face_count );
      std::vector<relation_t> element;
      if( map.find( group_name ) == map.end() ) {
         element.push_back( r );
         map.emplace( std::piecewise_construct,
                      std::forward_as_tuple( std::move( group_name ) ),
                      std::forward_as_tuple( std::move( element ) )
                    );
      } else {
         auto& element = map.at( group_name );
         element.push_back( r );
      }
   }
}

std::unordered_map<GroupName, std::vector<relation_t>> SortRelationsByGroupName( std::vector<relation_t> const& relations ) {
   std::unordered_map<GroupName, std::vector<relation_t>> map;
   for( auto const& r : relations ) {
      MoveRelationIntoMapAtEndOfGroup( r, map );
   }
   return map;
}