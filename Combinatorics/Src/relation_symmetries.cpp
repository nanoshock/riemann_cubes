/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "relation_symmetries.h"

#include "wave_inverter.h"

namespace RelationSymmetry {
   relation_t SymmetryOne( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] =             relation[ 8];
      result[ 1] =             relation[ 9];
      result[ 2] =             relation[10];
      result[ 3] =             relation[11];
      result[ 4] = InvertWave( relation[ 4] );
      result[ 5] = InvertWave( relation[ 5] );
      result[ 6] = InvertWave( relation[ 6] );
      result[ 7] = InvertWave( relation[ 7] );
      result[ 8] =             relation[ 0];
      result[ 9] =             relation[ 1];
      result[10] =             relation[ 2];
      result[11] =             relation[ 3];
      return result;
   }

   relation_t SymmetryTwo( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] = InvertWave( relation[ 0] );
      result[ 1] =             relation[ 3]  ;
      result[ 2] = InvertWave( relation[ 2] );
      result[ 3] =             relation[ 1]  ;
      result[ 4] =             relation[ 6]  ;
      result[ 5] =             relation[ 7]  ;
      result[ 6] =             relation[ 4]  ;
      result[ 7] =             relation[ 5]  ;
      result[ 8] = InvertWave( relation[ 8] );
      result[ 9] =             relation[11]  ;
      result[10] = InvertWave( relation[10] );
      result[11] =             relation[ 9]  ;
      return result;
   }

   relation_t SymmetryThree( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] =             relation[ 2];
      result[ 1] = InvertWave( relation[ 1] );
      result[ 2] =             relation[ 0];
      result[ 3] = InvertWave( relation[ 3] );
      result[ 4] =             relation[ 5];
      result[ 5] =             relation[ 4];
      result[ 6] =             relation[ 7];
      result[ 7] =             relation[ 6];
      result[ 8] =             relation[10];
      result[ 9] = InvertWave( relation[ 9] );
      result[10] =             relation[ 8];
      result[11] = InvertWave( relation[11] );
      return result;
   }

   relation_t SymmetryFour( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] =  InvertWave( relation[ 1] );
      result[ 1] =  InvertWave( relation[ 0] );
      result[ 2] =  InvertWave( relation[ 3] );
      result[ 3] =  InvertWave( relation[ 2] );
      result[ 4] =              relation[ 4];
      result[ 5] =              relation[ 6];
      result[ 6] =              relation[ 5];
      result[ 7] =              relation[ 7];
      result[ 8] =  InvertWave( relation[ 9] );
      result[ 9] =  InvertWave( relation[ 8] );
      result[10] =  InvertWave( relation[11] );
      result[11] =  InvertWave( relation[10] );
      return result;
   }

   relation_t SymmetryFive( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] = relation[ 3];
      result[ 1] = relation[ 2];
      result[ 2] = relation[ 1];
      result[ 3] = relation[ 0];
      result[ 4] = relation[ 7];
      result[ 5] = relation[ 5];
      result[ 6] = relation[ 6];
      result[ 7] = relation[ 4];
      result[ 8] = relation[11];
      result[ 9] = relation[10];
      result[10] = relation[ 9];
      result[11] = relation[ 8];
      return result;
   }

   relation_t SymmetrySix( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] = relation[ 4];
      result[ 1] = relation[ 1];
      result[ 2] = relation[ 5];
      result[ 3] = relation[ 9];
      result[ 4] = relation[ 0];
      result[ 5] = relation[ 2];
      result[ 6] = relation[ 8];
      result[ 7] = relation[10];
      result[ 8] = relation[ 6];
      result[ 9] = relation[ 3];
      result[10] = relation[ 7];
      result[11] = relation[11];
      return result;
   }

   relation_t SymmetrySeven( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] = InvertWave( relation[ 6] );
      result[ 1] =             relation[11];
      result[ 2] = InvertWave( relation[ 7] );
      result[ 3] =             relation[ 3];
      result[ 4] = InvertWave( relation[ 8] );
      result[ 5] = InvertWave( relation[10] );
      result[ 6] = InvertWave( relation[ 0] );
      result[ 7] = InvertWave( relation[ 2] );
      result[ 8] = InvertWave( relation[ 4] );
      result[ 9] =             relation[ 9];
      result[10] = InvertWave( relation[ 5] );
      result[11] =             relation[ 1];
      return result;
   }

   relation_t SymmetryEight( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] = relation[10];
      result[ 1] = relation[ 5];
      result[ 2] = relation[ 2];
      result[ 3] = relation[ 7];
      result[ 4] = relation[ 9];
      result[ 5] = relation[ 1];
      result[ 6] = relation[11];
      result[ 7] = relation[ 3];
      result[ 8] = relation[ 8];
      result[ 9] = relation[ 4];
      result[10] = relation[ 0];
      result[11] = relation[ 6];
      return result;
   }

   relation_t SymmetryNine( relation_t const& relation ) {
      relation_t result;
      result.fill( 5 );
      result[ 0] =             relation[ 0];
      result[ 1] = InvertWave( relation[ 4] );
      result[ 2] =             relation[ 8];
      result[ 3] = InvertWave( relation[ 6] );
      result[ 4] = InvertWave( relation[ 1] );
      result[ 5] = InvertWave( relation[ 9] );
      result[ 6] = InvertWave( relation[ 3] );
      result[ 7] = InvertWave( relation[11] );
      result[ 8] =             relation[ 2];
      result[ 9] = InvertWave( relation[ 5] );
      result[10] =             relation[10];
      result[11] = InvertWave( relation[ 7] );
      return result;
   }
}