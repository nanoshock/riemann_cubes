/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>

#include "relation_checker.h"
#include "face_names.h"
#include "relation_face_map.h"
#include "cube_generator.h"

SCENARIO( "Relations validities are correctly determined" ) {
   GIVEN( "Some not-valid relations" ) {
      relation_t const invalid_one =   {1,1,1,1, 1,1,1,1, 1,1,1,0};;
      relation_t const invalid_two =   {0,0,0,1, 1,0,1,0, 1,0,1,1};
      relation_t const invalid_three = {1,1,0,1, 1,1,0,0, 0,0,0,1};
      relation_t const invalid_four =  {0,0,0,1, 0,0,0,0, 1,1,0,1};
      WHEN( "They are checked" ) {
         bool const result_one   = std::get<0>( CheckRelation( invalid_one ) );
         bool const result_two   = std::get<0>( CheckRelation( invalid_two ) );
         bool const result_three = std::get<0>( CheckRelation( invalid_three ) );
         bool const result_four  = std::get<0>( CheckRelation( invalid_four ) );
         THEN( "The relations are not valid" ) {
            REQUIRE_FALSE( result_one );
            REQUIRE_FALSE( result_two );
            REQUIRE_FALSE( result_three );
            REQUIRE_FALSE( result_four );
         }
      }
   }
   GIVEN( "Some valid realtions" ) {
      auto const six_right_r = GenerateValidCube( ValidCubeName::SixRightR );
      auto const four_right_r_two_leftright_r = GenerateValidCube( ValidCubeName::FourRightRTwoLeftRightR );
      auto const three_right_r_three_leftright_r = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
      auto const six_leftright_r = GenerateValidCube( ValidCubeName::SixLeftRightR );
      WHEN( "They are checked" ) {
         bool const six_right_valid     = std::get<0>( CheckRelation( six_right_r ) );
         bool const four_two_valid      = std::get<0>( CheckRelation( four_right_r_two_leftright_r ) );
         bool const three_three_valid   = std::get<0>( CheckRelation( three_right_r_three_leftright_r ) );
         bool const six_leftright_valid = std::get<0>( CheckRelation( six_leftright_r ) );
         THEN( "The relations are valid" ) {
            REQUIRE( six_right_valid );
            REQUIRE( four_two_valid );
            REQUIRE( three_three_valid );
            REQUIRE( six_leftright_valid );
         }
      }
   }
}

SCENARIO( "Relation checks give meaningful log information" ) {
   GIVEN( "One valid and one invalid relation" ) {
      relation_t const valid   = GenerateValidCube( ValidCubeName::ThreeOutRInSJOneOpposingRSJOneOpposingSJOneLeftrightS );
      relation_t const invalid = {0,0,0,0, 1,0,1,0, 1,0,1,0};
      WHEN( "They are checked" ) {
         auto const [res_val,   log_val]   = CheckRelation( valid );
         auto const [res_inval, log_inval] = CheckRelation( invalid );
         THEN( "The log messaged are as expected" ) {
            std::string const expected_valid_log = "Testing: R_l J__ S_r J__ | J__ S_r S_l S_l | J__ R_r S_l J__\n"
                                                   "   West  -   R_r S_r J__ J__:     Valid\n"
                                                   "   East  -   J__ S_l J__ S_l:     Valid\n"
                                                   "   South -   S_l S_r S_r S_l:     Valid\n"
                                                   "   North -   J__ J__ R_l S_l:     Valid\n"
                                                   "   Bottom-   R_l J__ S_r J__:     Valid\n"
                                                   "   Top   -   J__ R_r S_l J__:     Valid\n"
                                                   "   ------------------------------------\n"
                                                   "                                  VALID\n";
            std::string const expected_invalid_log = "Testing: R_l R_l R_l R_l | R_r R_l R_r R_l | R_r R_l R_r R_l\n"
                                                     "   West  -   R_l R_l R_l R_r:   Invalid\n"
                                                     "   East  -   R_l R_l R_l R_r:   Invalid\n"
                                                     "   South -   R_r R_l R_l R_l:   Invalid\n"
                                                     "   North -   R_r R_r R_l R_r:   Invalid\n"
                                                     "   Bottom-   R_l R_l R_l R_l:     Valid\n"
                                                     "   Top   -   R_r R_l R_r R_l:     Valid\n"
                                                     "   ------------------------------------\n"
                                                     "                                INVALID\n";
            REQUIRE( log_val   == expected_valid_log );
            REQUIRE( log_inval == expected_invalid_log );
         }
      }
   }
}
