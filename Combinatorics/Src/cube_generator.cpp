/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "cube_generator.h"

relation_t GenerateValidCube( ValidCubeName const vc ) {
   switch( vc ) {
      case ValidCubeName::SixRightR :
         return {1,1,1,1, 1,1,1,1, 1,1,1,1};
      case ValidCubeName::SixLeftR :
         return {0,0,0,0, 0,0,0,0, 0,0,0,0};
      case ValidCubeName::FourRightRTwoLeftRightR :
         return {1,1,1,1, 0,1,0,1, 1,0,1,0};
      case ValidCubeName::ThreeRightRThreeLeftRightR :
         return {0,0,0,0, 0,0,1,0, 1,0,0,1};
      case ValidCubeName::SixLeftRightR :
         return {1,0,0,1, 1,0,0,1, 0,1,1,0};
      case ValidCubeName::SixLeftS :
         return {2,2,2,2, 2,2,2,2, 2,2,2,2};
      case ValidCubeName::FourLeftSTwoLeftRightS :
         return {2,2,2,2, 3,2,3,2, 2,3,2,3};
      case ValidCubeName::ThreeLeftSThreeLeftRightS :
         return {3,3,3,3, 3,3,2,3, 2,3,3,2};
      case ValidCubeName::SixLeftRightS :
         return {2,3,3,2, 2,3,3,2, 3,2,2,3};
      case ValidCubeName::SixAllJ :
         return {4,4,4,4, 4,4,4,4, 4,4,4,4};
      case ValidCubeName::FourLeftRightRSTwoLeftRightS :
         return {1,3,0,2, 2,3,3,2, 0,2,1,3};
      case ValidCubeName::FourOppSTwoLeftS :
         return {2,2,2,2, 4,4,4,4, 2,2,2,2};
      case ValidCubeName::ThreeOutRInSJOneOpposingRSJOneOpposingSJOneLeftrightS :
         return {0,4,3,4, 4,3,2,2, 4,1,2,4};
      default:
         return {};
   }
}