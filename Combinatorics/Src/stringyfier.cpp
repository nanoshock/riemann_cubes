/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "stringyfier.h"

#include <algorithm>
#include <array>
#include <unordered_map>

#include "relation_face_map.h"
#include "face_checker.h"
#include "face_type_count_map.h"

std::string SimpleIntString( relation_t const& relation ) {
   std::string s( relation.size(), 'n' );
   std::transform( std::begin( relation ), std::end( relation ), std::begin( s ), []( auto const e ){ return e + '0'; } );
   return s;
}

namespace {
   template<typename Iter>
   Iter nth_occurrence(Iter first, Iter last, Iter first_, Iter last_, unsigned nth) {
      Iter it = std::search( first, last, first_, last_ );
      if( nth == 0 ) return it;
      if( it == last ) return it;
      return nth_occurrence( it + std::distance( first_, last_ ), last, first_, last_, nth -1 );
   }
}

namespace {
   const std::array<std::string, 5> relation_name_map = { "R_l", "R_r", "S_l", "S_r", "J__" };
}

std::string NiceString( relation_t const& relation ) {
   std::string s;
   s.reserve( 4 * relation.size() );
   std::for_each( std::begin( relation ), std::end( relation ), [&s]( auto const e ){ s.append( relation_name_map[e] + " " ); } );
   std::string white_space( " " ); //cannot be const, nth_occurrence is too dumb
   std::string const pipe( " |" );
   s.insert( nth_occurrence( std::begin( s ), std::end( s ), std::begin( white_space ), std::end( white_space ), 3 ), std::begin( pipe ), std::end( pipe ) );
   s.insert( nth_occurrence( std::begin( s ), std::end( s ), std::begin( white_space ), std::end( white_space ), 8 ), std::begin( pipe ), std::end( pipe ) );
   s.pop_back();
   return s;
}

std::string FaceString( face_t const& relation ) {
   return relation_name_map[relation[0]] + " " + relation_name_map[relation[1]] + " " + relation_name_map[relation[2]] + " " + relation_name_map[relation[3]];
}

namespace {
   std::unordered_map<ValidFaceName, std::string> const face_name_to_string_map = {
      {ValidFaceName::RrRrRrRr, "AllRightR"},   {ValidFaceName::RlRrRlRr, "AllRightR-Rot90"},   {ValidFaceName::RlRlRlRl, "AllRightR-Rot180"},   {ValidFaceName::RrRlRrRl, "AllRightR-Rot270"},
      {ValidFaceName::RrRlRlRr, "LeftRightR"},  {ValidFaceName::RlRrRrRl, "LeftRightR-Rot"},
      {ValidFaceName::RrSrRlSl, "LeftRightRS"}, {ValidFaceName::SrRrSlSl, "LeftRightRS-Rot"},   {ValidFaceName::RlSlRrSr, "LeftRightRS-Sym"},    {ValidFaceName::SlRlSrRr, "LeftRightRS-SymRot"},
      {ValidFaceName::SlSlSlSl, "AllLeftS"},    {ValidFaceName::SrSlSrSl, "AllLeftS-Rot90"},    {ValidFaceName::SrSrSrSr, "AllLeftS-Rot180"},    {ValidFaceName::SlSrSlSr, "AllLeftS-270"},
      {ValidFaceName::SlSrSrSl, "LeftRightS"},  {ValidFaceName::SrSlSlSr, "LeftRightS-Rot"},
      {ValidFaceName::J_J_J_J_, "AllJ"},
      {ValidFaceName::RrJ_J_Rr, "TwoInRTwoJ"},  {ValidFaceName::RlRrJ_J_, "TwoInRTwoJ-Rot90"},  {ValidFaceName::J_RlRlJ_, "TwoInRTwoJ-Rot180"},  {ValidFaceName::J_J_RrRl, "TwoInRTwoJ-Rot270"},
      {ValidFaceName::RlJ_J_Rl, "TwoOutRTwoJ"}, {ValidFaceName::RrRlJ_J_, "TwoOutRTwoJ-Rot90"}, {ValidFaceName::J_RrRrJ_, "TwoOutRTwoJ-Rot180"}, {ValidFaceName::J_J_RlRr, "TwoOutRTwoJ-Rot270"},
      {ValidFaceName::J_RrJ_Rr, "OppRTwoJ"},    {ValidFaceName::RlJ_RlJ_, "OppRTwoJ-Rot90"},    {ValidFaceName::J_RlJ_Rl, "OppRTwoJ-Rot180"},    {ValidFaceName::RrJ_RrJ_, "OppRTwoJ-Rot270"},
      {ValidFaceName::SlJ_J_Sl, "TwoOutSTwoJ"}, {ValidFaceName::SrSlJ_J_, "TwoOutSTwoJ-Rot90"}, {ValidFaceName::J_SrSrJ_, "TwoOutSTwoJ-Rot180"}, {ValidFaceName::J_J_SlSr, "TwoOutSTwoJ-Rot270"},
      {ValidFaceName::SrJ_J_Sr, "TwoInSTwoJ"},  {ValidFaceName::SlSrJ_J_, "TwoInSTwoJ-Rot90"},  {ValidFaceName::J_SlSlJ_, "TwoInSTwoJ-Rot180"},  {ValidFaceName::J_J_SrSl, "TwoInSTwoJ-Rot270"},
      {ValidFaceName::J_SlJ_Sl, "OppSTwoJ"},    {ValidFaceName::SrJ_SrJ_, "OppSTwoJ-Rot90"},    {ValidFaceName::J_SrJ_Sr, "OppSTwoJ-Rot180"},    {ValidFaceName::SlJ_SlJ_, "OppSTwoJ-Rot270"},
      {ValidFaceName::RrJ_J_Sl, "InROutsTwoJ"}, {ValidFaceName::SrRrJ_J_, "InROutsTwoJ-Rot90"}, {ValidFaceName::J_SrRlJ_, "InROutsTwoJ-Rot180"}, {ValidFaceName::J_J_SlRl, "InROutsTwoJ-Rot270"}, {ValidFaceName::SlJ_J_Rr, "InROutsTwoJ-Sym"}, {ValidFaceName::RlSlJ_J_, "InROutsTwoJ-Sym-Rot90"}, {ValidFaceName::J_RlSrJ_, "InROutsTwoJ-Sym-Rot180"}, {ValidFaceName::J_J_RrSr, "InROutsTwoJ-Sym-Rot270"},
      {ValidFaceName::RlJ_J_Sr, "InSOutRTwoJ"}, {ValidFaceName::SlRlJ_J_, "InSOutRTwoJ-Rot90"}, {ValidFaceName::J_SlRrJ_, "InSOutRTwoJ-Rot180"}, {ValidFaceName::J_J_SrRr, "InSOutRTwoJ-Rot270"}, {ValidFaceName::SrJ_J_Rl, "InSOutRTwoJ-Sym"}, {ValidFaceName::RrSrJ_J_, "InSOutRTwoJ-Sym-Rot90"}, {ValidFaceName::J_RrSlJ_, "InSOutRTwoJ-Sym-Rot180"}, {ValidFaceName::J_J_RlSl, "InSOutRTwoJ-Sym-Rot270"},
      {ValidFaceName::J_SlJ_Rr, "OppRSTowJ"},   {ValidFaceName::RlJ_SrJ_, "OppRSTowJ-Rot90"},   {ValidFaceName::J_RlJ_Sr, "OppRSTowJ-Rot180"},   {ValidFaceName::SlJ_RrJ_, "OppRSTowJ-Rot270"},   {ValidFaceName::J_SrJ_Rl, "OppRSTowJ-Sym"},   {ValidFaceName::RrJ_SlJ_, "OppRSTowJ-Sym-Rot90"},   {ValidFaceName::J_RrJ_Sl, "OppRSTowJ-Sym-Rot180"},   {ValidFaceName::SrJ_RlJ_, "OppRSTowJ-Sym-Rot270"},
      {ValidFaceName::Unkown,   "Unkown"}
   };
}

std::string FaceNameString( relation_t const& relation ) {
   auto const west_face   = MapRelationToFace( relation, FaceName::West );
   auto const east_face   = MapRelationToFace( relation, FaceName::East );
   auto const south_face  = MapRelationToFace( relation, FaceName::South );
   auto const north_face  = MapRelationToFace( relation, FaceName::North );
   auto const bottom_face = MapRelationToFace( relation, FaceName::Bottom );
   auto const top_face    = MapRelationToFace( relation, FaceName::Top );
   return {
      std::string( "West: " ) + face_name_to_string_map.at( std::get<1>( CheckFace( west_face   ) ) ) +  " | " +
                   "East: "   + face_name_to_string_map.at( std::get<1>( CheckFace( east_face   ) ) ) +  " | " +
                   "South: "  + face_name_to_string_map.at( std::get<1>( CheckFace( south_face  ) ) ) +  " | " +
                   "North: "  + face_name_to_string_map.at( std::get<1>( CheckFace( north_face  ) ) ) +  " | " +
                   "Bottom: " + face_name_to_string_map.at( std::get<1>( CheckFace( bottom_face ) ) ) +  " | " +
                   "Top: "    + face_name_to_string_map.at( std::get<1>( CheckFace( top_face    ) ) )
   };
}

namespace{
   std::unordered_map<FaceType, std::string> type_string = {
    {FaceType::AllRightR,   "AllRightR"},   {FaceType::LeftrightR, "LeftrightR"},
    {FaceType::LeftrightRS, "LeftrightRS"}, {FaceType::AllLeftS,   "AllLeftS"},
    {FaceType::LeftrightS,  "LeftrightS"},  {FaceType::AllJ,       "AllJ"},
    {FaceType::InflowRJ,    "InflowRJ"},    {FaceType::OutflowRJ,  "OutflowRJ"},
    {FaceType::OpposingRJ,  "OpposingRJ"},  {FaceType::OutflowSJ,  "OutflowSJ"},
    {FaceType::InflowSJ,    "InflowSJ"},    {FaceType::OpposingSJ, "OpposingSJ"},
    {FaceType::InROutSJ,    "InROutSJ"},    {FaceType::OutRInSJ,   "OutRInSJ"},
    {FaceType::OpposingRSJ, "OpposingRSJ"}
   };

   std::string TypeCountToString( std::unordered_map<FaceType, std::uint_fast8_t> const& type_count ) {
      std::string result;
      for( auto const [type, count] : type_count ) {
         if( count > 0 ) {
            if( !result.empty() ) {
               result += "\t+ ";
            }
            result += std::to_string( count );
            result += ": ";
            result += type_string.at( type );
         }
      }
      return result;
   }
}

std::string FaceCountString( std::vector<FaceType> const face_count ) {
   return TypeCountToString( CountMapOfGroup( face_count ) );
}

namespace{
   std::unordered_map<GroupName,std::string> groupname_strings = {
      { GroupName::SixAllRightR, "SixAllRightR"},
      { GroupName::TwoLeftrightRFourAllRightR, "TwoLeftrightRFourAllRightR"},
      { GroupName::ThreeLeftrightRThreeAllRightR, "ThreeLeftrightRThreeAllRightR"},
      { GroupName::ThreeInflowRJThreeAllRightR, "ThreeInflowRJThreeAllRightR"},
      { GroupName::ThreeOutflowRJThreeAllRightR, "ThreeOutflowRJThreeAllRightR"},
      { GroupName::OneOutflowRJTwoInflowRJOneLeftrightRTwoAllRightR, "OneOutflowRJTwoInflowRJOneLeftrightRTwoAllRightR"},
      { GroupName::TwoOutflowRJOneInflowRJOneLeftrightRTwoAllRightR, "TwoOutflowRJOneInflowRJOneLeftrightRTwoAllRightR"},
      { GroupName::TwoOpposingRJTwoInflowRJTwoAllRightR, "TwoOpposingRJTwoInflowRJTwoAllRightR"},
      { GroupName::TwoOpposingRSJTwoInflowRJTwoAllRightR, "TwoOpposingRSJTwoInflowRJTwoAllRightR"},
      { GroupName::TwoOpposingRJTwoOutflowRJTwoAllRightR, "TwoOpposingRJTwoOutflowRJTwoAllRightR"},
      { GroupName::TwoOpposingRSJTwoOutflowRJTwoAllRightR, "TwoOpposingRSJTwoOutflowRJTwoAllRightR"},
      { GroupName::FourOpposingRJTwoAllRightR, "FourOpposingRJTwoAllRightR"},
      { GroupName::FourOpposingRSJOneAllLeftSOneAllRightR, "FourOpposingRSJOneAllLeftSOneAllRightR"},
      { GroupName::TwoOutflowRJTwoInflowRJOneAllJOneAllRightR, "TwoOutflowRJTwoInflowRJOneAllJOneAllRightR"},
      { GroupName::TwoOutRInSJTwoInflowRJOneAllJOneAllRightR, "TwoOutRInSJTwoInflowRJOneAllJOneAllRightR"},
      { GroupName::TwoInROutSJTwoOutflowRJOneAllJOneAllRightR, "TwoInROutSJTwoOutflowRJOneAllJOneAllRightR"},
      { GroupName::TwoOutRInSJTwoInROutSJOneAllJOneAllRightR, "TwoOutRInSJTwoInROutSJOneAllJOneAllRightR"},
      { GroupName::TwoOpposingRJOneOutflowRJTwoInflowRJOneAllRightR, "TwoOpposingRJOneOutflowRJTwoInflowRJOneAllRightR"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJTwoInflowRJOneAllRightR, "OneOpposingRSJOneOutRInSJOneOpposingRJTwoInflowRJOneAllRightR"},
      { GroupName::TwoOpposingRSJOneInflowSJTwoInflowRJOneAllRightR, "TwoOpposingRSJOneInflowSJTwoInflowRJOneAllRightR"},
      { GroupName::TwoOpposingRJTwoOutflowRJOneInflowRJOneAllRightR, "TwoOpposingRJTwoOutflowRJOneInflowRJOneAllRightR"},
      { GroupName::TwoOutRInSJTwoOpposingRJOneInflowRJOneAllRightR, "TwoOutRInSJTwoOpposingRJOneInflowRJOneAllRightR"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneAllRightR, "OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneAllRightR"},
      { GroupName::TwoOpposingRSJOneOutflowSJTwoOutflowRJOneAllRightR, "TwoOpposingRSJOneOutflowSJTwoOutflowRJOneAllRightR"},
      { GroupName::TwoInROutSJTwoOpposingRJOneOutflowRJOneAllRightR, "TwoInROutSJTwoOpposingRJOneOutflowRJOneAllRightR"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllRightR, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllRightR"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllRightR, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllRightR"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllRightR, "TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllRightR"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneInflowSJOneAllRightR, "TwoOpposingRSJTwoInROutSJOneInflowSJOneAllRightR"},
      { GroupName::SixLeftrightR, "SixLeftrightR"},
      { GroupName::ThreeInflowRJThreeLeftrightR, "ThreeInflowRJThreeLeftrightR"},
      { GroupName::ThreeOutflowRJThreeLeftrightR, "ThreeOutflowRJThreeLeftrightR"},
      { GroupName::FourLeftrightRSTwoLeftrightR, "FourLeftrightRSTwoLeftrightR"},
      { GroupName::TwoOpposingRJOneOutflowRJOneInflowRJTwoLeftrightR, "TwoOpposingRJOneOutflowRJOneInflowRJTwoLeftrightR"},
      { GroupName::TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightR, "TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightR"},
      { GroupName::FourOpposingRJTwoLeftrightR, "FourOpposingRJTwoLeftrightR"},
      { GroupName::TwoInROutSJOneInflowRJTwoLeftrightRSOneLeftrightR, "TwoInROutSJOneInflowRJTwoLeftrightRSOneLeftrightR"},
      { GroupName::TwoOutRInSJOneOutflowRJTwoLeftrightRSOneLeftrightR, "TwoOutRInSJOneOutflowRJTwoLeftrightRSOneLeftrightR"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneLeftrightRSOneLeftrightR, "OneOutRInSJOneInROutSJTwoOpposingRJOneLeftrightRSOneLeftrightR"},
      { GroupName::TwoOpposingRSJTwoOpposingRJOneLeftrightRSOneLeftrightR, "TwoOpposingRSJTwoOpposingRJOneLeftrightRSOneLeftrightR"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightRSOneLeftrightR, "TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightRSOneLeftrightR"},
      { GroupName::FourOpposingRSJOneLeftrightSOneLeftrightR, "FourOpposingRSJOneLeftrightSOneLeftrightR"},
      { GroupName::FourInflowRJOneAllJOneLeftrightR, "FourInflowRJOneAllJOneLeftrightR"},
      { GroupName::TwoInROutSJTwoInflowRJOneAllJOneLeftrightR, "TwoInROutSJTwoInflowRJOneAllJOneLeftrightR"},
      { GroupName::FourOutflowRJOneAllJOneLeftrightR, "FourOutflowRJOneAllJOneLeftrightR"},
      { GroupName::TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightR, "TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightR"},
      { GroupName::FourInROutSJOneAllJOneLeftrightR, "FourInROutSJOneAllJOneLeftrightR"},
      { GroupName::FourOutRInSJOneAllJOneLeftrightR, "FourOutRInSJOneAllJOneLeftrightR"},
      { GroupName::TwoOpposingRJThreeInflowRJOneLeftrightR, "TwoOpposingRJThreeInflowRJOneLeftrightR"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightR, "OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightR"},
      { GroupName::TwoOpposingRSJOneOutflowSJTwoInflowRJOneLeftrightR, "TwoOpposingRSJOneOutflowSJTwoInflowRJOneLeftrightR"},
      { GroupName::TwoInROutSJTwoOpposingRJOneInflowRJOneLeftrightR, "TwoInROutSJTwoOpposingRJOneInflowRJOneLeftrightR"},
      { GroupName::TwoOpposingRJThreeOutflowRJOneLeftrightR, "TwoOpposingRJThreeOutflowRJOneLeftrightR"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightR, "OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightR"},
      { GroupName::TwoOpposingRSJOneInflowSJTwoOutflowRJOneLeftrightR, "TwoOpposingRSJOneInflowSJTwoOutflowRJOneLeftrightR"},
      { GroupName::TwoOutRInSJTwoOpposingRJOneOutflowRJOneLeftrightR, "TwoOutRInSJTwoOpposingRJOneOutflowRJOneLeftrightR"},
      { GroupName::OneOpposingRSJThreeInROutSJOneOpposingRJOneLeftrightR, "OneOpposingRSJThreeInROutSJOneOpposingRJOneLeftrightR"},
      { GroupName::OneOpposingRSJThreeOutRInSJOneOpposingRJOneLeftrightR, "OneOpposingRSJThreeOutRInSJOneOpposingRJOneLeftrightR"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightR, "TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightR"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightR, "TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightR"},
      { GroupName::TwoLeftrightSFourLeftrightRS, "TwoLeftrightSFourLeftrightRS"},
      { GroupName::TwoInROutSJOneOutflowSJOneLeftrightSTwoLeftrightRS, "TwoInROutSJOneOutflowSJOneLeftrightSTwoLeftrightRS"},
      { GroupName::TwoOutRInSJOneInflowSJOneLeftrightSTwoLeftrightRS, "TwoOutRInSJOneInflowSJOneLeftrightSTwoLeftrightRS"},
      { GroupName::TwoOpposingSJOneOutflowRJOneInflowRJTwoLeftrightRS, "TwoOpposingSJOneOutflowRJOneInflowRJTwoLeftrightRS"},
      { GroupName::TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightRS, "TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightRS"},
      { GroupName::OneInflowSJOneOutflowSJTwoOpposingRJTwoLeftrightRS, "OneInflowSJOneOutflowSJTwoOpposingRJTwoLeftrightRS"},
      { GroupName::TwoOpposingSJTwoOpposingRJTwoLeftrightRS, "TwoOpposingSJTwoOpposingRJTwoLeftrightRS"},
      { GroupName::TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightRS, "TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightRS"},
      { GroupName::FourOpposingRSJTwoLeftrightRS, "FourOpposingRSJTwoLeftrightRS"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneLeftrightSOneLeftrightRS, "OneOutRInSJOneInROutSJTwoOpposingSJOneLeftrightSOneLeftrightRS"},
      { GroupName::TwoOpposingRSJTwoOpposingSJOneLeftrightSOneLeftrightRS, "TwoOpposingRSJTwoOpposingSJOneLeftrightSOneLeftrightRS"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightSOneLeftrightRS, "TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightSOneLeftrightRS"},
      { GroupName::TwoInROutSJTwoInflowRJOneAllJOneLeftrightRS, "TwoInROutSJTwoInflowRJOneAllJOneLeftrightRS"},
      { GroupName::TwoInROutSJOneOutflowSJOneInflowRJOneAllJOneLeftrightRS, "TwoInROutSJOneOutflowSJOneInflowRJOneAllJOneLeftrightRS"},
      { GroupName::TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightRS, "TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightRS"},
      { GroupName::TwoOutRInSJOneInflowSJOneOutflowRJOneAllJOneLeftrightRS, "TwoOutRInSJOneInflowSJOneOutflowRJOneAllJOneLeftrightRS"},
      { GroupName::TwoInROutSJTwoOutflowSJOneAllJOneLeftrightRS, "TwoInROutSJTwoOutflowSJOneAllJOneLeftrightRS"},
      { GroupName::TwoOutRInSJTwoInflowSJOneAllJOneLeftrightRS, "TwoOutRInSJTwoInflowSJOneAllJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightRS, "OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneInROutSJOneOutflowSJOneOpposingRJOneInflowRJOneLeftrightRS, "OneOpposingRSJOneInROutSJOneOutflowSJOneOpposingRJOneInflowRJOneLeftrightRS"},
      { GroupName::TwoInROutSJOneOpposingSJOneOpposingRJOneInflowRJOneLeftrightRS, "TwoInROutSJOneOpposingSJOneOpposingRJOneInflowRJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJOneInflowRJOneLeftrightRS, "OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJOneInflowRJOneLeftrightRS"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightRS, "TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightRS, "OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneOutRInSJOneInflowSJOneOpposingRJOneOutflowRJOneLeftrightRS, "OneOpposingRSJOneOutRInSJOneInflowSJOneOpposingRJOneOutflowRJOneLeftrightRS"},
      { GroupName::TwoOutRInSJOneOpposingSJOneOpposingRJOneOutflowRJOneLeftrightRS, "TwoOutRInSJOneOpposingSJOneOpposingRJOneOutflowRJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowRJOneLeftrightRS, "OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowRJOneLeftrightRS"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightRS, "TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightRS"},
      { GroupName::TwoInROutSJOneOpposingSJOneOutflowSJOneOpposingRJOneLeftrightRS, "TwoInROutSJOneOpposingSJOneOutflowSJOneOpposingRJOneLeftrightRS"},
      { GroupName::TwoOutRInSJOneOpposingSJOneInflowSJOneOpposingRJOneLeftrightRS, "TwoOutRInSJOneOpposingSJOneInflowSJOneOpposingRJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightRS, "OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightRS"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightRS, "TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightRS"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightRS, "OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightRS"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightRS, "TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightRS"},
      { GroupName::SixAllLeftS, "SixAllLeftS"},
      { GroupName::TwoLeftrightSFourAllLeftS, "TwoLeftrightSFourAllLeftS"},
      { GroupName::ThreeLeftrightSThreeAllLeftS, "ThreeLeftrightSThreeAllLeftS"},
      { GroupName::ThreeOutflowSJThreeAllLeftS, "ThreeOutflowSJThreeAllLeftS"},
      { GroupName::ThreeInflowSJThreeAllLeftS, "ThreeInflowSJThreeAllLeftS"},
      { GroupName::OneInflowSJTwoOutflowSJOneLeftrightSTwoAllLeftS, "OneInflowSJTwoOutflowSJOneLeftrightSTwoAllLeftS"},
      { GroupName::TwoInflowSJOneOutflowSJOneLeftrightSTwoAllLeftS, "TwoInflowSJOneOutflowSJOneLeftrightSTwoAllLeftS"},
      { GroupName::TwoOpposingSJTwoOutflowSJTwoAllLeftS, "TwoOpposingSJTwoOutflowSJTwoAllLeftS"},
      { GroupName::TwoOpposingRSJTwoOutflowSJTwoAllLeftS, "TwoOpposingRSJTwoOutflowSJTwoAllLeftS"},
      { GroupName::TwoOpposingSJTwoInflowSJTwoAllLeftS, "TwoOpposingSJTwoInflowSJTwoAllLeftS"},
      { GroupName::TwoOpposingRSJTwoInflowSJTwoAllLeftS, "TwoOpposingRSJTwoInflowSJTwoAllLeftS"},
      { GroupName::FourOpposingSJTwoAllLeftS, "FourOpposingSJTwoAllLeftS"},
      { GroupName::TwoInflowSJTwoOutflowSJOneAllJOneAllLeftS, "TwoInflowSJTwoOutflowSJOneAllJOneAllLeftS"},
      { GroupName::TwoOutRInSJTwoOutflowSJOneAllJOneAllLeftS, "TwoOutRInSJTwoOutflowSJOneAllJOneAllLeftS"},
      { GroupName::TwoInROutSJTwoInflowSJOneAllJOneAllLeftS, "TwoInROutSJTwoInflowSJOneAllJOneAllLeftS"},
      { GroupName::TwoOutRInSJTwoInROutSJOneAllJOneAllLeftS, "TwoOutRInSJTwoInROutSJOneAllJOneAllLeftS"},
      { GroupName::TwoOpposingRSJTwoInflowSJOneInflowRJOneAllLeftS, "TwoOpposingRSJTwoInflowSJOneInflowRJOneAllLeftS"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllLeftS, "TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllLeftS"},
      { GroupName::TwoOpposingRSJTwoOutflowSJOneOutflowRJOneAllLeftS, "TwoOpposingRSJTwoOutflowSJOneOutflowRJOneAllLeftS"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllLeftS, "TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllLeftS"},
      { GroupName::TwoOpposingSJOneInflowSJTwoOutflowSJOneAllLeftS, "TwoOpposingSJOneInflowSJTwoOutflowSJOneAllLeftS"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJTwoOutflowSJOneAllLeftS, "OneOpposingRSJOneOutRInSJOneOpposingSJTwoOutflowSJOneAllLeftS"},
      { GroupName::TwoOpposingSJTwoInflowSJOneOutflowSJOneAllLeftS, "TwoOpposingSJTwoInflowSJOneOutflowSJOneAllLeftS"},
      { GroupName::TwoOutRInSJTwoOpposingSJOneOutflowSJOneAllLeftS, "TwoOutRInSJTwoOpposingSJOneOutflowSJOneAllLeftS"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneAllLeftS, "OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneAllLeftS"},
      { GroupName::TwoInROutSJTwoOpposingSJOneInflowSJOneAllLeftS, "TwoInROutSJTwoOpposingSJOneInflowSJOneAllLeftS"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllLeftS, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllLeftS"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllLeftS, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllLeftS"},
      { GroupName::SixLeftrightS, "SixLeftrightS"},
      { GroupName::ThreeOutflowSJThreeLeftrightS, "ThreeOutflowSJThreeLeftrightS"},
      { GroupName::ThreeInflowSJThreeLeftrightS, "ThreeInflowSJThreeLeftrightS"},
      { GroupName::TwoOpposingSJOneInflowSJOneOutflowSJTwoLeftrightS, "TwoOpposingSJOneInflowSJOneOutflowSJTwoLeftrightS"},
      { GroupName::TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightS, "TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightS"},
      { GroupName::FourOpposingSJTwoLeftrightS, "FourOpposingSJTwoLeftrightS"},
      { GroupName::FourOutflowSJOneAllJOneLeftrightS, "FourOutflowSJOneAllJOneLeftrightS"},
      { GroupName::TwoInROutSJTwoOutflowSJOneAllJOneLeftrightS, "TwoInROutSJTwoOutflowSJOneAllJOneLeftrightS"},
      { GroupName::FourInflowSJOneAllJOneLeftrightS, "FourInflowSJOneAllJOneLeftrightS"},
      { GroupName::TwoOutRInSJTwoInflowSJOneAllJOneLeftrightS, "TwoOutRInSJTwoInflowSJOneAllJOneLeftrightS"},
      { GroupName::FourInROutSJOneAllJOneLeftrightS, "FourInROutSJOneAllJOneLeftrightS"},
      { GroupName::FourOutRInSJOneAllJOneLeftrightS, "FourOutRInSJOneAllJOneLeftrightS"},
      { GroupName::TwoOpposingRSJTwoOutflowSJOneInflowRJOneLeftrightS, "TwoOpposingRSJTwoOutflowSJOneInflowRJOneLeftrightS"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightS, "TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightS"},
      { GroupName::TwoOpposingRSJTwoInflowSJOneOutflowRJOneLeftrightS, "TwoOpposingRSJTwoInflowSJOneOutflowRJOneLeftrightS"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightS, "TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightS"},
      { GroupName::TwoOpposingSJThreeOutflowSJOneLeftrightS, "TwoOpposingSJThreeOutflowSJOneLeftrightS"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightS, "OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightS"},
      { GroupName::TwoInROutSJTwoOpposingSJOneOutflowSJOneLeftrightS, "TwoInROutSJTwoOpposingSJOneOutflowSJOneLeftrightS"},
      { GroupName::TwoOpposingSJThreeInflowSJOneLeftrightS, "TwoOpposingSJThreeInflowSJOneLeftrightS"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightS, "OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightS"},
      { GroupName::TwoOutRInSJTwoOpposingSJOneInflowSJOneLeftrightS, "TwoOutRInSJTwoOpposingSJOneInflowSJOneLeftrightS"},
      { GroupName::OneOpposingRSJThreeInROutSJOneOpposingSJOneLeftrightS, "OneOpposingRSJThreeInROutSJOneOpposingSJOneLeftrightS"},
      { GroupName::OneOpposingRSJThreeOutRInSJOneOpposingSJOneLeftrightS, "OneOpposingRSJThreeOutRInSJOneOpposingSJOneLeftrightS"},
      { GroupName::SixAllJ, "SixAllJ"},
      { GroupName::ThreeInflowRJThreeAllJ, "ThreeInflowRJThreeAllJ"},
      { GroupName::TwoInROutSJOneInflowRJThreeAllJ, "TwoInROutSJOneInflowRJThreeAllJ"},
      { GroupName::ThreeOutflowRJThreeAllJ, "ThreeOutflowRJThreeAllJ"},
      { GroupName::TwoOutRInSJOneOutflowRJThreeAllJ, "TwoOutRInSJOneOutflowRJThreeAllJ"},
      { GroupName::ThreeOutflowSJThreeAllJ, "ThreeOutflowSJThreeAllJ"},
      { GroupName::TwoInROutSJOneOutflowSJThreeAllJ, "TwoInROutSJOneOutflowSJThreeAllJ"},
      { GroupName::ThreeInflowSJThreeAllJ, "ThreeInflowSJThreeAllJ"},
      { GroupName::TwoOutRInSJOneInflowSJThreeAllJ, "TwoOutRInSJOneInflowSJThreeAllJ"},
      { GroupName::TwoOpposingRJTwoInflowRJTwoAllJ, "TwoOpposingRJTwoInflowRJTwoAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingRJOneInflowRJTwoAllJ, "OneOpposingRSJOneInROutSJOneOpposingRJOneInflowRJTwoAllJ"},
      { GroupName::TwoOpposingRSJOneOutflowSJOneInflowRJTwoAllJ, "TwoOpposingRSJOneOutflowSJOneInflowRJTwoAllJ"},
      { GroupName::TwoOpposingRJTwoOutflowRJTwoAllJ, "TwoOpposingRJTwoOutflowRJTwoAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoAllJ, "OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoAllJ"},
      { GroupName::TwoOpposingRSJOneInflowSJOneOutflowRJTwoAllJ, "TwoOpposingRSJOneInflowSJOneOutflowRJTwoAllJ"},
      { GroupName::FourOpposingRJTwoAllJ, "FourOpposingRJTwoAllJ"},
      { GroupName::TwoOpposingRSJTwoOpposingRJTwoAllJ, "TwoOpposingRSJTwoOpposingRJTwoAllJ"},
      { GroupName::TwoInROutSJOneOpposingSJOneOpposingRJTwoAllJ, "TwoInROutSJOneOpposingSJOneOpposingRJTwoAllJ"},
      { GroupName::TwoOutRInSJOneOpposingSJOneOpposingRJTwoAllJ, "TwoOutRInSJOneOpposingSJOneOpposingRJTwoAllJ"},
      { GroupName::TwoOpposingRSJOneOpposingSJOneOpposingRJTwoAllJ, "TwoOpposingRSJOneOpposingSJOneOpposingRJTwoAllJ"},
      { GroupName::TwoOpposingSJTwoOutflowSJTwoAllJ, "TwoOpposingSJTwoOutflowSJTwoAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJTwoAllJ, "OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJTwoAllJ"},
      { GroupName::TwoOpposingSJTwoInflowSJTwoAllJ, "TwoOpposingSJTwoInflowSJTwoAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoAllJ, "OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoAllJ"},
      { GroupName::FourOpposingSJTwoAllJ, "FourOpposingSJTwoAllJ"},
      { GroupName::TwoOpposingRSJTwoOpposingSJTwoAllJ, "TwoOpposingRSJTwoOpposingSJTwoAllJ"},
      { GroupName::TwoOpposingRSJTwoInROutSJTwoAllJ, "TwoOpposingRSJTwoInROutSJTwoAllJ"},
      { GroupName::TwoOpposingRSJTwoOutRInSJTwoAllJ, "TwoOpposingRSJTwoOutRInSJTwoAllJ"},
      { GroupName::FourOpposingRSJTwoAllJ, "FourOpposingRSJTwoAllJ"},
      { GroupName::TwoOpposingRJOneOutflowRJTwoInflowRJOneAllJ, "TwoOpposingRJOneOutflowRJTwoInflowRJOneAllJ"},
      { GroupName::TwoOpposingRSJOneOutflowRJTwoInflowRJOneAllJ, "TwoOpposingRSJOneOutflowRJTwoInflowRJOneAllJ"},
      { GroupName::TwoOpposingRJTwoOutflowRJOneInflowRJOneAllJ, "TwoOpposingRJTwoOutflowRJOneInflowRJOneAllJ"},
      { GroupName::TwoOpposingRSJTwoOutflowRJOneInflowRJOneAllJ, "TwoOpposingRSJTwoOutflowRJOneInflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ, "OneOpposingRSJOneInROutSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ, "OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ, "OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ, "OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneInflowRJOneAllJ, "OneOutRInSJOneInROutSJTwoOpposingRJOneInflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneInflowRJOneAllJ, "OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneInflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowSJOneInflowRJOneAllJ, "OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowSJOneInflowRJOneAllJ"},
      { GroupName::TwoOutRInSJTwoOpposingSJOneInflowRJOneAllJ, "TwoOutRInSJTwoOpposingSJOneInflowRJOneAllJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowRJOneAllJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowRJOneAllJ"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllJ, "TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneAllJ, "OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneAllJ, "OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowRJOneAllJ, "OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowRJOneAllJ"},
      { GroupName::TwoInROutSJTwoOpposingSJOneOutflowRJOneAllJ, "TwoInROutSJTwoOpposingSJOneOutflowRJOneAllJ"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllJ, "TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneAllJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneAllJ"},
      { GroupName::TwoOutRInSJOneOutflowSJTwoOpposingRJOneAllJ, "TwoOutRInSJOneOutflowSJTwoOpposingRJOneAllJ"},
      { GroupName::TwoInROutSJOneInflowSJTwoOpposingRJOneAllJ, "TwoInROutSJOneInflowSJTwoOpposingRJOneAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ, "OneOpposingRSJOneInROutSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ, "OneOpposingRSJOneOutRInSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllJ, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllJ"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllJ, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllJ"},
      { GroupName::TwoOpposingSJOneInflowSJTwoOutflowSJOneAllJ, "TwoOpposingSJOneInflowSJTwoOutflowSJOneAllJ"},
      { GroupName::TwoOpposingRSJOneInflowSJTwoOutflowSJOneAllJ, "TwoOpposingRSJOneInflowSJTwoOutflowSJOneAllJ"},
      { GroupName::TwoOpposingSJTwoInflowSJOneOutflowSJOneAllJ, "TwoOpposingSJTwoInflowSJOneOutflowSJOneAllJ"},
      { GroupName::TwoOpposingRSJTwoInflowSJOneOutflowSJOneAllJ, "TwoOpposingRSJTwoInflowSJOneOutflowSJOneAllJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ, "OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ, "OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneOutflowSJOneAllJ, "OneOutRInSJOneInROutSJTwoOpposingSJOneOutflowSJOneAllJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneAllJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneAllJ"},
      { GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllJ, "TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneAllJ, "OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneAllJ"},
      { GroupName::TwoOpposingRSJTwoInROutSJOneInflowSJOneAllJ, "TwoOpposingRSJTwoInROutSJOneInflowSJOneAllJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneAllJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneAllJ"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllJ, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllJ"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllJ, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllJ"},
      { GroupName::SixInflowRJ, "SixInflowRJ"},
      { GroupName::TwoInROutSJFourInflowRJ, "TwoInROutSJFourInflowRJ"},
      { GroupName::ThreeOutflowRJThreeInflowRJ, "ThreeOutflowRJThreeInflowRJ"},
      { GroupName::TwoOutRInSJOneOutflowRJThreeInflowRJ, "TwoOutRInSJOneOutflowRJThreeInflowRJ"},
      { GroupName::ThreeOutflowSJThreeInflowRJ, "ThreeOutflowSJThreeInflowRJ"},
      { GroupName::TwoInROutSJOneOutflowSJThreeInflowRJ, "TwoInROutSJOneOutflowSJThreeInflowRJ"},
      { GroupName::ThreeInflowSJThreeInflowRJ, "ThreeInflowSJThreeInflowRJ"},
      { GroupName::TwoOutRInSJOneInflowSJThreeInflowRJ, "TwoOutRInSJOneInflowSJThreeInflowRJ"},
      { GroupName::TwoOpposingRJTwoOutflowRJTwoInflowRJ, "TwoOpposingRJTwoOutflowRJTwoInflowRJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOutflowRJTwoInflowRJ, "OneOutRInSJOneInROutSJTwoOutflowRJTwoInflowRJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoInflowRJ, "OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoInflowRJ"},
      { GroupName::TwoOutRInSJOneOutflowSJOneOutflowRJTwoInflowRJ, "TwoOutRInSJOneOutflowSJOneOutflowRJTwoInflowRJ"},
      { GroupName::FourInROutSJTwoInflowRJ, "FourInROutSJTwoInflowRJ"},
      { GroupName::TwoOpposingRSJTwoOutRInSJTwoInflowRJ, "TwoOpposingRSJTwoOutRInSJTwoInflowRJ"},
      { GroupName::TwoInROutSJThreeOutflowRJOneInflowRJ, "TwoInROutSJThreeOutflowRJOneInflowRJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneInflowRJ, "OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneInflowRJ"},
      { GroupName::TwoInROutSJOneInflowSJTwoOutflowRJOneInflowRJ, "TwoInROutSJOneInflowSJTwoOutflowRJOneInflowRJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneInflowRJ, "OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneInflowRJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneOutflowRJOneInflowRJ, "OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneOutflowRJOneInflowRJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneInflowRJ, "OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneInflowRJ"},
      { GroupName::OneOutRInSJOneInROutSJOneOpposingSJOneOpposingRJOneOutflowRJOneInflowRJ, "OneOutRInSJOneInROutSJOneOpposingSJOneOpposingRJOneOutflowRJOneInflowRJ"},
      { GroupName::OneOutRInSJOneInROutSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ, "OneOutRInSJOneInROutSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ"},
      { GroupName::TwoOpposingRSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ, "TwoOpposingRSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ"},
      { GroupName::TwoOutRInSJTwoInROutSJOneOutflowRJOneInflowRJ, "TwoOutRInSJTwoInROutSJOneOutflowRJOneInflowRJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneInflowRJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneInflowRJ"},
      { GroupName::TwoOutRInSJOneOpposingSJOneOutflowSJOneOpposingRJOneInflowRJ, "TwoOutRInSJOneOpposingSJOneOutflowSJOneOpposingRJOneInflowRJ"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneInflowRJ, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneInflowRJ"},
      { GroupName::TwoInROutSJThreeOutflowSJOneInflowRJ, "TwoInROutSJThreeOutflowSJOneInflowRJ"},
      { GroupName::TwoOutRInSJOneInflowSJTwoOutflowSJOneInflowRJ, "TwoOutRInSJOneInflowSJTwoOutflowSJOneInflowRJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneInflowRJ, "OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneInflowRJ"},
      { GroupName::FourInROutSJOneOutflowSJOneInflowRJ, "FourInROutSJOneOutflowSJOneInflowRJ"},
      { GroupName::ThreeOutRInSJOneInROutSJOneOutflowSJOneInflowRJ, "ThreeOutRInSJOneInROutSJOneOutflowSJOneInflowRJ"},
      { GroupName::TwoInROutSJThreeInflowSJOneInflowRJ, "TwoInROutSJThreeInflowSJOneInflowRJ"},
      { GroupName::TwoOutRInSJTwoInROutSJOneInflowSJOneInflowRJ, "TwoOutRInSJTwoInROutSJOneInflowSJOneInflowRJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneInflowRJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneInflowRJ"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneInflowRJ, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneInflowRJ"},
      { GroupName::SixOutflowRJ, "SixOutflowRJ"},
      { GroupName::TwoOutRInSJFourOutflowRJ, "TwoOutRInSJFourOutflowRJ"},
      { GroupName::ThreeOutflowSJThreeOutflowRJ, "ThreeOutflowSJThreeOutflowRJ"},
      { GroupName::TwoInROutSJOneOutflowSJThreeOutflowRJ, "TwoInROutSJOneOutflowSJThreeOutflowRJ"},
      { GroupName::ThreeInflowSJThreeOutflowRJ, "ThreeInflowSJThreeOutflowRJ"},
      { GroupName::TwoOutRInSJOneInflowSJThreeOutflowRJ, "TwoOutRInSJOneInflowSJThreeOutflowRJ"},
      { GroupName::TwoOpposingRSJTwoInROutSJTwoOutflowRJ, "TwoOpposingRSJTwoInROutSJTwoOutflowRJ"},
      { GroupName::FourOutRInSJTwoOutflowRJ, "FourOutRInSJTwoOutflowRJ"},
      { GroupName::TwoInROutSJOneOpposingSJOneInflowSJOneOpposingRJOneOutflowRJ, "TwoInROutSJOneOpposingSJOneInflowSJOneOpposingRJOneOutflowRJ"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneOutflowRJ, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneOutflowRJ"},
      { GroupName::TwoOutRInSJThreeOutflowSJOneOutflowRJ, "TwoOutRInSJThreeOutflowSJOneOutflowRJ"},
      { GroupName::TwoInROutSJTwoInflowSJOneOutflowSJOneOutflowRJ, "TwoInROutSJTwoInflowSJOneOutflowSJOneOutflowRJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOutflowRJ, "OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOutflowRJ"},
      { GroupName::TwoOutRInSJTwoInROutSJOneOutflowSJOneOutflowRJ, "TwoOutRInSJTwoInROutSJOneOutflowSJOneOutflowRJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneOutflowRJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneOutflowRJ"},
      { GroupName::TwoOutRInSJThreeInflowSJOneOutflowRJ, "TwoOutRInSJThreeInflowSJOneOutflowRJ"},
      { GroupName::OneOutRInSJThreeInROutSJOneInflowSJOneOutflowRJ, "OneOutRInSJThreeInROutSJOneInflowSJOneOutflowRJ"},
      { GroupName::FourOutRInSJOneInflowSJOneOutflowRJ, "FourOutRInSJOneInflowSJOneOutflowRJ"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneOutflowRJ, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneOutflowRJ"},
      { GroupName::TwoOutRInSJTwoInROutSJTwoOpposingRJ, "TwoOutRInSJTwoInROutSJTwoOpposingRJ"},
      { GroupName::OneOutRInSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOpposingRJ, "OneOutRInSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOpposingRJ"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOutflowSJOneOpposingRJ, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOutflowSJOneOpposingRJ"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneInflowSJOneOpposingRJ, "OneOpposingRSJOneOutRInSJTwoInROutSJOneInflowSJOneOpposingRJ"},
      { GroupName::SixOutflowSJ, "SixOutflowSJ"},
      { GroupName::TwoInROutSJFourOutflowSJ, "TwoInROutSJFourOutflowSJ"},
      { GroupName::ThreeInflowSJThreeOutflowSJ, "ThreeInflowSJThreeOutflowSJ"},
      { GroupName::TwoOutRInSJOneInflowSJThreeOutflowSJ, "TwoOutRInSJOneInflowSJThreeOutflowSJ"},
      { GroupName::TwoOpposingSJTwoInflowSJTwoOutflowSJ, "TwoOpposingSJTwoInflowSJTwoOutflowSJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoInflowSJTwoOutflowSJ, "OneOutRInSJOneInROutSJTwoInflowSJTwoOutflowSJ"},
      { GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoOutflowSJ, "OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoOutflowSJ"},
      { GroupName::FourInROutSJTwoOutflowSJ, "FourInROutSJTwoOutflowSJ"},
      { GroupName::TwoOpposingRSJTwoOutRInSJTwoOutflowSJ, "TwoOpposingRSJTwoOutRInSJTwoOutflowSJ"},
      { GroupName::TwoInROutSJThreeInflowSJOneOutflowSJ, "TwoInROutSJThreeInflowSJOneOutflowSJ"},
      { GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneOutflowSJ, "OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneOutflowSJ"},
      { GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneOutflowSJ, "OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneOutflowSJ"},
      { GroupName::TwoOutRInSJTwoInROutSJOneInflowSJOneOutflowSJ, "TwoOutRInSJTwoInROutSJOneInflowSJOneOutflowSJ"},
      { GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneOutflowSJ, "TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneOutflowSJ"},
      { GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneOutflowSJ, "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneOutflowSJ"},
      { GroupName::SixInflowSJ, "SixInflowSJ"},
      { GroupName::TwoOutRInSJFourInflowSJ, "TwoOutRInSJFourInflowSJ"},
      { GroupName::TwoOpposingRSJTwoInROutSJTwoInflowSJ, "TwoOpposingRSJTwoInROutSJTwoInflowSJ"},
      { GroupName::FourOutRInSJTwoInflowSJ, "FourOutRInSJTwoInflowSJ"},
      { GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneInflowSJ, "OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneInflowSJ"},
      { GroupName::TwoOutRInSJTwoInROutSJTwoOpposingSJ, "TwoOutRInSJTwoInROutSJTwoOpposingSJ"},
      { GroupName::ThreeOutRInSJThreeInROutSJ, "ThreeOutRInSJThreeInROutSJ"},
      { GroupName::TwoOpposingRSJTwoOutRInSJTwoInROutSJ, "TwoOpposingRSJTwoOutRInSJTwoInROutSJ"}
   };
}

std::string GroupNameString( GroupName const gn ) {
   return groupname_strings.at( gn );
}

namespace{
   std::array<std::string, 5> wave_latex_string_map = { "\\NamedWaveLeft{R}", "\\NamedWaveRight{R}",
                                                        "\\NamedWaveLeft{S}", "\\NamedWaveRight{S}",
                                                        "\\NamedWave{J}" };
   std::array<std::string, 12> position_string_map = { "{2}{1}", "{3}{2}", "{3}{4}", "{4}{1}",
                                                       "{2}{6}", "{3}{7}", "{1}{5}", "{4}{8}",
                                                       "{6}{5}", "{7}{6}", "{7}{8}", "{8}{5}" };
}

std::string LatexString( relation_t const& relation ) {
   std::string result;
   for( std::size_t i = 0; i < relation.size(); ++i ) {
      result += wave_latex_string_map[static_cast<std::size_t>( relation[i] )];
      result += position_string_map[i];
   }
   return result;
}

std::string FaceNameEnumString( FaceName const fn, bool const padding ) {
   switch( fn ) {
      case FaceName::West:
         return padding ? "West  " : "West";
      case FaceName::East:
         return padding ? "East  " : "East";
      case FaceName::South:
         return padding ? "South " : "South";
      case FaceName::North:
         return padding ? "North " : "North";
      case FaceName::Bottom:
         return padding ? "Bottom" : "Bottom";
      default:
         return padding ? "Top   " : "Top";
   }
}
