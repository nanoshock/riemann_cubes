/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#ifndef VALID_FACE_NAMES_H
#define VALID_FACE_NAMES_H

enum class ValidFaceName {
   RrRrRrRr, RlRrRlRr, RlRlRlRl, RrRlRrRl,
   RrRlRlRr, RlRrRrRl,
   RrSrRlSl, SrRrSlSl, RlSlRrSr, SlRlSrRr,
   SlSlSlSl, SrSlSrSl, SrSrSrSr, SlSrSlSr,
   SlSrSrSl, SrSlSlSr,
   J_J_J_J_,
   RrJ_J_Rr, RlRrJ_J_, J_RlRlJ_, J_J_RrRl,
   RlJ_J_Rl, RrRlJ_J_, J_RrRrJ_, J_J_RlRr,
   J_RrJ_Rr, RlJ_RlJ_, J_RlJ_Rl, RrJ_RrJ_,
   SlJ_J_Sl, SrSlJ_J_, J_SrSrJ_, J_J_SlSr,
   SrJ_J_Sr, SlSrJ_J_, J_SlSlJ_, J_J_SrSl,
   J_SlJ_Sl, SrJ_SrJ_, J_SrJ_Sr, SlJ_SlJ_,
   RrJ_J_Sl, SrRrJ_J_, J_SrRlJ_, J_J_SlRl, SlJ_J_Rr, RlSlJ_J_, J_RlSrJ_, J_J_RrSr,
   RlJ_J_Sr, SlRlJ_J_, J_SlRrJ_, J_J_SrRr, SrJ_J_Rl, RrSrJ_J_, J_RrSlJ_, J_J_RlSl,
   J_SlJ_Rr, RlJ_SrJ_, J_RlJ_Sr, SlJ_RrJ_, J_SrJ_Rl, RrJ_SlJ_, J_RrJ_Sl, SrJ_RlJ_,
   Unkown
};

#endif // VALID_FACE_NAMES_H