/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "face_generator.h"

face_t GenerateValidFace( ValidFaceName const vf ) {
   switch( vf ) {
      case ValidFaceName::RrRrRrRr :
         return {1, 1, 1, 1};
      case ValidFaceName::RlRrRlRr :
         return {0, 1, 0, 1};
      case ValidFaceName::RlRlRlRl :
         return {0, 0, 0, 0};
      case ValidFaceName::RrRlRrRl :
         return {1, 0, 1, 0};
      case ValidFaceName::RrRlRlRr :
         return {1, 0, 0, 1};
      case ValidFaceName::RlRrRrRl :
         return {0, 1, 1, 0};
      case ValidFaceName::RrSrRlSl :
         return {1, 3, 0, 2};
      case ValidFaceName::SrRrSlSl :
         return {3, 1, 2, 0};
      case ValidFaceName::RlSlRrSr :
         return {0, 2, 1, 3};
      case ValidFaceName::SlRlSrRr :
         return {2, 0, 3, 1};
      case ValidFaceName::SlSlSlSl :
         return {2, 2, 2, 2};
      case ValidFaceName::SrSlSrSl :
         return {3, 2, 3, 2};
      case ValidFaceName::SrSrSrSr :
         return {3, 3, 3, 3};
      case ValidFaceName::SlSrSlSr :
         return {2, 3, 2, 3};
      case ValidFaceName::SlSrSrSl :
         return {2, 3, 3, 2};
      case ValidFaceName::SrSlSlSr :
         return {3, 2, 2, 3};
      case ValidFaceName::J_J_J_J_ :
         return {4, 4, 4, 4};
      case ValidFaceName::RrJ_J_Rr :
         return {1, 4, 4, 1};
      case ValidFaceName::RlRrJ_J_ :
         return {0, 1, 4, 4};
      case ValidFaceName::J_RlRlJ_ :
         return {4, 0, 0, 4};
      case ValidFaceName::J_J_RrRl :
         return {4, 4, 1, 0};
      case ValidFaceName::RlJ_J_Rl :
         return {0, 4, 4, 0};
      case ValidFaceName::RrRlJ_J_ :
         return {1, 0, 4, 4};
      case ValidFaceName::J_RrRrJ_ :
         return {4, 1, 1, 4};
      case ValidFaceName::J_J_RlRr :
         return {4, 4, 0, 1};
      case ValidFaceName::J_RrJ_Rr :
         return {4, 1, 4, 1};
      case ValidFaceName::RlJ_RlJ_ :
         return {0, 4, 0, 4};
      case ValidFaceName::J_RlJ_Rl :
         return {4, 0, 4, 0};
      case ValidFaceName::RrJ_RrJ_ :
         return {1, 4, 1, 4};
      case ValidFaceName::SlJ_J_Sl :
         return {2, 4, 4, 2};
      case ValidFaceName::SrSlJ_J_ :
         return {3, 2, 4, 4};
      case ValidFaceName::J_SrSrJ_ :
         return {4, 3, 3, 4};
      case ValidFaceName::J_J_SlSr :
         return {4, 4, 2, 3};
      case ValidFaceName::SrJ_J_Sr :
         return {3, 4, 4, 3};
      case ValidFaceName::SlSrJ_J_ :
         return {2, 3, 4, 4};
      case ValidFaceName::J_SlSlJ_ :
         return {4, 2, 2, 4};
      case ValidFaceName::J_J_SrSl :
         return {4, 4, 3, 2};
      case ValidFaceName::J_SlJ_Sl :
         return {4, 2, 4, 2};
      case ValidFaceName::SrJ_SrJ_ :
         return {3, 4, 3, 4};
      case ValidFaceName::J_SrJ_Sr :
         return {4, 3, 4, 3};
      case ValidFaceName::SlJ_SlJ_ :
         return {2, 4, 2, 4};
      case ValidFaceName::RrJ_J_Sl :
         return {1, 4, 4, 2};
      case ValidFaceName::SrRrJ_J_ :
         return {3, 1, 4, 4};
      case ValidFaceName::J_SrRlJ_ :
         return {4, 3, 0, 4};
      case ValidFaceName::J_J_SlRl :
         return {4, 4, 2, 0};
      case ValidFaceName::SlJ_J_Rr :
         return {2, 4, 4, 1};
      case ValidFaceName::RlSlJ_J_ :
         return {0, 2, 4, 4};
      case ValidFaceName::J_RlSrJ_ :
         return {4, 0, 3, 4};
      case ValidFaceName::J_J_RrSr :
         return {4, 4, 1, 3};
      case ValidFaceName::RlJ_J_Sr :
         return {0, 4, 4, 3};
      case ValidFaceName::SlRlJ_J_ :
         return {2, 0, 4, 4};
      case ValidFaceName::J_SlRrJ_ :
         return {4, 2, 1, 4};
      case ValidFaceName::J_J_SrRr :
         return {4, 4, 3, 1};
      case ValidFaceName::SrJ_J_Rl :
         return {3, 4, 4, 0};
      case ValidFaceName::RrSrJ_J_ :
         return {1, 3, 4, 4};
      case ValidFaceName::J_RrSlJ_ :
         return {4, 1, 2, 4};
      case ValidFaceName::J_J_RlSl :
         return {4, 4, 0, 2};
      case ValidFaceName::J_SlJ_Rr :
         return {4, 2, 4, 1};
      case ValidFaceName::RlJ_SrJ_ :
         return {0, 4, 3, 4};
      case ValidFaceName::J_RlJ_Sr :
         return {4, 0, 4, 3};
      case ValidFaceName::SlJ_RrJ_ :
         return {2, 4, 1, 4};
      case ValidFaceName::J_SrJ_Rl :
         return {4, 3, 4, 0};
      case ValidFaceName::RrJ_SlJ_ :
         return {1, 4, 2, 4};
      case ValidFaceName::J_RrJ_Sl :
         return {4, 1, 4, 2};
      case ValidFaceName::SrJ_RlJ_ :
         return {3, 4, 0, 4};
      default:
         return {};
   }
}