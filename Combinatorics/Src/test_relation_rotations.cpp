/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "relation_rotations.h"

#include "cube_generator.h"

SCENARIO( "Relation can be rotated properly" ) {
   GIVEN( "A valid and an arbitrary Relation" ) {
      relation_t valid_relation = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
      relation_t arbitrary_relation = {0,1,2,2, 4,0,1,2, 3,4,0,2};
      WHEN( "We apply the know rotations" ) {
         auto const valid_bottom_front        = RelationRotation::Bottom(    valid_relation );
         auto const valid_bottom_front_rot90  = RelationRotation::Bottom90(  valid_relation );
         auto const valid_bottom_front_rot180 = RelationRotation::Bottom180( valid_relation );
         auto const valid_bottom_front_rot270 = RelationRotation::Bottom270( valid_relation );
         auto const valid_west_front          = RelationRotation::West(      valid_relation );
         auto const valid_west_front_rot90    = RelationRotation::West90(    valid_relation );
         auto const valid_west_front_rot180   = RelationRotation::West180(   valid_relation );
         auto const valid_west_front_rot270   = RelationRotation::West270(   valid_relation );
         auto const valid_south_front         = RelationRotation::South(     valid_relation );
         auto const valid_south_front_rot90   = RelationRotation::South90(   valid_relation );
         auto const valid_south_front_rot180  = RelationRotation::South180(  valid_relation );
         auto const valid_south_front_rot270  = RelationRotation::South270(  valid_relation );
         auto const valid_top_front           = RelationRotation::Top(       valid_relation );
         auto const valid_top_front_rot90     = RelationRotation::Top90(     valid_relation );
         auto const valid_top_front_rot180    = RelationRotation::Top180(    valid_relation );
         auto const valid_top_front_rot270    = RelationRotation::Top270(    valid_relation );
         auto const valid_east_front          = RelationRotation::East(      valid_relation );
         auto const valid_east_front_rot90    = RelationRotation::East90(    valid_relation );
         auto const valid_east_front_rot180   = RelationRotation::East180(   valid_relation );
         auto const valid_east_front_rot270   = RelationRotation::East270(   valid_relation );
         auto const valid_north_front         = RelationRotation::North(     valid_relation );
         auto const valid_north_front_rot90   = RelationRotation::North90(   valid_relation );
         auto const valid_north_front_rot180  = RelationRotation::North180(  valid_relation );
         auto const valid_north_front_rot270  = RelationRotation::North270(  valid_relation );
         auto const arbitrary_bottom_front        = RelationRotation::Bottom(    arbitrary_relation );
         auto const arbitrary_bottom_front_rot90  = RelationRotation::Bottom90(  arbitrary_relation );
         auto const arbitrary_bottom_front_rot180 = RelationRotation::Bottom180( arbitrary_relation );
         auto const arbitrary_bottom_front_rot270 = RelationRotation::Bottom270( arbitrary_relation );
         auto const arbitrary_west_front          = RelationRotation::West(      arbitrary_relation );
         auto const arbitrary_west_front_rot90    = RelationRotation::West90(    arbitrary_relation );
         auto const arbitrary_west_front_rot180   = RelationRotation::West180(   arbitrary_relation );
         auto const arbitrary_west_front_rot270   = RelationRotation::West270(   arbitrary_relation );
         auto const arbitrary_south_front         = RelationRotation::South(     arbitrary_relation );
         auto const arbitrary_south_front_rot90   = RelationRotation::South90(   arbitrary_relation );
         auto const arbitrary_south_front_rot180  = RelationRotation::South180(  arbitrary_relation );
         auto const arbitrary_south_front_rot270  = RelationRotation::South270(  arbitrary_relation );
         auto const arbitrary_top_front           = RelationRotation::Top(       arbitrary_relation );
         auto const arbitrary_top_front_rot90     = RelationRotation::Top90(     arbitrary_relation );
         auto const arbitrary_top_front_rot180    = RelationRotation::Top180(    arbitrary_relation );
         auto const arbitrary_top_front_rot270    = RelationRotation::Top270(    arbitrary_relation );
         auto const arbitrary_east_front          = RelationRotation::East(      arbitrary_relation );
         auto const arbitrary_east_front_rot90    = RelationRotation::East90(    arbitrary_relation );
         auto const arbitrary_east_front_rot180   = RelationRotation::East180(   arbitrary_relation );
         auto const arbitrary_east_front_rot270   = RelationRotation::East270(   arbitrary_relation );
         auto const arbitrary_north_front         = RelationRotation::North(     arbitrary_relation );
         auto const arbitrary_north_front_rot90   = RelationRotation::North90(   arbitrary_relation );
         auto const arbitrary_north_front_rot180  = RelationRotation::North180(  arbitrary_relation );
         auto const arbitrary_north_front_rot270  = RelationRotation::North270(  arbitrary_relation );
         THEN( "The rotations match the analytically known rotations" ) {
            relation_t expected_valid_bottom_front        = {1,0,0,1, 1,1,0,1, 0,0,0,0};
            relation_t expected_valid_bottom_front_rot90  = {0,0,0,0, 0,0,1,0, 1,0,0,1};
            relation_t expected_valid_bottom_front_rot180 = {1,0,1,0, 1,0,0,0, 0,1,1,0};
            relation_t expected_valid_bottom_front_rot270 = {0,1,1,0, 0,1,1,1, 1,0,1,0};
            relation_t expected_valid_west_front          = {1,0,1,0, 1,0,0,0, 0,1,1,0};
            relation_t expected_valid_west_front_rot90    = {0,1,1,0, 0,1,1,1, 1,0,1,0};
            relation_t expected_valid_west_front_rot180   = {1,0,0,1, 1,1,0,1, 0,0,0,0};
            relation_t expected_valid_west_front_rot270   = {0,0,0,0, 0,0,1,0, 1,0,0,1};
            relation_t expected_valid_south_front         = {0,0,0,0, 0,0,1,0, 1,0,0,1};
            relation_t expected_valid_south_front_rot90   = {1,0,1,0, 1,0,0,0, 0,1,1,0};
            relation_t expected_valid_south_front_rot180  = {0,1,1,0, 0,1,1,1, 1,0,1,0};
            relation_t expected_valid_south_front_rot270  = {1,0,0,1, 1,1,0,1, 0,0,0,0};
            relation_t expected_valid_top_front           = {0,1,0,1, 0,0,0,1, 0,1,1,0};
            relation_t expected_valid_top_front_rot90     = {1,1,1,1, 0,1,0,0, 1,0,0,1};
            relation_t expected_valid_top_front_rot180    = {1,0,0,1, 1,0,1,1, 1,1,1,1};
            relation_t expected_valid_top_front_rot270    = {0,1,1,0, 1,1,1,0, 0,1,0,1};
            relation_t expected_valid_east_front          = {0,1,0,1, 0,0,0,1, 0,1,1,0};
            relation_t expected_valid_east_front_rot90    = {1,1,1,1, 0,1,0,0, 1,0,0,1};
            relation_t expected_valid_east_front_rot180   = {1,0,0,1, 1,0,1,1, 1,1,1,1};
            relation_t expected_valid_east_front_rot270   = {0,1,1,0, 1,1,1,0, 0,1,0,1};
            relation_t expected_valid_north_front         = {0,1,1,0, 1,1,1,0, 0,1,0,1};
            relation_t expected_valid_north_front_rot90   = {0,1,0,1, 0,0,0,1, 0,1,1,0};
            relation_t expected_valid_north_front_rot180  = {1,1,1,1, 0,1,0,0, 1,0,0,1};
            relation_t expected_valid_north_front_rot270  = {1,0,0,1, 1,0,1,1, 1,1,1,1};
            relation_t expected_arbitrary_bottom_front        = {3,4,0,1, 4,0,3,3, 0,0,2,2};
            relation_t expected_arbitrary_bottom_front_rot90  = {4,0,1,4, 0,2,3,0, 2,2,2,1};
            relation_t expected_arbitrary_bottom_front_rot180 = {1,2,3,0, 2,2,4,1, 2,1,1,4};
            relation_t expected_arbitrary_bottom_front_rot270 = {3,1,3,2, 2,1,1,3, 4,4,0,0};
            relation_t expected_arbitrary_west_front          = {3,0,0,2, 1,4,2,0, 3,3,4,0};
            relation_t expected_arbitrary_west_front_rot90    = {0,3,4,0, 3,4,3,0, 3,0,1,2};
            relation_t expected_arbitrary_west_front_rot180   = {2,0,4,3, 3,1,0,4, 2,2,1,0};
            relation_t expected_arbitrary_west_front_rot270   = {2,2,0,0, 2,1,2,4, 1,0,4,3};
            relation_t expected_arbitrary_south_front         = {0,1,2,2, 4,0,1,2, 3,4,0,2};
            relation_t expected_arbitrary_south_front_rot90   = {4,4,1,1, 3,0,0,2, 0,2,3,2};
            relation_t expected_arbitrary_south_front_rot180  = {2,2,1,4, 0,3,4,1, 1,2,3,1};
            relation_t expected_arbitrary_south_front_rot270  = {1,2,2,2, 1,3,2,1, 4,1,0,4};
            relation_t expected_arbitrary_top_front           = {2,1,0,3, 1,4,2,2, 0,4,3,0};
            relation_t expected_arbitrary_top_front_rot90     = {0,4,4,1, 0,3,2,0, 3,0,3,3};
            relation_t expected_arbitrary_top_front_rot180    = {1,0,2,4, 3,3,0,4, 3,3,1,1};
            relation_t expected_arbitrary_top_front_rot270    = {2,3,2,0, 3,1,1,2, 1,1,4,4};
            relation_t expected_arbitrary_east_front          = {1,3,2,1, 0,2,4,1, 4,1,2,2};
            relation_t expected_arbitrary_east_front_rot90    = {1,1,3,3, 4,2,1,2, 4,2,0,1};
            relation_t expected_arbitrary_east_front_rot180   = {4,2,3,1, 4,0,1,3, 0,1,3,3};
            relation_t expected_arbitrary_east_front_rot270   = {4,1,1,2, 0,3,4,3, 0,3,2,1};
            relation_t expected_arbitrary_north_front         = {0,4,3,3, 1,4,3,0, 2,0,0,3};
            relation_t expected_arbitrary_north_front_rot90   = {0,0,4,4, 2,0,0,3, 2,3,1,3};
            relation_t expected_arbitrary_north_front_rot180  = {3,3,1,0, 2,1,0,4, 1,3,2,4};
            relation_t expected_arbitrary_north_front_rot270  = {3,3,0,3, 1,2,3,1, 1,4,4,0};
            REQUIRE( valid_bottom_front        == expected_valid_bottom_front );
            REQUIRE( valid_bottom_front_rot90  == expected_valid_bottom_front_rot90 );
            REQUIRE( valid_bottom_front_rot180 == expected_valid_bottom_front_rot180 );
            REQUIRE( valid_bottom_front_rot270 == expected_valid_bottom_front_rot270 );
            REQUIRE( valid_west_front          == expected_valid_west_front );
            REQUIRE( valid_west_front_rot90    == expected_valid_west_front_rot90 );
            REQUIRE( valid_west_front_rot180   == expected_valid_west_front_rot180 );
            REQUIRE( valid_west_front_rot270   == expected_valid_west_front_rot270 );
            REQUIRE( valid_south_front         == expected_valid_south_front );
            REQUIRE( valid_south_front_rot90   == expected_valid_south_front_rot90 );
            REQUIRE( valid_south_front_rot180  == expected_valid_south_front_rot180 );
            REQUIRE( valid_south_front_rot270  == expected_valid_south_front_rot270 );
            REQUIRE( valid_top_front           == expected_valid_top_front );
            REQUIRE( valid_top_front_rot90     == expected_valid_top_front_rot90 );
            REQUIRE( valid_top_front_rot180    == expected_valid_top_front_rot180 );
            REQUIRE( valid_top_front_rot270    == expected_valid_top_front_rot270 );
            REQUIRE( valid_east_front          == expected_valid_east_front );
            REQUIRE( valid_east_front_rot90    == expected_valid_east_front_rot90 );
            REQUIRE( valid_east_front_rot180   == expected_valid_east_front_rot180 );
            REQUIRE( valid_east_front_rot270   == expected_valid_east_front_rot270 );
            REQUIRE( valid_north_front         == expected_valid_north_front );
            REQUIRE( valid_north_front_rot90   == expected_valid_north_front_rot90 );
            REQUIRE( valid_north_front_rot180  == expected_valid_north_front_rot180 );
            REQUIRE( valid_north_front_rot270  == expected_valid_north_front_rot270 );
            REQUIRE( arbitrary_bottom_front        == expected_arbitrary_bottom_front );
            REQUIRE( arbitrary_bottom_front_rot90  == expected_arbitrary_bottom_front_rot90 );
            REQUIRE( arbitrary_bottom_front_rot180 == expected_arbitrary_bottom_front_rot180 );
            REQUIRE( arbitrary_bottom_front_rot270 == expected_arbitrary_bottom_front_rot270 );
            REQUIRE( arbitrary_west_front          == expected_arbitrary_west_front );
            REQUIRE( arbitrary_west_front_rot90    == expected_arbitrary_west_front_rot90 );
            REQUIRE( arbitrary_west_front_rot180   == expected_arbitrary_west_front_rot180 );
            REQUIRE( arbitrary_west_front_rot270   == expected_arbitrary_west_front_rot270 );
            REQUIRE( arbitrary_south_front         == expected_arbitrary_south_front );
            REQUIRE( arbitrary_south_front_rot90   == expected_arbitrary_south_front_rot90 );
            REQUIRE( arbitrary_south_front_rot180  == expected_arbitrary_south_front_rot180 );
            REQUIRE( arbitrary_south_front_rot270  == expected_arbitrary_south_front_rot270 );
            REQUIRE( arbitrary_top_front           == expected_arbitrary_top_front );
            REQUIRE( arbitrary_top_front_rot90     == expected_arbitrary_top_front_rot90 );
            REQUIRE( arbitrary_top_front_rot180    == expected_arbitrary_top_front_rot180 );
            REQUIRE( arbitrary_top_front_rot270    == expected_arbitrary_top_front_rot270 );
            REQUIRE( arbitrary_east_front          == expected_arbitrary_east_front );
            REQUIRE( arbitrary_east_front_rot90    == expected_arbitrary_east_front_rot90 );
            REQUIRE( arbitrary_east_front_rot180   == expected_arbitrary_east_front_rot180 );
            REQUIRE( arbitrary_east_front_rot270   == expected_arbitrary_east_front_rot270 );
            REQUIRE( arbitrary_north_front         == expected_arbitrary_north_front );
            REQUIRE( arbitrary_north_front_rot90   == expected_arbitrary_north_front_rot90 );
            REQUIRE( arbitrary_north_front_rot180  == expected_arbitrary_north_front_rot180 );
            REQUIRE( arbitrary_north_front_rot270  == expected_arbitrary_north_front_rot270 );
         }
      }
   }
}