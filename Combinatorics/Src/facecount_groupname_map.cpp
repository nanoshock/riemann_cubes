/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "facecount_groupname_map.h"

#include <array>

#include "face_type_count_map.h"

namespace{
   /* The index works as follows (octal system):
    * 1                   x n x FaceType::AllRightR
    * 10                  x n x FaceType::LeftrightR
    * 100                 x n x FaceType::LeftrightRS
    * 1.000               x n x FaceType::AllLeftS
    * 10.000              x n x FaceType::LeftrightS
    * 100.000             x n x FaceType::AllJ
    * 1.000.000           x n x FaceType::InflowRJ
    * 10.000.000          x n x FaceType::OutflowRJ
    * 100.000.000         x n x FaceType::OpposingRJ
    * 1.000.000.000       x n x FaceType::OutflowSJ
    * 10.000.000.000      x n x FaceType::InflowSJ
    * 100.000.000.000     x n x FaceType::OpposingSJ
    * 1.000.000.000.000   x n x FaceType::InROutSJ
    * 10.000.000.000.000  x n x FaceType::OutRInSJ
    * 100.000.000.000.000 x n x FaceType::OpposingRSJ
    */
    std::uint_fast64_t CountIndexOfGroup( std::unordered_map<FaceType, std::uint_fast8_t> const& count_map ) {
      /*
       * [ 0] : FaceType::AllRightR    , [ 1] : FaceType::LeftrightR
       * [ 2] : FaceType::LeftrightRS  , [ 3] : FaceType::AllLeftS
       * [ 4] : FaceType::LeftrightS   , [ 5] : FaceType::AllJ
       * [ 6] : FaceType::InflowRJ     , [ 7] : FaceType::OutflowRJ
       * [ 8] : FaceType::OpposingRJ   , [ 9] : FaceType::OutflowSJ
       * [10] : FaceType::InflowSJ     , [11] : FaceType::OpposingSJ
       * [12] : FaceType::InROutSJ     , [13] : FaceType::OutRInSJ
       * [14] : FaceType::OpposingRSJ
       */
      std::array<std::uint_fast64_t, 15> intermediate = {
         count_map.at( FaceType::AllRightR ),   count_map.at( FaceType::LeftrightR ),
         count_map.at( FaceType::LeftrightRS ), count_map.at( FaceType::AllLeftS ),
         count_map.at( FaceType::LeftrightS ),  count_map.at( FaceType::AllJ ),
         count_map.at( FaceType::InflowRJ ),    count_map.at( FaceType::OutflowRJ ),
         count_map.at( FaceType::OpposingRJ ),  count_map.at( FaceType::OutflowSJ ),
         count_map.at( FaceType::InflowSJ ),    count_map.at( FaceType::OpposingSJ ),
         count_map.at( FaceType::InROutSJ ),    count_map.at( FaceType::OutRInSJ ),
         count_map.at( FaceType::OpposingRSJ )
      };
      return intermediate[ 0] * static_cast<std::uint_fast64_t>( 01             ) + intermediate[ 1] * static_cast<std::uint_fast64_t>( 010             ) + intermediate[ 2] * static_cast<std::uint_fast64_t>( 0100             ) +
             intermediate[ 3] * static_cast<std::uint_fast64_t>( 01000          ) + intermediate[ 4] * static_cast<std::uint_fast64_t>( 010000          ) + intermediate[ 5] * static_cast<std::uint_fast64_t>( 0100000          ) +
             intermediate[ 6] * static_cast<std::uint_fast64_t>( 01000000       ) + intermediate[ 7] * static_cast<std::uint_fast64_t>( 010000000       ) + intermediate[ 8] * static_cast<std::uint_fast64_t>( 0100000000       ) +
             intermediate[ 9] * static_cast<std::uint_fast64_t>( 01000000000    ) + intermediate[10] * static_cast<std::uint_fast64_t>( 010000000000    ) + intermediate[11] * static_cast<std::uint_fast64_t>( 0100000000000    ) +
             intermediate[12] * static_cast<std::uint_fast64_t>( 01000000000000 ) + intermediate[13] * static_cast<std::uint_fast64_t>( 010000000000000 ) + intermediate[14] * static_cast<std::uint_fast64_t>( 0100000000000000 );
   }

   std::unordered_map<std::uint_fast64_t, GroupName> indexed_group_name = {
      {0000000000000006, GroupName::SixAllRightR},
      {0000000000000024, GroupName::TwoLeftrightRFourAllRightR},
      {0000000000000033, GroupName::ThreeLeftrightRThreeAllRightR},
      {0000000003000003, GroupName::ThreeInflowRJThreeAllRightR},
      {0000000030000003, GroupName::ThreeOutflowRJThreeAllRightR},
      {0000000012000012, GroupName::OneOutflowRJTwoInflowRJOneLeftrightRTwoAllRightR},
      {0000000021000012, GroupName::TwoOutflowRJOneInflowRJOneLeftrightRTwoAllRightR},
      {0000000202000002, GroupName::TwoOpposingRJTwoInflowRJTwoAllRightR},
      {0200000002000002, GroupName::TwoOpposingRSJTwoInflowRJTwoAllRightR},
      {0000000220000002, GroupName::TwoOpposingRJTwoOutflowRJTwoAllRightR},
      {0200000020000002, GroupName::TwoOpposingRSJTwoOutflowRJTwoAllRightR},
      {0000000400000002, GroupName::FourOpposingRJTwoAllRightR},
      {0400000000001001, GroupName::FourOpposingRSJOneAllLeftSOneAllRightR},
      {0000000022100001, GroupName::TwoOutflowRJTwoInflowRJOneAllJOneAllRightR},
      {0020000002100001, GroupName::TwoOutRInSJTwoInflowRJOneAllJOneAllRightR},
      {0002000020100001, GroupName::TwoInROutSJTwoOutflowRJOneAllJOneAllRightR},
      {0022000000100001, GroupName::TwoOutRInSJTwoInROutSJOneAllJOneAllRightR},
      {0000000212000001, GroupName::TwoOpposingRJOneOutflowRJTwoInflowRJOneAllRightR},
      {0110000102000001, GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJTwoInflowRJOneAllRightR},
      {0200010002000001, GroupName::TwoOpposingRSJOneInflowSJTwoInflowRJOneAllRightR},
      {0000000221000001, GroupName::TwoOpposingRJTwoOutflowRJOneInflowRJOneAllRightR},
      {0020000201000001, GroupName::TwoOutRInSJTwoOpposingRJOneInflowRJOneAllRightR},
      {0101000120000001, GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneAllRightR},
      {0200001020000001, GroupName::TwoOpposingRSJOneOutflowSJTwoOutflowRJOneAllRightR},
      {0002000210000001, GroupName::TwoInROutSJTwoOpposingRJOneOutflowRJOneAllRightR},
      {0112000100000001, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllRightR},
      {0121000100000001, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllRightR},
      {0220001000000001, GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllRightR},
      {0202010000000001, GroupName::TwoOpposingRSJTwoInROutSJOneInflowSJOneAllRightR},
      {0000000000000060, GroupName::SixLeftrightR},
      {0000000003000030, GroupName::ThreeInflowRJThreeLeftrightR},
      {0000000030000030, GroupName::ThreeOutflowRJThreeLeftrightR},
      {0000000000000420, GroupName::FourLeftrightRSTwoLeftrightR},
      {0000000211000020, GroupName::TwoOpposingRJOneOutflowRJOneInflowRJTwoLeftrightR},
      {0200000011000020, GroupName::TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightR},
      {0000000400000020, GroupName::FourOpposingRJTwoLeftrightR},
      {0002000001000210, GroupName::TwoInROutSJOneInflowRJTwoLeftrightRSOneLeftrightR},
      {0020000010000210, GroupName::TwoOutRInSJOneOutflowRJTwoLeftrightRSOneLeftrightR},
      {0011000200000110, GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneLeftrightRSOneLeftrightR},
      {0200000200000110, GroupName::TwoOpposingRSJTwoOpposingRJOneLeftrightRSOneLeftrightR},
      {0211000000000110, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightRSOneLeftrightR},
      {0400000000010010, GroupName::FourOpposingRSJOneLeftrightSOneLeftrightR},
      {0000000004100010, GroupName::FourInflowRJOneAllJOneLeftrightR},
      {0002000002100010, GroupName::TwoInROutSJTwoInflowRJOneAllJOneLeftrightR},
      {0000000040100010, GroupName::FourOutflowRJOneAllJOneLeftrightR},
      {0020000020100010, GroupName::TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightR},
      {0004000000100010, GroupName::FourInROutSJOneAllJOneLeftrightR},
      {0040000000100010, GroupName::FourOutRInSJOneAllJOneLeftrightR},
      {0000000203000010, GroupName::TwoOpposingRJThreeInflowRJOneLeftrightR},
      {0101000102000010, GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightR},
      {0200001002000010, GroupName::TwoOpposingRSJOneOutflowSJTwoInflowRJOneLeftrightR},
      {0002000201000010, GroupName::TwoInROutSJTwoOpposingRJOneInflowRJOneLeftrightR},
      {0000000230000010, GroupName::TwoOpposingRJThreeOutflowRJOneLeftrightR},
      {0110000120000010, GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightR},
      {0200010020000010, GroupName::TwoOpposingRSJOneInflowSJTwoOutflowRJOneLeftrightR},
      {0020000210000010, GroupName::TwoOutRInSJTwoOpposingRJOneOutflowRJOneLeftrightR},
      {0103000100000010, GroupName::OneOpposingRSJThreeInROutSJOneOpposingRJOneLeftrightR},
      {0130000100000010, GroupName::OneOpposingRSJThreeOutRInSJOneOpposingRJOneLeftrightR},
      {0202001000000010, GroupName::TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightR},
      {0220010000000010, GroupName::TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightR},
      {0000000000020400, GroupName::TwoLeftrightSFourLeftrightRS},
      {0002001000010200, GroupName::TwoInROutSJOneOutflowSJOneLeftrightSTwoLeftrightRS},
      {0020010000010200, GroupName::TwoOutRInSJOneInflowSJOneLeftrightSTwoLeftrightRS},
      {0000200011000200, GroupName::TwoOpposingSJOneOutflowRJOneInflowRJTwoLeftrightRS},
      {0200000011000200, GroupName::TwoOpposingRSJOneOutflowRJOneInflowRJTwoLeftrightRS},
      {0000011200000200, GroupName::OneInflowSJOneOutflowSJTwoOpposingRJTwoLeftrightRS},
      {0000200200000200, GroupName::TwoOpposingSJTwoOpposingRJTwoLeftrightRS},
      {0200011000000200, GroupName::TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightRS},
      {0400000000000200, GroupName::FourOpposingRSJTwoLeftrightRS},
      {0011200000010100, GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneLeftrightSOneLeftrightRS},
      {0200200000010100, GroupName::TwoOpposingRSJTwoOpposingSJOneLeftrightSOneLeftrightRS},
      {0211000000010100, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneLeftrightSOneLeftrightRS},
      {0002000002100100, GroupName::TwoInROutSJTwoInflowRJOneAllJOneLeftrightRS},
      {0002001001100100, GroupName::TwoInROutSJOneOutflowSJOneInflowRJOneAllJOneLeftrightRS},
      {0020000020100100, GroupName::TwoOutRInSJTwoOutflowRJOneAllJOneLeftrightRS},
      {0020010010100100, GroupName::TwoOutRInSJOneInflowSJOneOutflowRJOneAllJOneLeftrightRS},
      {0002002000100100, GroupName::TwoInROutSJTwoOutflowSJOneAllJOneLeftrightRS},
      {0020020000100100, GroupName::TwoOutRInSJTwoInflowSJOneAllJOneLeftrightRS},
      {0101000102000100, GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoInflowRJOneLeftrightRS},
      {0101001101000100, GroupName::OneOpposingRSJOneInROutSJOneOutflowSJOneOpposingRJOneInflowRJOneLeftrightRS},
      {0002100101000100, GroupName::TwoInROutSJOneOpposingSJOneOpposingRJOneInflowRJOneLeftrightRS},
      {0101101001000100, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJOneInflowRJOneLeftrightRS},
      {0202000001000100, GroupName::TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightRS},
      {0110000120000100, GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJTwoOutflowRJOneLeftrightRS},
      {0110010110000100, GroupName::OneOpposingRSJOneOutRInSJOneInflowSJOneOpposingRJOneOutflowRJOneLeftrightRS},
      {0020100110000100, GroupName::TwoOutRInSJOneOpposingSJOneOpposingRJOneOutflowRJOneLeftrightRS},
      {0110110010000100, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowRJOneLeftrightRS},
      {0220000010000100, GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightRS},
      {0002101100000100, GroupName::TwoInROutSJOneOpposingSJOneOutflowSJOneOpposingRJOneLeftrightRS},
      {0020110100000100, GroupName::TwoOutRInSJOneOpposingSJOneInflowSJOneOpposingRJOneLeftrightRS},
      {0101102000000100, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightRS},
      {0202001000000100, GroupName::TwoOpposingRSJTwoInROutSJOneOutflowSJOneLeftrightRS},
      {0110120000000100, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightRS},
      {0220010000000100, GroupName::TwoOpposingRSJTwoOutRInSJOneInflowSJOneLeftrightRS},
      {0000000000006000, GroupName::SixAllLeftS},
      {0000000000024000, GroupName::TwoLeftrightSFourAllLeftS},
      {0000000000033000, GroupName::ThreeLeftrightSThreeAllLeftS},
      {0000003000003000, GroupName::ThreeOutflowSJThreeAllLeftS},
      {0000030000003000, GroupName::ThreeInflowSJThreeAllLeftS},
      {0000012000012000, GroupName::OneInflowSJTwoOutflowSJOneLeftrightSTwoAllLeftS},
      {0000021000012000, GroupName::TwoInflowSJOneOutflowSJOneLeftrightSTwoAllLeftS},
      {0000202000002000, GroupName::TwoOpposingSJTwoOutflowSJTwoAllLeftS},
      {0200002000002000, GroupName::TwoOpposingRSJTwoOutflowSJTwoAllLeftS},
      {0000220000002000, GroupName::TwoOpposingSJTwoInflowSJTwoAllLeftS},
      {0200020000002000, GroupName::TwoOpposingRSJTwoInflowSJTwoAllLeftS},
      {0000400000002000, GroupName::FourOpposingSJTwoAllLeftS},
      {0000022000101000, GroupName::TwoInflowSJTwoOutflowSJOneAllJOneAllLeftS},
      {0020002000101000, GroupName::TwoOutRInSJTwoOutflowSJOneAllJOneAllLeftS},
      {0002020000101000, GroupName::TwoInROutSJTwoInflowSJOneAllJOneAllLeftS},
      {0022000000101000, GroupName::TwoOutRInSJTwoInROutSJOneAllJOneAllLeftS},
      {0200020001001000, GroupName::TwoOpposingRSJTwoInflowSJOneInflowRJOneAllLeftS},
      {0220000001001000, GroupName::TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllLeftS},
      {0200002010001000, GroupName::TwoOpposingRSJTwoOutflowSJOneOutflowRJOneAllLeftS},
      {0202000010001000, GroupName::TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllLeftS},
      {0000212000001000, GroupName::TwoOpposingSJOneInflowSJTwoOutflowSJOneAllLeftS},
      {0110102000001000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJTwoOutflowSJOneAllLeftS},
      {0000221000001000, GroupName::TwoOpposingSJTwoInflowSJOneOutflowSJOneAllLeftS},
      {0020201000001000, GroupName::TwoOutRInSJTwoOpposingSJOneOutflowSJOneAllLeftS},
      {0101120000001000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneAllLeftS},
      {0002210000001000, GroupName::TwoInROutSJTwoOpposingSJOneInflowSJOneAllLeftS},
      {0112100000001000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllLeftS},
      {0121100000001000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllLeftS},
      {0000000000060000, GroupName::SixLeftrightS},
      {0000003000030000, GroupName::ThreeOutflowSJThreeLeftrightS},
      {0000030000030000, GroupName::ThreeInflowSJThreeLeftrightS},
      {0000211000020000, GroupName::TwoOpposingSJOneInflowSJOneOutflowSJTwoLeftrightS},
      {0200011000020000, GroupName::TwoOpposingRSJOneInflowSJOneOutflowSJTwoLeftrightS},
      {0000400000020000, GroupName::FourOpposingSJTwoLeftrightS},
      {0000004000110000, GroupName::FourOutflowSJOneAllJOneLeftrightS},
      {0002002000110000, GroupName::TwoInROutSJTwoOutflowSJOneAllJOneLeftrightS},
      {0000040000110000, GroupName::FourInflowSJOneAllJOneLeftrightS},
      {0020020000110000, GroupName::TwoOutRInSJTwoInflowSJOneAllJOneLeftrightS},
      {0004000000110000, GroupName::FourInROutSJOneAllJOneLeftrightS},
      {0040000000110000, GroupName::FourOutRInSJOneAllJOneLeftrightS},
      {0200002001010000, GroupName::TwoOpposingRSJTwoOutflowSJOneInflowRJOneLeftrightS},
      {0202000001010000, GroupName::TwoOpposingRSJTwoInROutSJOneInflowRJOneLeftrightS},
      {0200020010010000, GroupName::TwoOpposingRSJTwoInflowSJOneOutflowRJOneLeftrightS},
      {0220000010010000, GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowRJOneLeftrightS},
      {0000203000010000, GroupName::TwoOpposingSJThreeOutflowSJOneLeftrightS},
      {0101102000010000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoOutflowSJOneLeftrightS},
      {0002201000010000, GroupName::TwoInROutSJTwoOpposingSJOneOutflowSJOneLeftrightS},
      {0000230000010000, GroupName::TwoOpposingSJThreeInflowSJOneLeftrightS},
      {0110120000010000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJTwoInflowSJOneLeftrightS},
      {0020210000010000, GroupName::TwoOutRInSJTwoOpposingSJOneInflowSJOneLeftrightS},
      {0103100000010000, GroupName::OneOpposingRSJThreeInROutSJOneOpposingSJOneLeftrightS},
      {0130100000010000, GroupName::OneOpposingRSJThreeOutRInSJOneOpposingSJOneLeftrightS},
      {0000000000600000, GroupName::SixAllJ},
      {0000000003300000, GroupName::ThreeInflowRJThreeAllJ},
      {0002000001300000, GroupName::TwoInROutSJOneInflowRJThreeAllJ},
      {0000000030300000, GroupName::ThreeOutflowRJThreeAllJ},
      {0020000010300000, GroupName::TwoOutRInSJOneOutflowRJThreeAllJ},
      {0000003000300000, GroupName::ThreeOutflowSJThreeAllJ},
      {0002001000300000, GroupName::TwoInROutSJOneOutflowSJThreeAllJ},
      {0000030000300000, GroupName::ThreeInflowSJThreeAllJ},
      {0020010000300000, GroupName::TwoOutRInSJOneInflowSJThreeAllJ},
      {0000000202200000, GroupName::TwoOpposingRJTwoInflowRJTwoAllJ},
      {0101000101200000, GroupName::OneOpposingRSJOneInROutSJOneOpposingRJOneInflowRJTwoAllJ},
      {0200001001200000, GroupName::TwoOpposingRSJOneOutflowSJOneInflowRJTwoAllJ},
      {0000000220200000, GroupName::TwoOpposingRJTwoOutflowRJTwoAllJ},
      {0110000110200000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoAllJ},
      {0200010010200000, GroupName::TwoOpposingRSJOneInflowSJOneOutflowRJTwoAllJ},
      {0000000400200000, GroupName::FourOpposingRJTwoAllJ},
      {0200000200200000, GroupName::TwoOpposingRSJTwoOpposingRJTwoAllJ},
      {0002100100200000, GroupName::TwoInROutSJOneOpposingSJOneOpposingRJTwoAllJ},
      {0020100100200000, GroupName::TwoOutRInSJOneOpposingSJOneOpposingRJTwoAllJ},
      {0200100100200000, GroupName::TwoOpposingRSJOneOpposingSJOneOpposingRJTwoAllJ},
      {0000202000200000, GroupName::TwoOpposingSJTwoOutflowSJTwoAllJ},
      {0101101000200000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowSJTwoAllJ},
      {0000220000200000, GroupName::TwoOpposingSJTwoInflowSJTwoAllJ},
      {0110110000200000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoAllJ},
      {0000400000200000, GroupName::FourOpposingSJTwoAllJ},
      {0200200000200000, GroupName::TwoOpposingRSJTwoOpposingSJTwoAllJ},
      {0202000000200000, GroupName::TwoOpposingRSJTwoInROutSJTwoAllJ},
      {0220000000200000, GroupName::TwoOpposingRSJTwoOutRInSJTwoAllJ},
      {0400000000200000, GroupName::FourOpposingRSJTwoAllJ},
      {0000000212100000, GroupName::TwoOpposingRJOneOutflowRJTwoInflowRJOneAllJ},
      {0200000012100000, GroupName::TwoOpposingRSJOneOutflowRJTwoInflowRJOneAllJ},
      {0000000221100000, GroupName::TwoOpposingRJTwoOutflowRJOneInflowRJOneAllJ},
      {0200000021100000, GroupName::TwoOpposingRSJTwoOutflowRJOneInflowRJOneAllJ},
      {0101000111100000, GroupName::OneOpposingRSJOneInROutSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ},
      {0110000111100000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJOneInflowRJOneAllJ},
      {0101100011100000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ},
      {0110100011100000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowRJOneInflowRJOneAllJ},
      {0011000201100000, GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneInflowRJOneAllJ},
      {0110001101100000, GroupName::OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneInflowRJOneAllJ},
      {0110101001100000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneOutflowSJOneInflowRJOneAllJ},
      {0020200001100000, GroupName::TwoOutRInSJTwoOpposingSJOneInflowRJOneAllJ},
      {0211000001100000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowRJOneAllJ},
      {0220000001100000, GroupName::TwoOpposingRSJTwoOutRInSJOneInflowRJOneAllJ},
      {0011000210100000, GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneAllJ},
      {0101010110100000, GroupName::OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneAllJ},
      {0101110010100000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowRJOneAllJ},
      {0002200010100000, GroupName::TwoInROutSJTwoOpposingSJOneOutflowRJOneAllJ},
      {0202000010100000, GroupName::TwoOpposingRSJTwoInROutSJOneOutflowRJOneAllJ},
      {0211000010100000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneAllJ},
      {0020001200100000, GroupName::TwoOutRInSJOneOutflowSJTwoOpposingRJOneAllJ},
      {0002010200100000, GroupName::TwoInROutSJOneInflowSJTwoOpposingRJOneAllJ},
      {0101011100100000, GroupName::OneOpposingRSJOneInROutSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ},
      {0110011100100000, GroupName::OneOpposingRSJOneOutRInSJOneInflowSJOneOutflowSJOneOpposingRJOneAllJ},
      {0112000100100000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneAllJ},
      {0121000100100000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllJ},
      {0000212000100000, GroupName::TwoOpposingSJOneInflowSJTwoOutflowSJOneAllJ},
      {0200012000100000, GroupName::TwoOpposingRSJOneInflowSJTwoOutflowSJOneAllJ},
      {0000221000100000, GroupName::TwoOpposingSJTwoInflowSJOneOutflowSJOneAllJ},
      {0200021000100000, GroupName::TwoOpposingRSJTwoInflowSJOneOutflowSJOneAllJ},
      {0101111000100000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ},
      {0110111000100000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneAllJ},
      {0011201000100000, GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneOutflowSJOneAllJ},
      {0211001000100000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneAllJ},
      {0220001000100000, GroupName::TwoOpposingRSJTwoOutRInSJOneOutflowSJOneAllJ},
      {0011210000100000, GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneAllJ},
      {0202010000100000, GroupName::TwoOpposingRSJTwoInROutSJOneInflowSJOneAllJ},
      {0211010000100000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneAllJ},
      {0112100000100000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneAllJ},
      {0121100000100000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneAllJ},
      {0000000006000000, GroupName::SixInflowRJ},
      {0002000004000000, GroupName::TwoInROutSJFourInflowRJ},
      {0000000033000000, GroupName::ThreeOutflowRJThreeInflowRJ},
      {0020000013000000, GroupName::TwoOutRInSJOneOutflowRJThreeInflowRJ},
      {0000003003000000, GroupName::ThreeOutflowSJThreeInflowRJ},
      {0002001003000000, GroupName::TwoInROutSJOneOutflowSJThreeInflowRJ},
      {0000030003000000, GroupName::ThreeInflowSJThreeInflowRJ},
      {0020010003000000, GroupName::TwoOutRInSJOneInflowSJThreeInflowRJ},
      {0000000222000000, GroupName::TwoOpposingRJTwoOutflowRJTwoInflowRJ},
      {0011000022000000, GroupName::OneOutRInSJOneInROutSJTwoOutflowRJTwoInflowRJ},
      {0110000112000000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingRJOneOutflowRJTwoInflowRJ},
      {0020001012000000, GroupName::TwoOutRInSJOneOutflowSJOneOutflowRJTwoInflowRJ},
      {0004000002000000, GroupName::FourInROutSJTwoInflowRJ},
      {0220000002000000, GroupName::TwoOpposingRSJTwoOutRInSJTwoInflowRJ},
      {0002000031000000, GroupName::TwoInROutSJThreeOutflowRJOneInflowRJ},
      {0101000121000000, GroupName::OneOpposingRSJOneInROutSJOneOpposingRJTwoOutflowRJOneInflowRJ},
      {0002010021000000, GroupName::TwoInROutSJOneInflowSJTwoOutflowRJOneInflowRJ},
      {0011000211000000, GroupName::OneOutRInSJOneInROutSJTwoOpposingRJOneOutflowRJOneInflowRJ},
      {0110001111000000, GroupName::OneOpposingRSJOneOutRInSJOneOutflowSJOneOpposingRJOneOutflowRJOneInflowRJ},
      {0101010111000000, GroupName::OneOpposingRSJOneInROutSJOneInflowSJOneOpposingRJOneOutflowRJOneInflowRJ},
      {0011100111000000, GroupName::OneOutRInSJOneInROutSJOneOpposingSJOneOpposingRJOneOutflowRJOneInflowRJ},
      {0011011011000000, GroupName::OneOutRInSJOneInROutSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ},
      {0200011011000000, GroupName::TwoOpposingRSJOneInflowSJOneOutflowSJOneOutflowRJOneInflowRJ},
      {0022000011000000, GroupName::TwoOutRInSJTwoInROutSJOneOutflowRJOneInflowRJ},
      {0211000011000000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowRJOneInflowRJ},
      {0020101101000000, GroupName::TwoOutRInSJOneOpposingSJOneOutflowSJOneOpposingRJOneInflowRJ},
      {0121000101000000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneInflowRJ},
      {0002003001000000, GroupName::TwoInROutSJThreeOutflowSJOneInflowRJ},
      {0020012001000000, GroupName::TwoOutRInSJOneInflowSJTwoOutflowSJOneInflowRJ},
      {0110111001000000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJOneOutflowSJOneInflowRJ},
      {0004001001000000, GroupName::FourInROutSJOneOutflowSJOneInflowRJ},
      {0031001001000000, GroupName::ThreeOutRInSJOneInROutSJOneOutflowSJOneInflowRJ},
      {0002030001000000, GroupName::TwoInROutSJThreeInflowSJOneInflowRJ},
      {0022010001000000, GroupName::TwoOutRInSJTwoInROutSJOneInflowSJOneInflowRJ},
      {0211010001000000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneInflowRJ},
      {0121100001000000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneInflowRJ},
      {0000000060000000, GroupName::SixOutflowRJ},
      {0020000040000000, GroupName::TwoOutRInSJFourOutflowRJ},
      {0000003030000000, GroupName::ThreeOutflowSJThreeOutflowRJ},
      {0002001030000000, GroupName::TwoInROutSJOneOutflowSJThreeOutflowRJ},
      {0000030030000000, GroupName::ThreeInflowSJThreeOutflowRJ},
      {0020010030000000, GroupName::TwoOutRInSJOneInflowSJThreeOutflowRJ},
      {0202000020000000, GroupName::TwoOpposingRSJTwoInROutSJTwoOutflowRJ},
      {0040000020000000, GroupName::FourOutRInSJTwoOutflowRJ},
      {0002110110000000, GroupName::TwoInROutSJOneOpposingSJOneInflowSJOneOpposingRJOneOutflowRJ},
      {0112000110000000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingRJOneOutflowRJ},
      {0020003010000000, GroupName::TwoOutRInSJThreeOutflowSJOneOutflowRJ},
      {0002021010000000, GroupName::TwoInROutSJTwoInflowSJOneOutflowSJOneOutflowRJ},
      {0101111010000000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOutflowRJ},
      {0022001010000000, GroupName::TwoOutRInSJTwoInROutSJOneOutflowSJOneOutflowRJ},
      {0211001010000000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneOutflowSJOneOutflowRJ},
      {0020030010000000, GroupName::TwoOutRInSJThreeInflowSJOneOutflowRJ},
      {0013010010000000, GroupName::OneOutRInSJThreeInROutSJOneInflowSJOneOutflowRJ},
      {0040010010000000, GroupName::FourOutRInSJOneInflowSJOneOutflowRJ},
      {0112100010000000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneOutflowRJ},
      {0022000200000000, GroupName::TwoOutRInSJTwoInROutSJTwoOpposingRJ},
      {0011111100000000, GroupName::OneOutRInSJOneInROutSJOneOpposingSJOneInflowSJOneOutflowSJOneOpposingRJ},
      {0121001100000000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOutflowSJOneOpposingRJ},
      {0112010100000000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneInflowSJOneOpposingRJ},
      {0000006000000000, GroupName::SixOutflowSJ},
      {0002004000000000, GroupName::TwoInROutSJFourOutflowSJ},
      {0000033000000000, GroupName::ThreeInflowSJThreeOutflowSJ},
      {0020013000000000, GroupName::TwoOutRInSJOneInflowSJThreeOutflowSJ},
      {0000222000000000, GroupName::TwoOpposingSJTwoInflowSJTwoOutflowSJ},
      {0011022000000000, GroupName::OneOutRInSJOneInROutSJTwoInflowSJTwoOutflowSJ},
      {0110112000000000, GroupName::OneOpposingRSJOneOutRInSJOneOpposingSJOneInflowSJTwoOutflowSJ},
      {0004002000000000, GroupName::FourInROutSJTwoOutflowSJ},
      {0220002000000000, GroupName::TwoOpposingRSJTwoOutRInSJTwoOutflowSJ},
      {0002031000000000, GroupName::TwoInROutSJThreeInflowSJOneOutflowSJ},
      {0101121000000000, GroupName::OneOpposingRSJOneInROutSJOneOpposingSJTwoInflowSJOneOutflowSJ},
      {0011211000000000, GroupName::OneOutRInSJOneInROutSJTwoOpposingSJOneInflowSJOneOutflowSJ},
      {0022011000000000, GroupName::TwoOutRInSJTwoInROutSJOneInflowSJOneOutflowSJ},
      {0211011000000000, GroupName::TwoOpposingRSJOneOutRInSJOneInROutSJOneInflowSJOneOutflowSJ},
      {0121101000000000, GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingSJOneOutflowSJ},
      {0000060000000000, GroupName::SixInflowSJ},
      {0020040000000000, GroupName::TwoOutRInSJFourInflowSJ},
      {0202020000000000, GroupName::TwoOpposingRSJTwoInROutSJTwoInflowSJ},
      {0040020000000000, GroupName::FourOutRInSJTwoInflowSJ},
      {0112110000000000, GroupName::OneOpposingRSJOneOutRInSJTwoInROutSJOneOpposingSJOneInflowSJ},
      {0022200000000000, GroupName::TwoOutRInSJTwoInROutSJTwoOpposingSJ},
      {0033000000000000, GroupName::ThreeOutRInSJThreeInROutSJ},
      {0222000000000000, GroupName::TwoOpposingRSJTwoOutRInSJTwoInROutSJ}
   };

   GroupName GroupNameOfIndex( std::uint_fast64_t index ) {
      return indexed_group_name.at( index );
   }

}

GroupName MapFaceCountToGroupName( std::vector<FaceType> const& face_count ) {
   // Todo: Make one-liner as son as debugged!
   auto map = CountMapOfGroup( face_count );
   auto index = CountIndexOfGroup( map );
   auto groupname = GroupNameOfIndex( index );
   return groupname;
}