/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include <vector>
#include <cstdint>
#include "face_names.h"
#include "relation_face_map.h"
#include "cube_generator.h"

SCENARIO( "Relations are correctly mapped to single faces" ) {
   GIVEN( "A random (non-existing) relation" ) {
      relation_t relation = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
      WHEN( "We decompose the relation into the six faces" ) {
         auto const west_face   = MapRelationToFace( relation, FaceName::West   );
         auto const east_face   = MapRelationToFace( relation, FaceName::East   );
         auto const south_face  = MapRelationToFace( relation, FaceName::South  );
         auto const north_face  = MapRelationToFace( relation, FaceName::North  );
         auto const bottom_face = MapRelationToFace( relation, FaceName::Bottom );
         auto const top_face    = MapRelationToFace( relation, FaceName::Top    );
         THEN( "We get the correct mapping" ) {
            face_t const west_expected   = { 9, 5,  1,  4};
            face_t const east_expected   = {11, 7,  3,  6};
            face_t const south_expected  = {10, 5,  2,  7};
            face_t const north_expected  = { 8, 4,  0,  6};
            face_t const bottom_expected = { 0, 1,  2,  3};
            face_t const top_expected    = { 8, 9, 10, 11};
            REQUIRE( west_face   == west_expected   );
            REQUIRE( east_face   == east_expected   );
            REQUIRE( south_face  == south_expected  );
            REQUIRE( north_face  == north_expected  );
            REQUIRE( bottom_face == bottom_expected );
            REQUIRE( top_face    == top_expected    );
         }
      }
   }
   GIVEN( "Two known valid relation" ) {
      auto const six_leftright_s = GenerateValidCube( ValidCubeName::SixLeftRightS );
      auto const three_three_r = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
      WHEN( "We decompose the relation into the six faces" ) {
         auto const six_west_face   = MapRelationToFace( six_leftright_s, FaceName::West   );
         auto const six_east_face   = MapRelationToFace( six_leftright_s, FaceName::East   );
         auto const six_south_face  = MapRelationToFace( six_leftright_s, FaceName::South  );
         auto const six_north_face  = MapRelationToFace( six_leftright_s, FaceName::North  );
         auto const six_bottom_face = MapRelationToFace( six_leftright_s, FaceName::Bottom );
         auto const six_top_face    = MapRelationToFace( six_leftright_s, FaceName::Top    );
         auto const three_three_west_face   = MapRelationToFace( three_three_r, FaceName::West   );
         auto const three_three_east_face   = MapRelationToFace( three_three_r, FaceName::East   );
         auto const three_three_south_face  = MapRelationToFace( three_three_r, FaceName::South  );
         auto const three_three_north_face  = MapRelationToFace( three_three_r, FaceName::North  );
         auto const three_three_bottom_face = MapRelationToFace( three_three_r, FaceName::Bottom );
         auto const three_three_top_face    = MapRelationToFace( three_three_r, FaceName::Top    );
         THEN( "We get the correct mapping" ) {
            face_t const expected_six_west_face   = {2,3,3,2};
            face_t const expected_six_east_face   = {3,2,2,3};
            face_t const expected_six_south_face  = {2,3,3,2};
            face_t const expected_six_north_face  = {3,2,2,3};
            face_t const expected_six_bottom_face = {2,3,3,2};
            face_t const expected_six_top_face    = {3,2,2,3};
            face_t const expected_three_three_west_face   = {0, 0, 0, 0};
            face_t const expected_three_three_east_face   = {1, 0, 0, 1};
            face_t const expected_three_three_south_face  = {0, 0, 0, 0};
            face_t const expected_three_three_north_face  = {1, 0, 0, 1};
            face_t const expected_three_three_bottom_face = {0, 0, 0, 0};
            face_t const expected_three_three_top_face    = {1, 0, 0, 1};
            REQUIRE( six_west_face   == expected_six_west_face );
            REQUIRE( six_east_face   == expected_six_east_face );
            REQUIRE( six_south_face  == expected_six_south_face );
            REQUIRE( six_north_face  == expected_six_north_face );
            REQUIRE( six_bottom_face == expected_six_bottom_face );
            REQUIRE( six_top_face    == expected_six_top_face );
            REQUIRE( three_three_west_face   == expected_three_three_west_face   );
            REQUIRE( three_three_east_face   == expected_three_three_east_face   );
            REQUIRE( three_three_south_face  == expected_three_three_south_face  );
            REQUIRE( three_three_north_face  == expected_three_three_north_face  );
            REQUIRE( three_three_bottom_face == expected_three_three_bottom_face );
            REQUIRE( three_three_top_face    == expected_three_three_top_face    );
         }
      }
   }
}