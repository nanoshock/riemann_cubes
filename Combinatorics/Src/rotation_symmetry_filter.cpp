/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "rotation_symmetry_filter.h"

#include <deque>
#include "relation_rotations.h"
#include "relation_symmetries.h"

namespace{
   bool RelationIsRotation( relation_t const& comparions, relation_t const& relation ) {
      if( comparions == RelationRotation::Bottom(    relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Bottom90(  relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Bottom180( relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Bottom270( relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Top(       relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Top90(     relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Top180(    relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::Top270(    relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::West(      relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::West90(    relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::West180(   relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::West270(   relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::East(      relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::East90(    relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::East180(   relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::East270(   relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::South(     relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::South90(   relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::South180(  relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::South270(  relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::North(     relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::North90(   relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::North180(  relation ) ) {
         return true;
      }
      if( comparions == RelationRotation::North270(  relation ) ) {
         return true;
      }
      return false;
   }

   bool RelationIsSymmetry( relation_t const& comparions, relation_t const& relation ) {
      if( comparions == RelationSymmetry::SymmetryOne(   relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetryTwo(   relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetryThree( relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetryFour(  relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetryFive(  relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetrySix(   relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetrySeven( relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetryEight( relation ) ) {
         return true;
      }
      if( comparions == RelationSymmetry::SymmetryNine(  relation ) ) {
         return true;
      }
      return false;
   }

   bool RelationIsRotationSymmetryCombination( relation_t const& comparions, relation_t const& relation ) {
      auto const symmetry_one   = RelationSymmetry::SymmetryOne(    relation );
      if( RelationIsRotation( comparions, symmetry_one ) ) {
         return true;
      }
      auto const symmetry_two   = RelationSymmetry::SymmetryTwo(    relation );
      if( RelationIsRotation( comparions, symmetry_two ) ) {
         return true;
      }
      auto const symmetry_three = RelationSymmetry::SymmetryThree(  relation );
      if( RelationIsRotation( comparions, symmetry_three ) ) {
         return true;
      }
      auto const symmetry_four  = RelationSymmetry::SymmetryFour(   relation );
      if( RelationIsRotation( comparions, symmetry_four ) ) {
         return true;
      }
      auto const symmetry_five  = RelationSymmetry::SymmetryFive(   relation );
      if( RelationIsRotation( comparions, symmetry_five ) ) {
         return true;
      }
      auto const symmetry_six   = RelationSymmetry::SymmetrySix(    relation );
      if( RelationIsRotation( comparions, symmetry_six ) ) {
         return true;
      }
      auto const symmetry_seven = RelationSymmetry::SymmetrySeven(  relation );
      if( RelationIsRotation( comparions, symmetry_seven ) ) {
         return true;
      }
      auto const symmetry_eight = RelationSymmetry::SymmetryEight ( relation );
      if( RelationIsRotation( comparions, symmetry_eight ) ) {
         return true;
      }
      auto const symmetry_nine  = RelationSymmetry::SymmetryNine(   relation );
      if( RelationIsRotation( comparions, symmetry_nine ) ) {
         return true;
      }
      return false;
   }
}

template<typename Function>
std::vector<relation_t> FilterOutByFunction( Function f, std::vector<relation_t> const& relations_in_group ) {
   std::deque<relation_t> result( std::begin( relations_in_group ), std::end( relations_in_group ) );
   std::size_t probed_elements = 0;
   while( probed_elements < result.size() ) {
      relation_t comparions = result.front();
      result.pop_front();
      for( auto it = std::begin( result ); it != std::end( result ); ) {
         if( f( comparions, *it ) ) {
            it = result.erase( it );
         } else {
            it++;
         }
      }
      // Our element to compare was filtered out as it is a rotation (none) of itself.
      result.push_back( comparions );
      probed_elements++;
   }
   std::vector<relation_t> return_typed( std::begin( result ), std::end( result ) );
   return return_typed;
}

std::vector<relation_t> FilterOutRotations( std::vector<relation_t> const& relations_in_group ) {
   return FilterOutByFunction( RelationIsRotation, relations_in_group );
}

std::vector<relation_t> FilterOutSymmetries( std::vector<relation_t> const& relations_in_group ) {
   return FilterOutByFunction( RelationIsSymmetry, relations_in_group );
}

std::vector<relation_t> FilterOutSymmetryRotationCombinations( std::vector<relation_t> const& relations_in_group ) {
   return FilterOutByFunction( RelationIsRotationSymmetryCombination, relations_in_group );
}