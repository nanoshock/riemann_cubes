/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>

#include <vector>
#include <cstdint>
#include "face_names.h"
#include "stringyfier.h"
#include "cube_generator.h"
#include "face_types.h"

SCENARIO( "Relations are correclty transformed to strings" ) {
   GIVEN( "Some relations" ) {
      relation_t invalid =  {0,0,0,1, 0,0,0,0, 1,1,0,1};
      auto const three_three_r = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
      WHEN( "The relations are transformed to simply int string" ) {
         auto const res_invalid = SimpleIntString( invalid );
         auto const res_thrthrR = SimpleIntString( three_three_r );
         THEN( "The result strings are a one-to-one representation of the input" ) {
            std::string const expected_invalid( "000100001101" );
            std::string const expected_thrthrR( "000000101001" );
            REQUIRE( res_invalid == expected_invalid );
            REQUIRE( res_thrthrR == expected_thrthrR );
         }
      }
      WHEN( "The relations are transformed to nice string" ) {
         auto const res_invalid = NiceString( invalid );
         auto const res_thrthrR = NiceString( three_three_r );
         THEN( "The results string is the one-to-one representation of the input" ) {
            std::string const expected_invalid( "R_l R_l R_l R_r | R_l R_l R_l R_l | R_r R_r R_l R_r" );
            std::string const expected_thrthrR( "R_l R_l R_l R_l | R_l R_l R_r R_l | R_r R_l R_l R_r" );
            REQUIRE( res_invalid == expected_invalid );
            REQUIRE( res_thrthrR == expected_thrthrR );
         }
      }
      WHEN( "The relations are transformed to face name strings" ) {
         auto const res_invalid = FaceNameString( invalid );
         auto const res_thrthrR = FaceNameString( three_three_r );
         THEN( "The results are expected representation" ) {
            std::string const expected_invalid( "West: Unkown | East: AllRightR-Rot270 | South: AllRightR-Rot180 | North: Unkown | Bottom: Unkown | Top: Unkown" );
            std::string const expected_thrthrR( "West: AllRightR-Rot180 | East: LeftRightR | South: AllRightR-Rot180 | North: LeftRightR | Bottom: AllRightR-Rot180 | Top: LeftRightR" );
            REQUIRE( res_invalid == expected_invalid );
            REQUIRE( res_thrthrR == expected_thrthrR );
         }
      }
   }

}

SCENARIO( "Face counts are correclty transformed to string" ) {
   GIVEN( "Some face counts" ) {
      std::vector<FaceType> const face_count_one = { FaceType::AllRightR,   FaceType::AllRightR,
                                                     FaceType::AllRightR,   FaceType::AllRightR,
                                                     FaceType::AllRightR,   FaceType::AllRightR };
      std::vector<FaceType> const face_count_two = { FaceType::LeftrightR,  FaceType::LeftrightR,
                                                     FaceType::LeftrightRS, FaceType::LeftrightRS,
                                                     FaceType::LeftrightS,  FaceType::LeftrightS };
      std::vector<FaceType> const face_count_thr = { FaceType::OutRInSJ,    FaceType::InROutSJ,
                                                     FaceType::AllJ,        FaceType::InflowSJ,
                                                     FaceType::OpposingRSJ, FaceType::OpposingSJ };
      WHEN( "The realtions are stringified" ) {
         std::string const obtained_one = FaceCountString( face_count_one );
         std::string const obtained_two = FaceCountString( face_count_two );
         std::string const obtained_thr = FaceCountString( face_count_thr );
         THEN( "The strings match the expectation" ) {
            std::string const expected_one = "6: AllRightR";
            std::string const expected_two = "2: LeftrightS\t+ 2: LeftrightRS\t+ 2: LeftrightR";
            std::string const expected_thr = "1: OpposingRSJ\t+ 1: OutRInSJ\t+ 1: InROutSJ\t+ 1: OpposingSJ\t+ 1: InflowSJ\t+ 1: AllJ";
            REQUIRE( obtained_one == expected_one );
            REQUIRE( obtained_two == expected_two );
            REQUIRE( obtained_thr == expected_thr );
         }
      }
   }
}

SCENARIO( "Group names are correctly stringified" ) {
   GIVEN( "Some group names" ) {
      auto const group_name_one   = GroupName::OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllRightR;
      auto const group_name_two   = GroupName::TwoOpposingRJThreeOutflowRJOneLeftrightR;
      auto const group_name_three = GroupName::SixLeftrightS;
      auto const group_name_four  = GroupName::TwoOpposingSJThreeInflowSJOneLeftrightS;
      WHEN( "The groups are stringified" ) {
         std::string obtained_one   = GroupNameString( group_name_one   );
         std::string obtained_two   = GroupNameString( group_name_two   );
         std::string obtained_three = GroupNameString( group_name_three );
         std::string obtained_four  = GroupNameString( group_name_four  );
         THEN( "The strings match teh expectation" ) {
            std::string expected_one   = "OneOpposingRSJTwoOutRInSJOneInROutSJOneOpposingRJOneAllRightR";
            std::string expected_two   = "TwoOpposingRJThreeOutflowRJOneLeftrightR";
            std::string expected_three = "SixLeftrightS";
            std::string expected_four  = "TwoOpposingSJThreeInflowSJOneLeftrightS";
            REQUIRE( obtained_one   == expected_one   );
            REQUIRE( obtained_two   == expected_two   );
            REQUIRE( obtained_three == expected_three );
            REQUIRE( obtained_four  == expected_four  );
         }
      }
   }
}

SCENARIO( "Relations can be transformed to Latex String" ) {
   GIVEN( "A relation with all waves" ) {
      relation_t const relation = {0,1,2,3, 4,0,1,2, 3,4,0,3};
      WHEN( "The relation is latex-stringified" ) {
         auto const latex_string = LatexString( relation );
         THEN( "The results fulfills the expectations" ) {
            std::string const expected = "\\NamedWaveLeft{R}{2}{1}\\NamedWaveRight{R}{3}{2}"
                                         "\\NamedWaveLeft{S}{3}{4}\\NamedWaveRight{S}{4}{1}"
                                         "\\NamedWave{J}{2}{6}\\NamedWaveLeft{R}{3}{7}"
                                         "\\NamedWaveRight{R}{1}{5}\\NamedWaveLeft{S}{4}{8}"
                                         "\\NamedWaveRight{S}{6}{5}\\NamedWave{J}{7}{6}"
                                         "\\NamedWaveLeft{R}{7}{8}\\NamedWaveRight{S}{8}{5}";
            REQUIRE( latex_string == expected );
         }
      }
   }
}

SCENARIO( "Face name enums can be transformed to string" ) {
   constexpr std::size_t number_of_faces = 6;
   GIVEN( "The six face names" ) {
      std::array<FaceName, number_of_faces> const face_names { FaceName::West, FaceName::East, FaceName::South, FaceName::North, FaceName::Bottom, FaceName::Top };
      WHEN( "We ask for the string representation of each face with and without padding" ) {
         std::array<std::string, number_of_faces> face_strings;
         std::array<std::string, number_of_faces> padded_face_strings;
         std::transform( std::cbegin( face_names ), std::cend( face_names ), std::begin( face_strings ), []( auto const fn ) { return FaceNameEnumString( fn ); } );
         std::transform( std::cbegin( face_names ), std::cend( face_names ), std::begin( padded_face_strings ), []( auto const fn ){ return FaceNameEnumString( fn, true ); } );
         THEN( "The face name strings match the expectation" ) {
            std::array<std::string, number_of_faces> const expected_strings = { "West", "East", "South", "North", "Bottom", "Top" };
            std::array<std::string, number_of_faces> const expected_padded_strings = { "West  ", "East  ", "South ", "North ", "Bottom", "Top   " };
            REQUIRE( face_strings == expected_strings );
            REQUIRE( padded_face_strings == expected_padded_strings );
         }
      }
   }
}

SCENARIO( "String representations of face relations" ) {
   GIVEN( "Two arbitrary face relations" ) {
      face_t const first_face = {0,2,3,4};
      face_t const second_face = {1,3,4,2};
      WHEN( "We ask for the string representation" ) {
         auto const first_face_string = FaceString( first_face );
         auto const second_face_string = FaceString( second_face );
         THEN( "The results are expected representation" ) {
            std::string const expected_first_face(  "R_l S_l S_r J__" );
            std::string const expected_second_face( "R_r S_r J__ S_l" );
            REQUIRE( first_face_string == expected_first_face );
            REQUIRE( second_face_string == expected_second_face );
         }
      }
   }
}
