/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "face_type_count_map.h"

std::unordered_map<FaceType, std::uint_fast8_t> CountMapOfGroup( std::vector<FaceType> const& group ) {
   std::unordered_map<FaceType, std::uint_fast8_t> type_count = {
      {FaceType::AllRightR, 0}, {FaceType::LeftrightR, 0}, {FaceType::LeftrightRS, 0},
      {FaceType::AllLeftS,  0}, {FaceType::LeftrightS, 0}, {FaceType::AllJ,        0},
      {FaceType::InflowRJ,  0}, {FaceType::OutflowRJ,  0}, {FaceType::OpposingRJ,  0},
      {FaceType::OutflowSJ, 0}, {FaceType::InflowSJ,   0}, {FaceType::OpposingSJ,  0},
      {FaceType::InROutSJ,  0}, {FaceType::OutRInSJ,   0}, {FaceType::OpposingRSJ, 0}
   };
   for( auto const type : group ) {
      type_count.at( type )++;
   }
   return type_count;
}