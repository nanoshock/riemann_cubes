/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "relation_groupname_sorter.h"

#include <algorithm>
#include "valid_combinations_rarefaction_only.h"
#include "cube_generator.h"

namespace {
   bool RelationInMapEntry( std::vector<relation_t> const& map_entry, relation_t const& r ) {
      return std::any_of( std::begin( map_entry ), std::end( map_entry ),
                          [&r](relation_t const& e){return e == r; }
                        );
   }
}

SCENARIO( "Found relations are correctly sorted into their groups" ) {
   GIVEN( "Known valid relations" ) {
      auto const valids = std::get<2>( StoredValidRarefactionCombinations() );
      WHEN( "They are sorted into their groups" ) {
         auto const valids_sorted_by_group = SortRelationsByGroupName( std::move( valids ) );
         THEN( "There are four groups" ) {
            REQUIRE( valids_sorted_by_group.size() == 4 );
         }
         THEN( "The group SixRightR has eight members" ) {
            REQUIRE( valids_sorted_by_group.at( GroupName::SixAllRightR ).size() == 8 );
         }
         THEN( "The relation SixLeftR is sorted into group SixRightR") {
            auto const relation = GenerateValidCube( ValidCubeName::SixLeftR );
            REQUIRE( RelationInMapEntry( valids_sorted_by_group.at( GroupName::SixAllRightR ), relation ) );
         }
         THEN( "The group TwoLeftrightRFourRightR has twelve members" ) {
            REQUIRE( valids_sorted_by_group.at( GroupName::TwoLeftrightRFourAllRightR ).size() == 12 );
         }
         THEN( "The relation FourRightRTwoLeftRightR is sorted into group TwoLeftrightRFourRightR") {
            auto const relation = GenerateValidCube( ValidCubeName::FourRightRTwoLeftRightR );
            REQUIRE( RelationInMapEntry( valids_sorted_by_group.at( GroupName::TwoLeftrightRFourAllRightR ), relation ) );
         }
         THEN( "The group ThreeLeftrightRThreeRightR has 16 members" ) {
            REQUIRE( valids_sorted_by_group.at( GroupName::ThreeLeftrightRThreeAllRightR ).size() == 16 );
         }
         THEN( "The relation ThreeRightRThreeLeftRightR is sorted into group ThreeLeftrightRThreeRightR") {
            auto const relation = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
            REQUIRE( RelationInMapEntry( valids_sorted_by_group.at( GroupName::ThreeLeftrightRThreeAllRightR ), relation ) );
         }
         THEN( "The group SixLeftRightR has two members" ) {
            REQUIRE( valids_sorted_by_group.at( GroupName::SixLeftrightR ).size() == 2 );
         }
         THEN( "The relation SixLeftRightR is sorted into group SixLeftRightR") {
            auto const relation = GenerateValidCube( ValidCubeName::SixLeftRightR );
            REQUIRE( RelationInMapEntry( valids_sorted_by_group.at( GroupName::SixLeftrightR ), relation ) );
         }
      }
   }
}