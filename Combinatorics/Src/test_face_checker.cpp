/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>

#include <vector>
#include <cstdint>

#include "face_checker.h"
#include "face_generator.h"

SCENARIO( "Face checker recognizes Faces correctly" ) {
   GIVEN( "Valid faces" ) {
      auto const four_right_r = GenerateValidFace( ValidFaceName::RrRrRrRr );
      auto const two_left_two_right_r = GenerateValidFace( ValidFaceName::RrRlRlRr );
      auto const two_left_two_right_rotated = GenerateValidFace( ValidFaceName::RlRrRrRl );
      WHEN( "The faces are checked" ) {
         auto const result_four_right  = CheckFace( four_right_r );
         auto const result_two_two     = CheckFace( two_left_two_right_r );
         auto const result_two_two_rot = CheckFace( two_left_two_right_rotated );
         THEN( "The face are accepted and the correct name is returned" ) {
            REQUIRE( std::get<0>( result_four_right  ) );
            REQUIRE( std::get<1>( result_four_right  ) == ValidFaceName::RrRrRrRr);
            REQUIRE( std::get<0>( result_two_two     ) );
            REQUIRE( std::get<1>( result_two_two     ) == ValidFaceName::RrRlRlRr );
            REQUIRE( std::get<0>( result_two_two_rot ) );
            REQUIRE( std::get<1>( result_two_two_rot ) == ValidFaceName::RlRrRrRl );
         }
      }
   }
   GIVEN( "Invalid faces" ) {
      face_t invalid_one = {0,0,0,1};
      face_t invalid_two = {1,1,0,1};
      WHEN( "The faces are checked" ) {
         auto const result_one = CheckFace( invalid_one );
         auto const result_two = CheckFace( invalid_two );
         THEN( "The faces are not accepted and the name is 'unkown'" ) {
            REQUIRE_FALSE( std::get<0>( result_one  ) );
            REQUIRE( std::get<1>( result_one  ) == ValidFaceName::Unkown );
            REQUIRE_FALSE( std::get<0>( result_two ) );
            REQUIRE( std::get<1>( result_two )  == ValidFaceName::Unkown );
         }
      }
   }
}