/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#ifndef RELATION_COMBINATION_EXAMINER_H
#define RELATION_COMBINATION_EXAMINER_H

#include <vector>
#include <cstdint>
#include <tuple>
#include <iostream>
#include "relation_checker.h"
#include "relation_aliases.h"

template<std::size_t S, std::size_t E>
std::tuple<std::size_t,std::size_t, std::vector<relation_t>> TestAllRelationCombinations( bool const print = true ) {
   static_assert( S <= E, "Start index must be smaller end index" );
   static_assert( E <= 5, "End index must be smaller equal five" );
   relation_t relation;
   relation.fill( static_cast<wave_t>( S ) );
   std::size_t count = 0;
   std::size_t found = 0;
   std::vector<relation_t> valids;
   valids.reserve( 1024 );
   for( wave_t r_1 = S; r_1 < E; ++r_1 ) {
      for( wave_t r_2 = S; r_2 < E; ++r_2 ){
         for( wave_t r_3 = S; r_3 < E; ++r_3 ) {
            for( wave_t r_4 = S; r_4 < E; ++r_4 ) {
               for( wave_t r_5 = S; r_5 < E; ++r_5 ) {
                  for( wave_t r_6 = S; r_6 < E; ++r_6 ) {
                     for( wave_t r_7 = S; r_7 < E; ++r_7 ) {
                        for( wave_t r_8 = S; r_8 < E; ++r_8 ) {
                           for( wave_t r_9 = S; r_9 < E; ++r_9 ) {
                              for( wave_t r_10 = S; r_10 < E; ++r_10 ) {
                                 for( wave_t r_11 = S; r_11 < E; ++r_11 ) {
                                    for( wave_t r_12 = S; r_12 < E; ++r_12 ) {
                                       auto const [valid, log] = CheckRelation( relation );
                                       if( print) {
                                          std::cout << log << "\n";
                                       }
                                       if( valid ) {
                                          valids.push_back( relation );
                                          found++;
                                       };
                                       count++;
                                       relation[11]++;
                                    }
                                    relation[11] = static_cast<wave_t>( S );
                                    relation[10]++;
                                 }
                                 relation[10] = static_cast<wave_t>( S );
                                 relation[9]++;
                              }
                              relation[9] = static_cast<wave_t>( S );
                              relation[8]++;
                           }
                           relation[8] = static_cast<wave_t>( S );
                           relation[7]++;
                        }
                        relation[7] = static_cast<wave_t>( S );
                        relation[6]++;
                     }
                     relation[6] = static_cast<wave_t>( S );
                     relation[5]++;
                  }
                  relation[5] = static_cast<wave_t>( S );
                  relation[4]++;
               }
               relation[4] = static_cast<wave_t>( S );
               relation[3]++;
            }
            relation[3] = static_cast<wave_t>( S );
            relation[2]++;
         }
         relation[2] = static_cast<wave_t>( S );
         relation[1]++;
      }
      relation[1] = static_cast<wave_t>( S );
      relation[0]++;
   }
   return std::make_tuple( count, found, valids );
}


#endif //RELATION_COMBINATION_EXAMINER_H
