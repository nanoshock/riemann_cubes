/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "relation_combination_examiner.h"
#include "relation_checker.h"
#include "cube_generator.h"

SCENARIO( "All Combinations are run", "[.long]" ) {
   GIVEN( "All Rarefaction combinations are run" ) {
      constexpr std::size_t S = 0;
      constexpr std::size_t E = 2;
      WHEN( "The Combinations are tested" ) {
         auto const [count, found, valids] = TestAllRelationCombinations<S, E>();
         THEN( "The total number of relations tested is correct" ) {
            REQUIRE( count == 4096 );
         }
         THEN( "The expected number of cases are found" ) {
            REQUIRE( found == 38 );
         }
         THEN( "Known cases are recovered" ) {
            auto const valid_1 = GenerateValidCube( ValidCubeName::SixRightR );
            auto const valid_2 = GenerateValidCube( ValidCubeName::FourRightRTwoLeftRightR );
            auto const valid_3 = GenerateValidCube( ValidCubeName::ThreeRightRThreeLeftRightR );
            auto const valid_4 = GenerateValidCube( ValidCubeName::SixLeftRightR );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_1]( auto const& v){ return v == valid_1; } ) );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_2]( auto const& v){ return v == valid_2; } ) );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_3]( auto const& v){ return v == valid_3; } ) );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_4]( auto const& v){ return v == valid_4; } ) );
         }
      }
   }
   GIVEN( "All Shockwave combinations are run" ) {
      constexpr std::size_t S = 2;
      constexpr std::size_t E = 4;
      WHEN( "The Combinations are tested" ) {
         auto const [count, found, valids] = TestAllRelationCombinations<S, E>();
         THEN( "The total number of relations tested is correct" ) {
            REQUIRE( count == 4096 );
         }
         THEN( "The expected number of cases are found" ) {
            REQUIRE( found == 38 );
         }
         THEN( "Known cases are recovered" ) {
            auto const valid_1 = GenerateValidCube( ValidCubeName::SixLeftS );
            auto const valid_2 = GenerateValidCube( ValidCubeName::FourLeftSTwoLeftRightS );
            auto const valid_3 = GenerateValidCube( ValidCubeName::ThreeLeftSThreeLeftRightS );
            auto const valid_4 = GenerateValidCube( ValidCubeName::SixLeftRightS );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_1]( auto const& v){ return v == valid_1; } ) );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_2]( auto const& v){ return v == valid_2; } ) );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_3]( auto const& v){ return v == valid_3; } ) );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid_4]( auto const& v){ return v == valid_4; } ) );
         }
      }
   }
   GIVEN( "All Slipline Combinations are run" ) {
      constexpr std::size_t S = 4;
      constexpr std::size_t E = 5;
      WHEN( "The Combinations are tested" ) {
         auto const [count, found, valids] = TestAllRelationCombinations<S, E>();
         THEN( "The total number of relations tested is correct" ) {
            REQUIRE( count == 1 );
         }
         THEN( "The expected number of cases are found" ) {
            REQUIRE( found == 1 );
         }
         THEN( "The known cases is recovered" ) {
            auto const valid = GenerateValidCube( ValidCubeName::SixAllJ );
            REQUIRE( valids.front() == valid );
         }
      }
   }
   GIVEN( "All Slipline + Shockwave Combinations are run" ) {
      constexpr std::size_t S = 2;
      constexpr std::size_t E = 5;
      WHEN( "The Combinations are tested" ) {
         auto const [count, found, valids] = TestAllRelationCombinations<S, E>();
         THEN( "The total number of relations tested is correct" ) {
            REQUIRE( count == 531441 );
         }
         THEN( "The expected number of cases are found" ) {
            REQUIRE( found == 495 );
         }
         THEN( "A (special) known cases is recovered" ) {
            auto const valid = GenerateValidCube( ValidCubeName::FourOppSTwoLeftS );
            REQUIRE( std::any_of( std::begin( valids ), std::end( valids ), [&valid]( auto const& v){ return v == valid; } ) );
         }
      }
   }
}
