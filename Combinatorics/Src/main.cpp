/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <iostream>

#include "relation_combination_examiner.h"
#include "stringyfier.h"
#include "relation_groupname_sorter.h"
#include "rotation_symmetry_filter.h"

// #include "valid_combinations.h"
// #include "valid_combinations_rarefaction_only.h"
// #include "valid_combinations_withoutj_only.h"

// Relation 0 R_left, 1 R_right, 2 S_left, 3 S_right, 4 J

int main( int argc, char* argv[] ) {

   std::vector<std::string> const arguments( argv + 1, argv + argc );
   if( arguments.size() > 1 ) {
      return -1;
   }
   bool const quite = ( arguments.size() == 1 && arguments.front() == "-q" ) ? true : false;

   auto const [count, found, valids] = TestAllRelationCombinations<0,4>( !quite );
   // auto const [count, found, valids] = StoredValidCombinations();
   // auto const [count, found, valids] = StoredValidRarefactionCombinations();
   // auto const [count, found, valids] = StoredValidRarefactionAndShockCombinations();
   std::cout << "Ran " << count << " relations - found: " << found << " valid combinations\n";
   auto const sorted_groups = SortRelationsByGroupName( std::move( valids ) );
   std::cout << "Unique Groups: "  << sorted_groups.size() << "\n";
   std::size_t relation_count_after_rotation_filtering = 0;
   std::size_t relation_count_after_symmetry_filtering = 0;
   std::size_t relation_count_after_combined_filtering = 0;
   for( auto const& [name, relations] : sorted_groups ) {
      std::cout << GroupNameString( name ) << "\n";
      auto const rotation_filtered = FilterOutRotations( relations );
      relation_count_after_rotation_filtering += rotation_filtered.size();
      auto const symmetry_filtered = FilterOutSymmetries( rotation_filtered );
      relation_count_after_symmetry_filtering += symmetry_filtered.size();
      auto const combined_filtered = FilterOutSymmetryRotationCombinations( symmetry_filtered );
      relation_count_after_combined_filtering += combined_filtered.size();
      std::cout << "\tCount: " << relations.size() << " \tPost Rot-Filter: " << rotation_filtered.size() << "\tPost Sym-Filter: " << symmetry_filtered.size()
                << "\tPost Comb-Filer: " <<  combined_filtered.size() << "\n";
   }
   std::cout << "After Rotation Filtering: " << relation_count_after_rotation_filtering << "\n";
   std::cout << "After Symmetry Filtering: " << relation_count_after_symmetry_filtering << "\n";
   std::cout << "After Combined Filtering: " << relation_count_after_combined_filtering << "\n";
}
