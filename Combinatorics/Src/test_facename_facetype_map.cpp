/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "facename_facetype_map.h"
#include "valid_face_names.h"

SCENARIO( "Facenames are correctly mapped to facetypes" ) {
    GIVEN( "All the facenames" ) {
        auto const right_r       = ValidFaceName::RrRrRrRr; auto const right_r_rot90       = ValidFaceName::RlRrRlRr; auto const right_r_rot180       = ValidFaceName::RlRlRlRl; auto const right_r_rot270        = ValidFaceName::RrRlRrRl;
        auto const leftright_r   = ValidFaceName::RrRlRlRr; auto const leftright_r_rot     = ValidFaceName::RlRrRrRl;
        auto const leftright_rs  = ValidFaceName::RrSrRlSl; auto const leftright_rs_rot    = ValidFaceName::SrRrSlSl; auto const leftright_rs_sym     = ValidFaceName::RlSlRrSr; auto const leftright_rs_sym_rot  = ValidFaceName::SlRlSrRr;
        auto const left_s        = ValidFaceName::SlSlSlSl; auto const left_s_rot90        = ValidFaceName::SrSlSrSl; auto const left_s_rot180        = ValidFaceName::SrSrSrSr; auto const left_s_rot270         = ValidFaceName::SlSrSlSr;
        auto const leftright_s   = ValidFaceName::SlSrSrSl; auto const leftright_s_rot     = ValidFaceName::SrSlSlSr;
        auto const all_j         = ValidFaceName::J_J_J_J_;
        auto const r_inflow_j    = ValidFaceName::RrJ_J_Rr; auto const r_inflow_j_rot90    = ValidFaceName::RlRrJ_J_; auto const r_inflow_j_rot180    = ValidFaceName::J_RlRlJ_; auto const r_inflow_j_rot270     = ValidFaceName::J_J_RrRl;
        auto const r_outflow_j   = ValidFaceName::RlJ_J_Rl; auto const r_outflow_j_rot90   = ValidFaceName::RrRlJ_J_; auto const r_outflow_j_rot180   = ValidFaceName::J_RrRrJ_; auto const r_outflow_j_rot270    = ValidFaceName::J_J_RlRr;
        auto const r_opposing_j  = ValidFaceName::J_RrJ_Rr; auto const r_opposing_j_rot90  = ValidFaceName::RlJ_RlJ_; auto const r_opposing_j_rot180  = ValidFaceName::J_RlJ_Rl; auto const r_opposing_j_rot270   = ValidFaceName::RrJ_RrJ_;
        auto const s_outflow_j   = ValidFaceName::SlJ_J_Sl; auto const s_outflow_j_rot90   = ValidFaceName::SrSlJ_J_; auto const s_outflow_j_rot180   = ValidFaceName::J_SrSrJ_; auto const s_outflow_j_rot270    = ValidFaceName::J_J_SlSr;
        auto const s_inflow_j    = ValidFaceName::SrJ_J_Sr; auto const s_inflow_j_rot90    = ValidFaceName::SlSrJ_J_; auto const s_inflow_j_rot180    = ValidFaceName::J_SlSlJ_; auto const s_inflow_j_rot270     = ValidFaceName::J_J_SrSl;
        auto const s_opposing_j  = ValidFaceName::J_SlJ_Sl; auto const s_opposing_j_rot90  = ValidFaceName::SrJ_SrJ_; auto const s_opposing_j_rot180  = ValidFaceName::J_SrJ_Sr; auto const s_opposing_j_rot270   = ValidFaceName::SlJ_SlJ_;
        auto const r_in_s_out_j  = ValidFaceName::RrJ_J_Sl; auto const r_in_s_out_j_rot90  = ValidFaceName::SrRrJ_J_; auto const r_in_s_out_j_rot180  = ValidFaceName::J_SrRlJ_; auto const r_in_s_out_j_rot270   = ValidFaceName::J_J_SlRl; auto const r_in_s_out_j_sym  = ValidFaceName::SlJ_J_Rr; auto const r_in_s_out_j_sym_rot90  = ValidFaceName::RlSlJ_J_; auto const r_in_s_out_j_sym_rot180  = ValidFaceName::J_RlSrJ_; auto const r_in_s_out_j_sym_rot270  = ValidFaceName::J_J_RrSr;
        auto const r_out_s_in_j  = ValidFaceName::RlJ_J_Sr; auto const r_out_s_in_j_rot90  = ValidFaceName::SlRlJ_J_; auto const r_out_s_in_j_rot180  = ValidFaceName::J_SlRrJ_; auto const r_out_s_in_j_rot270   = ValidFaceName::J_J_SrRr; auto const r_out_s_in_j_sym  = ValidFaceName::SrJ_J_Rl; auto const r_out_s_in_j_sym_rot90  = ValidFaceName::RrSrJ_J_; auto const r_out_s_in_j_sym_rot180  = ValidFaceName::J_RrSlJ_; auto const r_out_s_in_j_sym_rot270  = ValidFaceName::J_J_RlSl;
        auto const rs_opposing_j = ValidFaceName::J_SlJ_Rr; auto const rs_opposing_j_rot90 = ValidFaceName::RlJ_SrJ_; auto const rs_opposing_j_rot180 = ValidFaceName::J_RlJ_Sr; auto const rs_opposing_j_rot270  = ValidFaceName::SlJ_RrJ_; auto const rs_opposing_j_sym = ValidFaceName::J_SrJ_Rl; auto const rs_opposing_j_sym_rot90 = ValidFaceName::RrJ_SlJ_; auto const rs_opposing_j_sym_rot180 = ValidFaceName::J_RrJ_Sl; auto const rs_opposing_j_sym_rot270 = ValidFaceName::SrJ_RlJ_;
        WHEN( "The face names are mapped to a face type" ) {
            auto const right_r_mapped       = MapFaceNameToFaceType( right_r       ); auto const right_r_rot90_mapped       = MapFaceNameToFaceType( right_r_rot90       ); auto const right_r_rot180_mapped       = MapFaceNameToFaceType( right_r_rot180       ); auto const right_r_rot270_mapped         = MapFaceNameToFaceType( right_r_rot270        );
            auto const leftright_r_mapped   = MapFaceNameToFaceType( leftright_r   ); auto const leftright_r_rot_mapped     = MapFaceNameToFaceType( leftright_r_rot     );
            auto const leftright_rs_mapped  = MapFaceNameToFaceType( leftright_rs  ); auto const leftright_rs_rot_mapped    = MapFaceNameToFaceType( leftright_rs_rot    ); auto const leftright_rs_sym_mapped     = MapFaceNameToFaceType( leftright_rs_sym     ); auto const leftright_rs_sym_rot_mapped  = MapFaceNameToFaceType( leftright_rs_sym_rot  );
            auto const left_s_mapped        = MapFaceNameToFaceType( left_s        ); auto const left_s_rot90_mapped        = MapFaceNameToFaceType( left_s_rot90        ); auto const left_s_rot180_mapped        = MapFaceNameToFaceType( left_s_rot180        ); auto const left_s_rot270_mapped         = MapFaceNameToFaceType( left_s_rot270         );
            auto const leftright_s_mapped   = MapFaceNameToFaceType( leftright_s   ); auto const leftright_s_rot_mapped     = MapFaceNameToFaceType( leftright_s_rot     );
            auto const all_j_mapped         = MapFaceNameToFaceType( all_j         );
            auto const r_inflow_j_mapped    = MapFaceNameToFaceType( r_inflow_j    ); auto const r_inflow_j_rot90_mapped    = MapFaceNameToFaceType( r_inflow_j_rot90    ); auto const r_inflow_j_rot180_mapped    = MapFaceNameToFaceType( r_inflow_j_rot180    ); auto const r_inflow_j_rot270_mapped     = MapFaceNameToFaceType( r_inflow_j_rot270     );
            auto const r_outflow_j_mapped   = MapFaceNameToFaceType( r_outflow_j   ); auto const r_outflow_j_rot90_mapped   = MapFaceNameToFaceType( r_outflow_j_rot90   ); auto const r_outflow_j_rot180_mapped   = MapFaceNameToFaceType( r_outflow_j_rot180   ); auto const r_outflow_j_rot270_mapped    = MapFaceNameToFaceType( r_outflow_j_rot270    );
            auto const r_opposing_j_mapped  = MapFaceNameToFaceType( r_opposing_j  ); auto const r_opposing_j_rot90_mapped  = MapFaceNameToFaceType( r_opposing_j_rot90  ); auto const r_opposing_j_rot180_mapped  = MapFaceNameToFaceType( r_opposing_j_rot180  ); auto const r_opposing_j_rot270_mapped   = MapFaceNameToFaceType( r_opposing_j_rot270   );
            auto const s_outflow_j_mapped   = MapFaceNameToFaceType( s_outflow_j   ); auto const s_outflow_j_rot90_mapped   = MapFaceNameToFaceType( s_outflow_j_rot90   ); auto const s_outflow_j_rot180_mapped   = MapFaceNameToFaceType( s_outflow_j_rot180   ); auto const s_outflow_j_rot270_mapped    = MapFaceNameToFaceType( s_outflow_j_rot270    );
            auto const s_inflow_j_mapped    = MapFaceNameToFaceType( s_inflow_j    ); auto const s_inflow_j_rot90_mapped    = MapFaceNameToFaceType( s_inflow_j_rot90    ); auto const s_inflow_j_rot180_mapped    = MapFaceNameToFaceType( s_inflow_j_rot180    ); auto const s_inflow_j_rot270_mapped     = MapFaceNameToFaceType( s_inflow_j_rot270     );
            auto const s_opposing_j_mapped  = MapFaceNameToFaceType( s_opposing_j  ); auto const s_opposing_j_rot90_mapped  = MapFaceNameToFaceType( s_opposing_j_rot90  ); auto const s_opposing_j_rot180_mapped  = MapFaceNameToFaceType( s_opposing_j_rot180  ); auto const s_opposing_j_rot270_mapped   = MapFaceNameToFaceType( s_opposing_j_rot270   );
            auto const r_in_s_out_j_mapped  = MapFaceNameToFaceType( r_in_s_out_j  ); auto const r_in_s_out_j_rot90_mapped  = MapFaceNameToFaceType( r_in_s_out_j_rot90  ); auto const r_in_s_out_j_rot180_mapped  = MapFaceNameToFaceType( r_in_s_out_j_rot180  ); auto const r_in_s_out_j_rot270_mapped   = MapFaceNameToFaceType( r_in_s_out_j_rot270   ); auto const r_in_s_out_j_sym_mapped  = MapFaceNameToFaceType( r_in_s_out_j_sym  ); auto const r_in_s_out_j_sym_rot90_mapped  = MapFaceNameToFaceType( r_in_s_out_j_sym_rot90  ); auto const r_in_s_out_j_sym_rot180_mapped  = MapFaceNameToFaceType( r_in_s_out_j_sym_rot180  ); auto const r_in_s_out_j_sym_rot270_mapped  = MapFaceNameToFaceType( r_in_s_out_j_sym_rot270  );
            auto const r_out_s_in_j_mapped  = MapFaceNameToFaceType( r_out_s_in_j  ); auto const r_out_s_in_j_rot90_mapped  = MapFaceNameToFaceType( r_out_s_in_j_rot90  ); auto const r_out_s_in_j_rot180_mapped  = MapFaceNameToFaceType( r_out_s_in_j_rot180  ); auto const r_out_s_in_j_rot270_mapped   = MapFaceNameToFaceType( r_out_s_in_j_rot270   ); auto const r_out_s_in_j_sym_mapped  = MapFaceNameToFaceType( r_out_s_in_j_sym  ); auto const r_out_s_in_j_sym_rot90_mapped  = MapFaceNameToFaceType( r_out_s_in_j_sym_rot90  ); auto const r_out_s_in_j_sym_rot180_mapped  = MapFaceNameToFaceType( r_out_s_in_j_sym_rot180  ); auto const r_out_s_in_j_sym_rot270_mapped  = MapFaceNameToFaceType( r_out_s_in_j_sym_rot270  );
            auto const rs_opposing_j_mapped = MapFaceNameToFaceType( rs_opposing_j ); auto const rs_opposing_j_rot90_mapped = MapFaceNameToFaceType( rs_opposing_j_rot90 ); auto const rs_opposing_j_rot180_mapped = MapFaceNameToFaceType( rs_opposing_j_rot180 ); auto const rs_opposing_j_rot270_mapped  = MapFaceNameToFaceType( rs_opposing_j_rot270  ); auto const rs_opposing_j_sym_mapped = MapFaceNameToFaceType( rs_opposing_j_sym ); auto const rs_opposing_j_sym_rot90_mapped = MapFaceNameToFaceType( rs_opposing_j_sym_rot90 ); auto const rs_opposing_j_sym_rot180_mapped = MapFaceNameToFaceType( rs_opposing_j_sym_rot180 ); auto const rs_opposing_j_sym_rot270_mapped = MapFaceNameToFaceType( rs_opposing_j_sym_rot270 );
            THEN( "The map fits the expected value" ) {
                auto const expected_right_r       = FaceType::AllRightR;
                auto const expected_leftright_r   = FaceType::LeftrightR;
                auto const expected_leftright_rs  = FaceType::LeftrightRS;
                auto const expected_left_s        = FaceType::AllLeftS;
                auto const expected_leftright_s   = FaceType::LeftrightS;
                auto const expected_all_j         = FaceType::AllJ;
                auto const expected_r_inflow_j    = FaceType::InflowRJ;
                auto const expected_r_outflow_j   = FaceType::OutflowRJ;
                auto const expected_r_opposing_j  = FaceType::OpposingRJ;
                auto const expected_s_outflow_j   = FaceType::OutflowSJ;
                auto const expected_s_inflow_j    = FaceType::InflowSJ;
                auto const expected_s_opposing_j  = FaceType::OpposingSJ;
                auto const expected_r_in_s_out_j  = FaceType::InROutSJ;
                auto const expected_r_out_s_in_j  = FaceType::OutRInSJ;
                auto const expected_rs_opposing_j = FaceType::OpposingRSJ;
                REQUIRE( right_r_mapped       == expected_right_r      ); REQUIRE( right_r_rot90_mapped       == expected_right_r      );  REQUIRE( right_r_rot180_mapped       == expected_right_r      ); REQUIRE( right_r_rot270_mapped       == expected_right_r );
                REQUIRE( leftright_r_mapped   == expected_leftright_r  ); REQUIRE( leftright_r_rot_mapped     == expected_leftright_r  );
                REQUIRE( leftright_rs_mapped  == expected_leftright_rs ); REQUIRE( leftright_rs_rot_mapped    == expected_leftright_rs ); REQUIRE( leftright_rs_sym_mapped     == expected_leftright_rs ); REQUIRE( leftright_rs_sym_rot_mapped  == expected_leftright_rs );
                REQUIRE( left_s_mapped        == expected_left_s       ); REQUIRE( left_s_rot90_mapped        == expected_left_s       ); REQUIRE( left_s_rot180_mapped        == expected_left_s       ); REQUIRE( left_s_rot270_mapped         == expected_left_s       );
                REQUIRE( leftright_s_mapped   == expected_leftright_s  ); REQUIRE( leftright_s_rot_mapped     == expected_leftright_s  );
                REQUIRE( all_j_mapped         == expected_all_j        );
                REQUIRE( r_inflow_j_mapped    == expected_r_inflow_j   ); REQUIRE( r_inflow_j_rot90_mapped    == expected_r_inflow_j   ); REQUIRE( r_inflow_j_rot180_mapped    == expected_r_inflow_j   ); REQUIRE( r_inflow_j_rot270_mapped     == expected_r_inflow_j   );
                REQUIRE( r_outflow_j_mapped   == expected_r_outflow_j  ); REQUIRE( r_outflow_j_rot90_mapped   == expected_r_outflow_j  ); REQUIRE( r_outflow_j_rot180_mapped   == expected_r_outflow_j  ); REQUIRE( r_outflow_j_rot270_mapped    == expected_r_outflow_j  );
                REQUIRE( r_opposing_j_mapped  == expected_r_opposing_j ); REQUIRE( r_opposing_j_rot90_mapped  == expected_r_opposing_j ); REQUIRE( r_opposing_j_rot180_mapped  == expected_r_opposing_j ); REQUIRE( r_opposing_j_rot270_mapped   == expected_r_opposing_j );
                REQUIRE( s_outflow_j_mapped   == expected_s_outflow_j  ); REQUIRE( s_outflow_j_rot90_mapped   == expected_s_outflow_j  ); REQUIRE( s_outflow_j_rot180_mapped   == expected_s_outflow_j  ); REQUIRE( s_outflow_j_rot270_mapped    == expected_s_outflow_j  );
                REQUIRE( s_inflow_j_mapped    == expected_s_inflow_j   ); REQUIRE( s_inflow_j_rot90_mapped    == expected_s_inflow_j   ); REQUIRE( s_inflow_j_rot180_mapped    == expected_s_inflow_j   ); REQUIRE( s_inflow_j_rot270_mapped     == expected_s_inflow_j   );
                REQUIRE( s_opposing_j_mapped  == expected_s_opposing_j ); REQUIRE( s_opposing_j_rot90_mapped  == expected_s_opposing_j ); REQUIRE( s_opposing_j_rot180_mapped  == expected_s_opposing_j ); REQUIRE( s_opposing_j_rot270_mapped   == expected_s_opposing_j );
                REQUIRE( r_in_s_out_j_mapped  == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_rot90_mapped  == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_rot180_mapped  == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_rot270_mapped   == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_sym_mapped  == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_sym_rot90_mapped  == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_sym_rot180_mapped  == expected_r_in_s_out_j ); REQUIRE( r_in_s_out_j_sym_rot270_mapped  == expected_r_in_s_out_j );
                REQUIRE( r_out_s_in_j_mapped  == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_rot90_mapped  == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_rot180_mapped  == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_rot270_mapped   == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_sym_mapped  == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_sym_rot90_mapped  == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_sym_rot180_mapped  == expected_r_out_s_in_j ); REQUIRE( r_out_s_in_j_sym_rot270_mapped  == expected_r_out_s_in_j );
                REQUIRE( rs_opposing_j_mapped == expected_rs_opposing_j); REQUIRE( rs_opposing_j_rot90_mapped == expected_rs_opposing_j); REQUIRE( rs_opposing_j_rot180_mapped == expected_rs_opposing_j); REQUIRE( rs_opposing_j_rot270_mapped  == expected_rs_opposing_j); REQUIRE( rs_opposing_j_sym_mapped == expected_rs_opposing_j); REQUIRE( rs_opposing_j_sym_rot90_mapped == expected_rs_opposing_j); REQUIRE( rs_opposing_j_sym_rot180_mapped == expected_rs_opposing_j); REQUIRE( rs_opposing_j_sym_rot270_mapped == expected_rs_opposing_j);
            }
        }
    }
}