/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include "facename_facetype_map.h"

#include <stdexcept>

FaceType MapFaceNameToFaceType( ValidFaceName const fn ) {
   switch( fn ) {
      case ValidFaceName::RrRrRrRr :
      case ValidFaceName::RlRrRlRr :
      case ValidFaceName::RlRlRlRl :
      case ValidFaceName::RrRlRrRl :
         return FaceType::AllRightR;

      case ValidFaceName::RrRlRlRr:
      case ValidFaceName::RlRrRrRl:
         return FaceType::LeftrightR;

      case ValidFaceName::RrSrRlSl:
      case ValidFaceName::SrRrSlSl:
      case ValidFaceName::RlSlRrSr:
      case ValidFaceName::SlRlSrRr:
         return FaceType::LeftrightRS;


      case ValidFaceName::SlSlSlSl:
      case ValidFaceName::SrSlSrSl:
      case ValidFaceName::SrSrSrSr:
      case ValidFaceName::SlSrSlSr:
         return FaceType::AllLeftS;

      case ValidFaceName::SlSrSrSl:
      case ValidFaceName::SrSlSlSr:
         return FaceType::LeftrightS;

      case ValidFaceName::J_J_J_J_:
         return FaceType::AllJ;

      case ValidFaceName::RrJ_J_Rr:
      case ValidFaceName::RlRrJ_J_:
      case ValidFaceName::J_RlRlJ_:
      case ValidFaceName::J_J_RrRl:
         return FaceType::InflowRJ;

      case ValidFaceName::RlJ_J_Rl:
      case ValidFaceName::RrRlJ_J_:
      case ValidFaceName::J_RrRrJ_:
      case ValidFaceName::J_J_RlRr:
         return FaceType::OutflowRJ;

      case ValidFaceName::J_RrJ_Rr:
      case ValidFaceName::RlJ_RlJ_:
      case ValidFaceName::J_RlJ_Rl:
      case ValidFaceName::RrJ_RrJ_:
         return FaceType::OpposingRJ;

      case ValidFaceName::SlJ_J_Sl:
      case ValidFaceName::SrSlJ_J_:
      case ValidFaceName::J_SrSrJ_:
      case ValidFaceName::J_J_SlSr:
         return FaceType::OutflowSJ;

      case ValidFaceName::SrJ_J_Sr:
      case ValidFaceName::SlSrJ_J_:
      case ValidFaceName::J_SlSlJ_:
      case ValidFaceName::J_J_SrSl:
         return FaceType::InflowSJ;

      case ValidFaceName::J_SlJ_Sl:
      case ValidFaceName::SrJ_SrJ_:
      case ValidFaceName::J_SrJ_Sr:
      case ValidFaceName::SlJ_SlJ_:
         return FaceType::OpposingSJ;

      case ValidFaceName::RrJ_J_Sl:
      case ValidFaceName::SrRrJ_J_:
      case ValidFaceName::J_SrRlJ_:
      case ValidFaceName::J_J_SlRl:
      case ValidFaceName::SlJ_J_Rr:
      case ValidFaceName::RlSlJ_J_:
      case ValidFaceName::J_RlSrJ_:
      case ValidFaceName::J_J_RrSr:
         return FaceType::InROutSJ;

      case ValidFaceName::RlJ_J_Sr:
      case ValidFaceName::SlRlJ_J_:
      case ValidFaceName::J_SlRrJ_:
      case ValidFaceName::J_J_SrRr:
      case ValidFaceName::SrJ_J_Rl:
      case ValidFaceName::RrSrJ_J_:
      case ValidFaceName::J_RrSlJ_:
      case ValidFaceName::J_J_RlSl:
         return FaceType::OutRInSJ;

      case ValidFaceName::J_SlJ_Rr:
      case ValidFaceName::RlJ_SrJ_:
      case ValidFaceName::J_RlJ_Sr:
      case ValidFaceName::SlJ_RrJ_:
      case ValidFaceName::J_SrJ_Rl:
      case ValidFaceName::RrJ_SlJ_:
      case ValidFaceName::J_RrJ_Sl:
      case ValidFaceName::SrJ_RlJ_:
         return FaceType::OpposingRSJ;

      default:
         throw std::invalid_argument( "Unkown face name cannot be mapped" );
   }
}