/*****************************************************************************************
*                                                                                        *
* This project is maintained by the 'Nanoshock group' at the Chair of Aerodynamics and   *
* Fluid Mechanics, Technical University of Munich.                                       *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* LICENSE                                                                                *
*                                                                                        *
* Copyright (C) 2021 Nikolaus A. Adams and contributors                                  *
*                                                                                        *
* This program is free software: you can redistribute it and/or modify it under          *
* the terms of the GNU General Public License as published by the Free Software          *
* Foundation version 3.                                                                  *
*                                                                                        *
* This program is distributed in the hope that it will be useful, but WITHOUT ANY        *
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A        *
* PARTICULAR PURPOSE. See the GNU General Public License for more details.               *
*                                                                                        *
* You should have received a copy of the GNU General Public License along with           *
* this program (gpl-3.0.txt). If not, see <https://www.gnu.org/licenses/gpl-3.0.html>    *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* THIRD-PARTY tool                                                                       *
*                                                                                        *
* Please note, this project uses Catch2 as git submodule (directing to its own           *
* repository). See its own respective (open-source) license agreement in the Catch2/     *
* folder.                                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* CONTACT                                                                                *
*                                                                                        *
* nanoshock@aer.mw.tum.de                                                                *
*                                                                                        *
******************************************************************************************
*                                                                                        *
* Munich, September 15th, 2020                                                           *
*                                                                                        *
*****************************************************************************************/
#include <catch.hpp>
#include "facecount_groupname_map.h"

#include "relation_facecount_map.h"
#include "cube_generator.h"

#include "valid_combinations.h"

SCENARIO( "Face counts are correctly mapped to their respective name (type)" ) {
   GIVEN( "Some valid face counts" ) {
      std::vector<FaceType> group1 = MapRelationToFaceCount( GenerateValidCube( ValidCubeName::SixRightR ) );
      std::vector<FaceType> group2 = MapRelationToFaceCount( GenerateValidCube( ValidCubeName::SixLeftR ) );
      std::vector<FaceType> group3 = MapRelationToFaceCount( GenerateValidCube( ValidCubeName::ThreeLeftSThreeLeftRightS ) );
      std::vector<FaceType> group4 = MapRelationToFaceCount( GenerateValidCube( ValidCubeName::FourRightRTwoLeftRightR ) );
      WHEN( "They are mapped to their name" ) {
         auto const name1 = MapFaceCountToGroupName( group1 );
         auto const name2 = MapFaceCountToGroupName( group2 );
         auto const name3 = MapFaceCountToGroupName( group3 );
         auto const name4 = MapFaceCountToGroupName( group4 );
         THEN( "The result is as expected" ) {
            auto const expected1 = GroupName::SixAllRightR;
            auto const expected2 = GroupName::SixAllRightR;
            auto const expected3 = GroupName::ThreeLeftrightSThreeAllLeftS;
            auto const expected4 = GroupName::TwoLeftrightRFourAllRightR;
            REQUIRE( name1 == expected1 );
            REQUIRE( name2 == expected2 );
            REQUIRE( name3 == expected3 );
            REQUIRE( name4 == expected4 );
         }
      }
   }
   GIVEN( "An invalid group" ) {
      std::vector<FaceType> invalid_group = { FaceType::AllRightR, FaceType::AllRightR, FaceType::AllRightR,
                                              FaceType::AllRightR, FaceType::AllRightR, FaceType::AllJ };
      WHEN( "They are mapped to their name" ) {
         THEN( "An exception is thrown" ) {
            REQUIRE_THROWS( MapFaceCountToGroupName( invalid_group ) );
         }
      }
   }
}

SCENARIO( "Face counts map all valid relations without errors", "[.long]" ) {
   GIVEN( "All valid combinations" ) {
      auto const valids = std::get<2>( StoredValidCombinations() );
      WHEN( "The relation is (indirectly) mapped to a group name" ) {
         THEN( "No exception occurs" ) {
            for( auto const relation : valids ) {
               auto const face_count = MapRelationToFaceCount( relation );
               REQUIRE_NOTHROW( MapFaceCountToGroupName( face_count ) );
            }
         }
      }
   }
}
