# Combinatorics

A small stand-alone C++ software to determine the elementary wave patterns in our setup of the three-dimensional Riemann problem. For details on our setup see the [landing page](/README.md) of this git repository.

## Compilation

Compilation is done with CMake Version 3.10 or higher. With CMake and a C++17 conforming compiler execute
```
mkdir build && cd build
cmake ..
cmake --build . --target combinator
```
to build the executable `combinator`. Similarly, the unit test can be compiled running `cmake --build . --target testsuite`

## Execution

The combinator runs a brute force analysis of all combinatorially possible configurations. For each combination it outputs a short summary. The resulting output should look like this:
```
./Combinator 
Testing: R_l R_l R_l R_l | R_l R_l R_l R_l | R_l R_l R_l R_l
   West  -   R_l R_l R_l R_l:     Valid
   East  -   R_l R_l R_l R_l:     Valid
   South -   R_l R_l R_l R_l:     Valid
   North -   R_l R_l R_l R_l:     Valid
   Bottom-   R_l R_l R_l R_l:     Valid
   Top   -   R_l R_l R_l R_l:     Valid
   ------------------------------------
                                  VALID

Testing: R_l R_l R_l R_l | R_l R_l R_l R_l | R_l R_l R_l R_r
   West  -   R_l R_l R_l R_l:     Valid
   East  -   R_r R_l R_l R_l:   Invalid
   South -   R_l R_l R_l R_l:     Valid
   North -   R_l R_l R_l R_l:     Valid
   Bottom-   R_l R_l R_l R_l:     Valid
   Top   -   R_l R_l R_l R_r:   Invalid
   ------------------------------------
                                INVALID
...
```
Similarly, the unit test may be executed by running `./testsuite`.

### Interpreting the output

Although we hope the output is self-explanatory, we state some more details here. The first line shows the wave pattern identifier under test. The capital letters `R, S, J` indicate a rarefaction fan, compression shock and degenerate wave, respectively. The small letters `l,r` indicate if the wave is left or right running, respectively. The order of the waves is according to the identifiers in the publication, cf. the [landing page](/README.md). Following this definition, the following six lines indicate how the identifier relates to the side of the cube. The information whether, the four waves on a side make up a valid combination according to [Schulz-Rinne](doi.org/10.1137/0524006) is printed at the end of each respective line. Below dashed line is the summary if the 3D wave pattern is valid. Naturally, all six sides need to be valid to obtain a valid three-dimensional configuration.

