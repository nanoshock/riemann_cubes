import paraview.simple as pv
import argparse as ag
import numpy as np
import glob
import os
import csv
from copy import deepcopy

# This script has to be run in a simulation folder, so that the relative
# path to the timestep data is 'domain/data*.xdmf'.

# This function returns the last 13 characters of a string.


def get_last_13_chars(x):
    return x[-13:]

# This function removes the last 5 characters of a string.


def remove_last_5_chars( x ) :
    return x[:-5]

# This function returns a list with all timestep files.


def get_files():
    files = glob.glob('domain/data*.xdmf')
    files = sorted(files, key=get_last_13_chars)
    print("Found " + str(np.size(files)) + " timestep files.")
    return files

# This function returns a list with all timesteps.


def extract_timestep_list(files, steps):
    iterator_list = np.linspace(0, steps - 1, steps)

    for k in iterator_list:
        time = files[int(k)]
        time = get_last_13_chars( time )
        time = remove_last_5_chars( time )
        files[int(k)] = float(time)

    return files

# This function creates a file with all timesteps


def extract_timesteps( files, steps ) :
    iterator_list = np.linspace(0, steps-1, steps)
    timesteps = np.zeros(( steps, 2 ))

    for k in iterator_list :
        time = files[ int(k) ]
        time = get_last_13_chars( time )
        time = remove_last_5_chars( time )

        timesteps[ int(k), int(0) ] = int( iterator_list[ int(k) ] + 1 )
        timesteps[ int(k), int(1) ] = float(time)

    with open( 'list_of_timesteps.csv', 'w', newline='') as csv_file :
        writer = csv.writer( csv_file )
        writer.writerows( timesteps )

    return 0

# This function opens a timestep file.


def open_data(filename):
    data = pv.XDMFReader(FileNames=[filename])
    data.CellArrayStatus = ['density', 'pressure']
    return data

# This function returns the pressure range.


def get_pressure_range(data_1, data_2):
    cell_data_1 = open_data(data_1)
    cell_data_2 = open_data(data_2)

    min_max_1 = cell_data_1.CellData.GetArray('pressure').GetRange()
    min_max_2 = cell_data_2.CellData.GetArray('pressure').GetRange()

    min_max = [min(min_max_1[0], min_max_2[0]),
               max(min_max_1[1], min_max_2[1])]

    pv.Delete(cell_data_1)
    pv.Delete(cell_data_2)
    del cell_data_1
    del cell_data_2

    return min_max

# This function returns the density range.


def get_density_range(data_1, data_2):
    cell_data_1 = open_data(data_1)
    cell_data_2 = open_data(data_2)

    min_max_1 = cell_data_1.CellData.GetArray('density').GetRange()
    min_max_2 = cell_data_2.CellData.GetArray('density').GetRange()

    min_max = [min(min_max_1[0], min_max_2[0]),
               max(min_max_1[1], min_max_2[1])]

    pv.Delete(cell_data_1)
    pv.Delete(cell_data_2)
    del cell_data_1
    del cell_data_2

    return min_max

# This function writes a file containing the density range


def write_density_and_pressure_range(density_range, pressure_range):
    with open( 'density_and_pressure_range.csv', 'w', newline='') as csv_file :
        writer = csv.writer( csv_file )
        writer.writerow(["density range", density_range])
        writer.writerow(["pressure range", pressure_range])

    return 0

# This function generates density contours.


def generate_density_contours(input_object, density_range):

    contour = pv.Contour(Input=input_object)
    contour.ContourBy = ['POINTS', 'density']

    print("Density range for contours = " + str(density_range))

    contour.Isosurfaces = np.linspace(
        start=density_range[0], stop=density_range[1], num=25)

    return contour

# This function returns the position of the camera in each timestep, in
# order to orbit the cube.


def get_orbit_position(timestep_counter, timestep_list, number_of_timesteps):
    current_Time = timestep_list[timestep_counter - 1]

    time_of_position = np.linspace(
        timestep_list[0], timestep_list[number_of_timesteps - 1], 10)

    pos_1 = np.array([3.20, 3.20, 3.2])
    pos_2 = np.array([0.87, 4.62, 2.6])
    pos_3 = np.array([-1.84, 4.21, 2.0])
    pos_4 = np.array([-3.65, 2.16, 1.4])
    pos_5 = np.array([-3.71, -0.58, 0.8])
    pos_6 = np.array([-2.00, -2.71, 0.2])
    pos_7 = np.array([0.68, -3.25, -0.4])
    pos_8 = np.array([3.08, -1.94, -1.0])
    pos_9 = np.array([4.08, 0.61, -1.6])
    pos_10 = np.array([3.20, 3.20, -2.2])
    pos_matrix = np.array(
        [pos_1, pos_2, pos_3, pos_4, pos_5, pos_6, pos_7, pos_8, pos_9, pos_10])

    counter = 0

    while True:
        counter += 1
        if current_Time >= time_of_position[counter -
                                            1] and current_Time <= time_of_position[counter]:
            break

    delta_t = (current_Time - time_of_position[counter - 1]) / (
        time_of_position[counter] - time_of_position[counter - 1])

    pos_x = pos_matrix[counter - 1, 0] + delta_t * \
        (pos_matrix[counter, 0] - pos_matrix[counter - 1, 0])
    pos_y = pos_matrix[counter - 1, 1] + delta_t * \
        (pos_matrix[counter, 1] - pos_matrix[counter - 1, 1])
    pos_z = pos_matrix[counter - 1, 2] + delta_t * \
        (pos_matrix[counter, 2] - pos_matrix[counter - 1, 2])

    camera_position = [pos_x, pos_y, pos_z]

    return camera_position

# This function loops over all timesteps and calls the PNG creating
# function for every timestep.


def create_pngs_loop_over_time():

    timestep_files = get_files()
    timestep_counter = 0

    number_of_timesteps = np.size(timestep_files)
    timestep_list = deepcopy(timestep_files)
    timestep_list = extract_timestep_list(timestep_list, number_of_timesteps)

    extract_timesteps_files = deepcopy(timestep_files)
    extract_timesteps( extract_timesteps_files, number_of_timesteps )

    # This is the relative path to the directory, where the created pngs will
    # be stored.
    image_path = 'Images'

    try:
        os.mkdir(image_path)
    except BaseException:
        print(
            "Creation of directory %s failed. Maybe this directory already exists." %
            image_path)
    else:
        print("Successfully created the directory %s." % image_path)

    pressure_range = get_pressure_range(
        timestep_files[0], timestep_files[np.size(timestep_files) - 1])
    density_range = get_density_range(
        timestep_files[0], timestep_files[np.size(timestep_files) - 1])

    write_density_and_pressure_range(density_range, pressure_range)

    for current_file in timestep_files:
        timestep_counter += 1
        print("Start creating images for timestep " + str(timestep_counter) +
              ". Time = " + str(timestep_list[timestep_counter - 1]) + " s")
        create_pngs_of_single_timestep(
            timestep_counter,
            current_file,
            number_of_timesteps,
            timestep_list,
            pressure_range,
            density_range,
            image_path)


# This function creates the pngs of a single timestep.
def create_pngs_of_single_timestep(
        timestep_counter, current_file, number_of_timesteps, timestep_list, pressure_range, density_range, image_path):

    # Set the Opacity.
    opacity = 1

    # Set the line width.
    line_width = 2.0
    line_width_2 = 1.0

    # Import a file with color scales. Depending on the directory of the file,
    # the path must be adapted.
    pv.ImportPresets(filename='../own_scale.xml')

    # Choose a color scale.
    color_scale = 'blueorange'
    contour_color_white = [1.0, 1.0, 1.0]
    contour_color_black = [0.0, 0.0, 0.0]

    cell_data = open_data(current_file)

    point_data = pv.CellDatatoPointData(Input=cell_data)
    point_data.CellDataArraytoprocess = ['density', 'pressure']

    contour_data = generate_density_contours(point_data, density_range)

    pv.Delete(point_data)
    del point_data

    # === Diagonal Images ===

    view_list = np.linspace(1, 8, 8)

    # Loop over all diagonal viewing angles.
    for k in view_list:
        number_of_view = view_list[int(k) - 1]

        if number_of_view == 1:
            view_vector = [1.0, 1.0, 1.0]
            position_of_camera = [3.2, 3.2, 3.2]
            view_name_tag = '+1_+1_+1'
        elif number_of_view == 2:
            view_vector = [1.0, -1.0, 1.0]
            position_of_camera = [3.2, -2.2, 3.2]
            view_name_tag = '+1_-1_+1'
        elif number_of_view == 3:
            view_vector = [-1.0, 1.0, 1.0]
            position_of_camera = [-2.2, 3.2, 3.2]
            view_name_tag = '-1_+1_+1'
        elif number_of_view == 4:
            view_vector = [-1.0, -1.0, 1.0]
            position_of_camera = [-2.2, -2.2, 3.2]
            view_name_tag = '-1_-1_+1'
        elif number_of_view == 5:
            view_vector = [1.0, 1.0, -1.0]
            position_of_camera = [3.2, 3.2, -2.2]
            view_name_tag = '+1_+1_-1'
        elif number_of_view == 6:
            view_vector = [1.0, -1.0, -1.0]
            position_of_camera = [3.2, -2.2, -2.2]
            view_name_tag = '+1_-1_-1'
        elif number_of_view == 7:
            view_vector = [-1.0, 1.0, -1.0]
            position_of_camera = [-2.2, 3.2, -2.2]
            view_name_tag = '-1_+1_-1'
        else:
            view_vector = [-1.0, -1.0, -1.0]
            position_of_camera = [-2.2, -2.2, -2.2]
            view_name_tag = '-1_-1_-1'

        ##--- Create Slices ---##
        # Create slices for view two.
        slice_view_two = pv.Slice(Input=contour_data)
        slice_view_two.SliceType = 'Plane'
        slice_view_two.SliceOffsetValues = [0.0]
        slice_view_two.SliceType.Origin = [0.5, 0.5, 0.5]
        slice_view_two.SliceType.Normal = view_vector

        cell_data_slice_view_two = pv.Slice(Input=cell_data)
        cell_data_slice_view_two.SliceType = 'Plane'
        cell_data_slice_view_two.SliceOffsetValues = [0.0]
        cell_data_slice_view_two.SliceType.Origin = [0.5, 0.5, 0.5]
        cell_data_slice_view_two.SliceType.Normal = view_vector


        ##--- Create and split layout. ---##
        layout_2 = pv.CreateLayout('Diagonal Images')
        pv.SetActiveView(None)

        layout_2.SplitHorizontal(0, 0.5)
        pv.SetActiveView(None)

        ##--- Create and setup views. ---##
        # Create first view.
        view_one = pv.CreateView('RenderView')
        pv.AssignViewToLayout(view=view_one, layout=layout_2, hint=1)
        cont_one_display = pv.Show(contour_data, view_one)
        pv.ColorBy(cont_one_display, ('POINTS', 'pressure'))
        cont_one_display.DataAxesGrid.GridAxesVisibility = 1
        cont_one_display.DataAxesGrid.XTitle = '     X Axis     '
        cont_one_display.DataAxesGrid.YTitle = '     Y Axis     '
        cont_one_display.DataAxesGrid.ZTitle = '     Z Axis     '
        cont_one_display.DataAxesGrid.XTitleFontSize = 14
        cont_one_display.DataAxesGrid.YTitleFontSize = 14
        cont_one_display.DataAxesGrid.ZTitleFontSize = 14
        cont_one_display.DataAxesGrid.XLabelFontSize = 14
        cont_one_display.DataAxesGrid.YLabelFontSize = 14
        cont_one_display.DataAxesGrid.ZLabelFontSize = 14
        cont_one_display.DataAxesGrid.XAxisNotation = 'Fixed'
        cont_one_display.DataAxesGrid.XAxisUseCustomLabels = 1
        cont_one_display.DataAxesGrid.XAxisLabels = [0.25, 0.5, 0.75]
        cont_one_display.DataAxesGrid.YAxisNotation = 'Fixed'
        cont_one_display.DataAxesGrid.YAxisUseCustomLabels = 1
        cont_one_display.DataAxesGrid.YAxisLabels = [0.25, 0.5, 0.75]
        cont_one_display.DataAxesGrid.ZAxisNotation = 'Fixed'
        cont_one_display.DataAxesGrid.ZAxisUseCustomLabels = 1
        cont_one_display.DataAxesGrid.ZAxisLabels = [0.25, 0.5, 0.75]
        cont_one_display.SetScalarBarVisibility(view_one, True)
        pressure_LUT = pv.GetColorTransferFunction('pressure')
        pressure_LUT.ApplyPreset(color_scale, True)
        pressure_LUT.RescaleTransferFunction(
            pressure_range[0], pressure_range[1])
        pressure_LUT_color_bar_one = pv.GetScalarBar(pressure_LUT, view_one)
        pressure_LUT_color_bar_one.TitleFontSize = 5
        pressure_LUT_color_bar_one.LabelFontSize = 5
        pressure_LUT_color_bar_one.ScalarBarThickness = 5
        pressure_LUT_color_bar_one.ScalarBarLength = 0.15
        cont_one_display.Opacity = opacity

        pv.SetActiveView(None)

        # Create second view.
        view_two = pv.CreateView('RenderView')
        pv.AssignViewToLayout(view=view_two, layout=layout_2, hint=1)

        slice_view_two_display = pv.Show(slice_view_two, view_two)
        pv.ColorBy(slice_view_two_display, ('POINTS', 'pressure'))
        slice_view_two_display.SetScalarBarVisibility(view_two, True)
        slice_view_two_display.SetRepresentationType('Wireframe')
        slice_view_two_display.LineWidth = line_width

        pressure_LUT.RescaleTransferFunction(
            pressure_range[0], pressure_range[1])
        pressure_LUT_color_bar_two = pv.GetScalarBar(pressure_LUT, view_two)
        pressure_LUT_color_bar_two.TitleFontSize = 5
        pressure_LUT_color_bar_two.LabelFontSize = 5
        pressure_LUT_color_bar_two.ScalarBarThickness = 5
        pressure_LUT_color_bar_two.ScalarBarLength = 0.15
        pressure_LUT_color_bar_two.Title = 'pressure'
        pressure_LUT_color_bar_two.ComponentTitle = ''

        cell_data_two_display = pv.Show(cell_data_slice_view_two, view_two)
        cell_data_two_display.SetScalarBarVisibility(view_two, False)
        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 1
        cell_data_two_display.DataAxesGrid.XTitle = '     X Axis     '
        cell_data_two_display.DataAxesGrid.YTitle = '     Y Axis     '
        cell_data_two_display.DataAxesGrid.ZTitle = '     Z Axis     '
        cell_data_two_display.DataAxesGrid.XTitleFontSize = 14
        cell_data_two_display.DataAxesGrid.YTitleFontSize = 14
        cell_data_two_display.DataAxesGrid.ZTitleFontSize = 14
        cell_data_two_display.DataAxesGrid.XLabelFontSize = 14
        cell_data_two_display.DataAxesGrid.YLabelFontSize = 14
        cell_data_two_display.DataAxesGrid.ZLabelFontSize = 14
        cell_data_two_display.DataAxesGrid.XAxisNotation = 'Fixed'
        cell_data_two_display.DataAxesGrid.XAxisUseCustomLabels = 1
        cell_data_two_display.DataAxesGrid.XAxisLabels = [0.25, 0.5, 0.75]
        cell_data_two_display.DataAxesGrid.YAxisNotation = 'Fixed'
        cell_data_two_display.DataAxesGrid.YAxisUseCustomLabels = 1
        cell_data_two_display.DataAxesGrid.YAxisLabels = [0.25, 0.5, 0.75]
        cell_data_two_display.DataAxesGrid.ZAxisNotation = 'Fixed'
        cell_data_two_display.DataAxesGrid.ZAxisUseCustomLabels = 1
        cell_data_two_display.DataAxesGrid.ZAxisLabels = [0.25, 0.5, 0.75]

        cell_data_two_display.Opacity = 0

        pv.SetActiveView(None)

        # Set camera position for view_two
        view_two.CameraPosition = position_of_camera
        view_two.CameraFocalPoint = [0.5, 0.5, 0.5]
        view_two.CameraViewUp = [0.0, 0.0, 1.0]
        view_two.CameraParallelScale = 0.8660254037844386

        # Set camera position for view_one
        view_one.CameraPosition = position_of_camera
        view_one.CameraFocalPoint = [0.5, 0.5, 0.5]
        view_one.CameraViewUp = [0.0, 0.0, 1.0]
        view_one.CameraParallelScale = 0.8660254037844386

        print("Creating images for view " + str(view_name_tag) + '.')

        if timestep_counter < 10:
            timestep_tag = "00" + str(timestep_counter)
        elif timestep_counter >= 10 and timestep_counter < 100:
            timestep_tag = "0" + str(timestep_counter)
        else:
            timestep_tag = str(timestep_counter)

        image_name = image_path + '/cube_slice_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, layout_2, SaveAllViews=1,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created cube and slice")

        pv.SetActiveView(view_one)

        image_name = image_path + '/cube_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_one, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created cube")

        pv.SetActiveView(view_two)

        image_name = image_path + '/slice_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created slice")

        # Create pure contour images
        view_two.OrientationAxesVisibility = 0
        slice_view_two_display.SetScalarBarVisibility(view_two, False)
        cell_data_two_display.DataAxesGrid.XTitleColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.YTitleColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.GridColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.XLabelColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.YLabelColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.ZLabelColor = [0.0, 0.0, 0.0]

        image_name = image_path + '/colored_slice_with_box_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created colored slice with box")

        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 0

        image_name = image_path + '/colored_slice_without_box_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created colored slice without box")

        pv.ColorBy(slice_view_two_display, None)
        slice_view_two_display.AmbientColor = contour_color_black
        slice_view_two_display.DiffuseColor = contour_color_black

        image_name = image_path + '/black_slice_without_box_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created black slice without box")

        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 1

        image_name = image_path + '/black_slice_with_box_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created black slice with box")

        # Create Density field images
        view_two.OrientationAxesVisibility = 1
        view_two.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
        cell_data_two_display.Opacity = 1
        pv.ColorBy(cell_data_two_display, ('CELLS', 'density'))
        pv.ColorBy(slice_view_two_display, None)
        slice_view_two_display.SetRepresentationType('Wireframe')
        slice_view_two_display.LineWidth = line_width_2
        pv.HideScalarBarIfNotNeeded(pressure_LUT, view_two)
        slice_view_two_display.AmbientColor = contour_color_white
        slice_view_two_display.DiffuseColor = contour_color_white
        density_LUT = pv.GetColorTransferFunction('density')
        density_LUT.ApplyPreset(color_scale, True)
        density_LUT.RescaleTransferFunction(density_range[0], density_range[1])
        density_LUT_color_bar_two = pv.GetScalarBar(density_LUT, view_two)
        density_LUT_color_bar_two.TitleFontSize = 5
        density_LUT_color_bar_two.LabelFontSize = 5
        density_LUT_color_bar_two.ScalarBarThickness = 5
        density_LUT_color_bar_two.ScalarBarLength = 0.15
        density_LUT_color_bar_two.Title = 'density'
        density_LUT_color_bar_two.ComponentTitle = ''
        density_LUT_color_bar_two.TitleColor = [0.0, 0.0, 0.0]
        density_LUT_color_bar_two.LabelColor = [0.0, 0.0, 0.0]

        image_name = image_path + '/density_field_with_box_transparent_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created density field with box with transparent background")

        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 0

        image_name = image_path + '/density_field_without_box_transparent_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created density field without box with transparent background")

        cell_data_two_display.DataAxesGrid.XTitleColor = [1.0, 1.0, 1.0]
        cell_data_two_display.DataAxesGrid.YTitleColor = [1.0, 1.0, 1.0]
        cell_data_two_display.DataAxesGrid.ZTitleColor = [1.0, 1.0, 1.0]
        cell_data_two_display.DataAxesGrid.GridColor = [1.0, 1.0, 1.0]
        cell_data_two_display.DataAxesGrid.XLabelColor = [1.0, 1.0, 1.0]
        cell_data_two_display.DataAxesGrid.YLabelColor = [1.0, 1.0, 1.0]
        cell_data_two_display.DataAxesGrid.ZLabelColor = [1.0, 1.0, 1.0]
        density_LUT_color_bar_two.TitleColor = [1.0, 1.0, 1.0]
        density_LUT_color_bar_two.LabelColor = [1.0, 1.0, 1.0]
        view_two.OrientationAxesLabelColor = [1.0, 1.0, 1.0]

        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 1

        image_name = image_path + '/density_field_with_box_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created density field with box")

        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 0

        image_name = image_path + '/density_field_without_box_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created density field without box")

        pv.SetActiveView(None)

        pv.Delete(slice_view_two)
        del slice_view_two
        pv.Delete(cell_data_slice_view_two)
        del cell_data_slice_view_two

    ##--- Create orbit image. ---##

    orbit_camera = get_orbit_position(
        timestep_counter,
        timestep_list,
        number_of_timesteps)
    view_one.CameraPosition = orbit_camera
    print("Creating image for orbit.")

    image_name = image_path + '/orbit_' + str(timestep_tag) + '.png'

    pv.SetActiveView(view_one)

    pv.SaveScreenshot(image_name, view_one, SaveAllViews=0,
                      ImageResolution=[1920, 1080],
                      FontScaling='Scale fonts proportionally',
                      SeparatorWidth=1,
                      SeparatorColor=[0.937, 0.922, 0.906],
                      OverrideColorPalette='DefaultBackground',
                      StereoMode='No change',
                      TransparentBackground=0,
                      CompressionLevel='5')
    print("Created orbit image.")

    pv.SetActiveView(None)

    # === Axial Slices ===

    axis_list = np.linspace(1, 3, 3)

    # Loop over all three axes ( x,y,z ).
    for k in axis_list:
        number_of_axis = axis_list[int(k) - 1]

        # x-axis
        if number_of_axis == 1:
            view_vector = [1.0, 0.0, 0.0]
            origin_1 = [0.25, 0.5, 0.5]
            origin_2 = [0.50, 0.5, 0.5]
            origin_3 = [0.75, 0.5, 0.5]
            cam_1 = [6.00, 0.5, 0.5]
            cam_2 = [6.25, 0.5, 0.5]
            cam_3 = [6.50, 0.5, 0.5]
            cam_view_up = [0, 0, 1]
            view_name_tag = 'x_axis'
            render_face = 1
        # y-axis
        elif number_of_axis == 2:
            view_vector = [0.0, 1.0, 0.0]
            origin_1 = [0.5, 0.25, 0.5]
            origin_2 = [0.5, 0.50, 0.5]
            origin_3 = [0.5, 0.75, 0.5]
            cam_1 = [0.5, -5.50, 0.5]
            cam_2 = [0.5, -5.25, 0.5]
            cam_3 = [0.5, -5.00, 0.5]
            cam_view_up = [0, 0, 1]
            view_name_tag = 'y_axis'
            render_face = 16
        # z-axis
        else:
            view_vector = [0.0, 0.0, 1.0]
            origin_1 = [0.5, 0.5, 0.25]
            origin_2 = [0.5, 0.5, 0.50]
            origin_3 = [0.5, 0.5, 0.75]
            cam_1 = [0.5, 0.5, 6.00]
            cam_2 = [0.5, 0.5, 6.25]
            cam_3 = [0.5, 0.5, 6.50]
            cam_view_up = [0, 1, 0]
            view_name_tag = 'z_axis'
            render_face = 4

        ##--- Create Slices. ---##
        # Create slices for view one.
        slice_view_one = pv.Slice(Input=contour_data)
        slice_view_one.SliceType = 'Plane'
        slice_view_one.SliceOffsetValues = [0.0]
        slice_view_one.SliceType.Origin = origin_1
        slice_view_one.SliceType.Normal = view_vector

        cell_data_slice_view_one = pv.Slice(Input=cell_data)
        cell_data_slice_view_one.SliceType = 'Plane'
        cell_data_slice_view_one.SliceOffsetValues = [0.0]
        cell_data_slice_view_one.SliceType.Origin = origin_1
        cell_data_slice_view_one.SliceType.Normal = view_vector

        # Create slices for view two.
        slice_view_two = pv.Slice(Input=contour_data)
        slice_view_two.SliceType = 'Plane'
        slice_view_two.SliceOffsetValues = [0.0]
        slice_view_two.SliceType.Origin = origin_2
        slice_view_two.SliceType.Normal = view_vector

        cell_data_slice_view_two = pv.Slice(Input=cell_data)
        cell_data_slice_view_two.SliceType = 'Plane'
        cell_data_slice_view_two.SliceOffsetValues = [0.0]
        cell_data_slice_view_two.SliceType.Origin = origin_2
        cell_data_slice_view_two.SliceType.Normal = view_vector

        # Create slices for view three.
        slice_view_three = pv.Slice(Input=contour_data)
        slice_view_three.SliceType = 'Plane'
        slice_view_three.SliceOffsetValues = [0.0]
        slice_view_three.SliceType.Origin = origin_3
        slice_view_three.SliceType.Normal = view_vector

        cell_data_slice_view_three = pv.Slice(Input=cell_data)
        cell_data_slice_view_three.SliceType = 'Plane'
        cell_data_slice_view_three.SliceOffsetValues = [0.0]
        cell_data_slice_view_three.SliceType.Origin = origin_3
        cell_data_slice_view_three.SliceType.Normal = view_vector

        ##--- Create and split layout. ---##
        layout_2 = pv.CreateLayout('Axial Slices')
        pv.SetActiveView(None)

        layout_2.SplitHorizontal(0, 0.66)
        layout_2.SplitHorizontal(1, 0.5)
        pv.SetActiveView(None)

        ##--- Create and setup views. ---##
        # Create first view.
        view_one = pv.CreateView('RenderView')
        pv.AssignViewToLayout(view=view_one, layout=layout_2, hint=1)

        slice_view_one_display = pv.Show(slice_view_one, view_one)
        pv.ColorBy(slice_view_one_display, ('POINTS', 'pressure'))
        slice_view_one_display.SetScalarBarVisibility(view_one, True)
        slice_view_one_display.SetRepresentationType('Wireframe')
        slice_view_one_display.LineWidth = line_width

        pressure_LUT = pv.GetColorTransferFunction('pressure')
        pressure_LUT.ApplyPreset(color_scale, True)
        pressure_LUT.RescaleTransferFunction(
            pressure_range[0], pressure_range[1])
        pressure_LUT_color_bar_one = pv.GetScalarBar(pressure_LUT, view_one)
        pressure_LUT_color_bar_one.TitleFontSize = 5
        pressure_LUT_color_bar_one.LabelFontSize = 5
        pressure_LUT_color_bar_one.ScalarBarThickness = 5
        pressure_LUT_color_bar_one.ScalarBarLength = 0.15
        pressure_LUT_color_bar_one.Title = 'pressure'
        pressure_LUT_color_bar_one.ComponentTitle = ''

        cell_data_one_display = pv.Show(cell_data_slice_view_one, view_one)
        cell_data_one_display.SetScalarBarVisibility(view_one, False)
        cell_data_one_display.DataAxesGrid.GridAxesVisibility = 1
        cell_data_one_display.DataAxesGrid.FacesToRender = render_face
        cell_data_one_display.Opacity = 0

        pv.SetActiveView(None)

        # Create second view.
        view_two = pv.CreateView('RenderView')
        pv.AssignViewToLayout(view=view_two, layout=layout_2, hint=1)

        slice_view_two_display = pv.Show(slice_view_two, view_two)
        pv.ColorBy(slice_view_two_display, ('POINTS', 'pressure'))
        slice_view_two_display.SetScalarBarVisibility(view_two, True)
        slice_view_two_display.SetRepresentationType('Wireframe')
        slice_view_two_display.LineWidth = line_width

        pressure_LUT.RescaleTransferFunction(
            pressure_range[0], pressure_range[1])
        pressure_LUT_color_bar_two = pv.GetScalarBar(pressure_LUT, view_two)
        pressure_LUT_color_bar_two.TitleFontSize = 5
        pressure_LUT_color_bar_two.LabelFontSize = 5
        pressure_LUT_color_bar_two.ScalarBarThickness = 5
        pressure_LUT_color_bar_two.ScalarBarLength = 0.15
        pressure_LUT_color_bar_two.Title = 'pressure'
        pressure_LUT_color_bar_two.ComponentTitle = ''

        cell_data_two_display = pv.Show(cell_data_slice_view_two, view_two)
        cell_data_two_display.SetScalarBarVisibility(view_two, False)
        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 1
        cell_data_two_display.DataAxesGrid.FacesToRender = render_face
        cell_data_two_display.Opacity = 0

        pv.SetActiveView(None)

        # Create third view.
        view_three = pv.CreateView('RenderView')
        pv.AssignViewToLayout(view=view_three, layout=layout_2, hint=1)

        slice_view_three_display = pv.Show(slice_view_three, view_three)
        pv.ColorBy(slice_view_three_display, ('POINTS', 'pressure'))
        slice_view_three_display.SetScalarBarVisibility(view_three, True)
        slice_view_three_display.SetRepresentationType('Wireframe')
        slice_view_three_display.LineWidth = line_width

        pressure_LUT.RescaleTransferFunction(
            pressure_range[0], pressure_range[1])
        pressure_LUT_color_bar_three = pv.GetScalarBar(
            pressure_LUT, view_three)
        pressure_LUT_color_bar_three.TitleFontSize = 5
        pressure_LUT_color_bar_three.LabelFontSize = 5
        pressure_LUT_color_bar_three.ScalarBarThickness = 5
        pressure_LUT_color_bar_three.ScalarBarLength = 0.15
        pressure_LUT_color_bar_three.Title = 'pressure'
        pressure_LUT_color_bar_three.ComponentTitle = ''

        cell_data_three_display = pv.Show(
            cell_data_slice_view_three, view_three)
        cell_data_three_display.SetScalarBarVisibility(view_three, False)
        cell_data_three_display.DataAxesGrid.GridAxesVisibility = 1
        cell_data_three_display.DataAxesGrid.FacesToRender = render_face
        cell_data_three_display.Opacity = 0

        pv.SetActiveView(None)

        # Set camera position for view_one
        view_one.CameraPosition = cam_1
        view_one.CameraFocalPoint = [0.5, 0.5, 0.5]
        view_one.CameraViewUp = cam_view_up
        view_one.CameraParallelScale = 0.8660254037844386

        # Set camera position for view_two
        view_two.CameraPosition = cam_2
        view_two.CameraFocalPoint = [0.5, 0.5, 0.5]
        view_two.CameraViewUp = cam_view_up
        view_two.CameraParallelScale = 0.8660254037844386

        # Set camera position for view_three
        view_three.CameraPosition = cam_3
        view_three.CameraFocalPoint = [0.5, 0.5, 0.5]
        view_three.CameraViewUp = cam_view_up
        view_three.CameraParallelScale = 0.8660254037844386

        print("Creating images for " + str(view_name_tag) + '.')

        if timestep_counter < 10:
            timestep_tag = "00" + str(timestep_counter)
        elif timestep_counter >= 10 and timestep_counter < 100:
            timestep_tag = "0" + str(timestep_counter)
        else:
            timestep_tag = str(timestep_counter)

        image_name = image_path + '/slices_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, layout_2, SaveAllViews=1,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=0,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created slices for " + view_name_tag)

        pv.SetActiveView(None)

        pv.Delete(slice_view_one)
        del slice_view_one
        pv.Delete(cell_data_slice_view_one)
        del cell_data_slice_view_one
        pv.Delete(slice_view_two)
        del slice_view_two
        pv.Delete(cell_data_slice_view_two)
        del cell_data_slice_view_two
        pv.Delete(slice_view_three)
        del slice_view_three
        pv.Delete(cell_data_slice_view_three)
        del cell_data_slice_view_three
        pv.Delete(layout_2)
        del layout_2
        pv.Delete(view_one)
        del view_one
        pv.Delete(view_two)
        del view_two
        pv.Delete(view_three)
        del view_three

    pv.Delete(cell_data)
    del cell_data
    pv.Delete(contour_data)
    del contour_data


def main():
    create_pngs_loop_over_time()
    print("Done")


if __name__ == "__main__":
    main()
