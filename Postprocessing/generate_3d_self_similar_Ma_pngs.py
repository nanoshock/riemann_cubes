import paraview.simple as pv
import argparse as ag
import numpy as np
import glob
import os

# This script has to be run in a simulation folder, so that the relative
# path to the timestep data is 'domain/data*.xdmf'.

# This function returns the last 13 characters of a string.


def get_last_13_chars(x):
    return x[-13:]

# This function returns a list with all timestep files.


def get_files():
    files = glob.glob('domain/data*.xdmf')
    files = sorted(files, key=get_last_13_chars)
    print("Found " + str(np.size(files)) + " timestep files.")
    return files

# This function returns a list with all timesteps.


def extract_timestep_list(case_name):
    data = pv.XDMFReader(FileNames=[case_name])
    data.CellArrayStatus = []
    timesteps = data.TimestepValues

    return timesteps

# This function opens a timestep file.


def open_data(filename):
    data = pv.XDMFReader(FileNames=[filename])
    data.CellArrayStatus = ['density', 'pressure', 'velocity']

    return data

# This function returns the velocity range.


def get_temporal_velocity_range(data):
    min_max = data.PointData.GetArray('velocity').GetRange()

    return min_max

# This function returns the self similar Mach number range


def get_self_similar_Ma_range( data ):
    min_max = data.PointData.GetArray('self_similar_Ma').GetRange()

    return min_max

# This function generates density contours.


def generate_density_contours(input_object, density_range):

    contour = pv.Contour(Input=input_object)
    contour.ContourBy = ['POINTS', 'density']

    print("Density range for contours = " + str(density_range))

    contour.Isosurfaces = np.linspace(
        start=density_range[0], stop=density_range[1], num=25)

    return contour

# This function generates self similar Mach number contours in subsonic regions.


def generate_subsonic_contours( input_object ):

    contour = pv.Contour(Input=input_object)
    contour.ContourBy = ['POINTS', 'self_similar_Ma']
    contour.Isosurfaces = [0.1, 0.2, 0.30000000000000004, 0.4, 0.5, 0.6, 0.7000000000000001, 0.8, 0.9]
    contour.PointMergeMethod = 'Uniform Binning'

    return contour

# This function generates self similar Mach sonic line


def generate_sonic_line( input_object ):

    contour = pv.Contour(Input=input_object)
    contour.ContourBy = ['POINTS', 'self_similar_Ma']
    contour.Isosurfaces = [ 1.0 ]
    contour.PointMergeMethod = 'Uniform Binning'

    return contour

# This function generates self similar Mach number contours in supersonic regions.


def generate_supersonic_contours( input_object ):

    contour = pv.Contour(Input=input_object)
    contour.ContourBy = ['POINTS', 'self_similar_Ma']
    contour.Isosurfaces = [1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]
    contour.PointMergeMethod = 'Uniform Binning'

    return contour

# This function calculates the selfsimilar Mach number field


def generate_self_similar_Ma_field(point_data, current_time):

    point_data = translate_field(point_data)
    point_data = scale_field( point_data, current_time)

    self_similar_Ma_field = pv.Calculator(Input=point_data)
    self_similar_Ma_field.ResultArrayName = 'self_similar_Ma'
    self_similar_Ma_field.Function = 'sqrt(((' + str(current_time) + '*velocity_X-coordsX)^2+(' + str(current_time) + '*velocity_Y-coordsY)^2+(' + str(current_time) + '*velocity_Z-coordsZ)^2)/(1.4*pressure/density))'

    return self_similar_Ma_field

# This function transforms the cell data field which is necessary for view two of the diagonal images


def transform_secondary_field(cell_data, current_time):
    cell_data = translate_field(cell_data)
    cell_data = scale_field( cell_data, current_time)

    return cell_data

# This function moves a field to the middle


def translate_field( input_object ):
    tf = pv.Transform(Input=input_object)
    tf.TransformAllInputVectors = 1
    tf.Transform.Translate = [-0.5, -0.5, -0.5]

    return tf

# This function scales the field


def scale_field( input_object, current_time ):
    tf = pv.Transform(Input=input_object)
    scale_factor = 1/current_time
    tf.TransformAllInputVectors = 1
    tf.Transform.Scale = [scale_factor, scale_factor, scale_factor]

    return tf

# This function calculates pseudo-velocities according to Schulz-Rinne 1993


def Calc_pseudo_vel( input_object, direction, current_time ):
    calculator = pv.Calculator(Input=input_object)
    calculator.ResultArrayName = 'pseudo_vel_' + direction
    calculator.Function = str(current_time) + '*velocity_' + direction + '-coords' + direction

    return calculator

# This function calculates the local Mach number


def Calc_local_Ma( input_object ):
    calculator = pv.Calculator(Input=input_object)
    calculator.ResultArrayName = 'local_Ma'
    calculator.Function = 'sqrt(1.4*pressure/density)'

    return calculator

# This functions returns color settings for the images with transparent background
# 1: all black, thicker sonic line
# 2: blueorange colorstyle
# 3: grey, black
# 4: yellow, blue
# 5: brown, green
# 6: red, blue


def get_color_settings( color_style, line_width ):
    if color_style == 1:
        subsonic_color = [ 0.0, 0.0, 0.0 ]
        sonic_color = [ 0.0, 0.0, 0.0 ]
        supersonic_color = [ 0.0, 0.0, 0.0 ]
        subsonic_line_width = line_width
        sonic_line_width = line_width+5
        supersonic_line_width = line_width
    elif color_style == 2:
        subsonic_color = [ 0.18, 0.47, 0.70 ]
        sonic_color = [ 0.0, 0.0, 0.0 ]
        supersonic_color = [ 0.77, 0.41, 0.05 ]
        subsonic_line_width = line_width
        sonic_line_width = line_width+1
        supersonic_line_width = line_width
    elif color_style == 3:
        subsonic_color = [ 0.5, 0.5, 0.5 ]
        sonic_color = [ 0.5, 0.5, 0.5 ]
        supersonic_color = [ 0.0, 0.0, 0.0 ]
        subsonic_line_width = line_width
        sonic_line_width = line_width
        supersonic_line_width = line_width
    elif color_style == 4:
        subsonic_color = [ 0.90, 1.0, 0.56 ]
        sonic_color = [ 0, 0, 0 ]
        supersonic_color = [ 0.20, 0.11, 0.53 ]
        subsonic_line_width = line_width
        sonic_line_width = line_width
        supersonic_line_width = line_width
    elif color_style == 5:
        subsonic_color = [ 0.63, 0.40, 0.08 ]
        sonic_color = [ 0.0, 0.0, 0.0 ]
        supersonic_color = [ 0.09, 0.48, 0.45 ]
        subsonic_line_width = line_width
        sonic_line_width = line_width
        supersonic_line_width = line_width
    elif color_style == 6:
        subsonic_color = [ 0.75, 0.21, 0.23 ]
        sonic_color = [ 0.0, 0.0, 0.0 ]
        supersonic_color = [ 0.18, 0.47, 0.70 ]
        subsonic_line_width = line_width
        sonic_line_width = line_width
        supersonic_line_width = line_width
    else:
        print( "Unknown color style" )

    return [ subsonic_color, sonic_color, supersonic_color, subsonic_line_width, sonic_line_width, supersonic_line_width ]

# This function loops over all timesteps and calls the PNG creating
# function for every timestep.


def create_pngs_loop_over_time(case_name, evaluated_timestep):

    timestep_files = get_files()
    timestep_counter = evaluated_timestep-1

    timestep_list = extract_timestep_list(case_name)

    # This is the relative path to the directory, where the created pngs will
    # be stored.
    image_path = 'self_similar_Ma_Images'

    try:
        os.mkdir(image_path)
    except BaseException:
        print(
            "Creation of directory %s failed. Maybe this directory already exists." %
            image_path)
    else:
        print("Successfully created the directory %s." % image_path)

    timestep_files = timestep_files[evaluated_timestep-1:evaluated_timestep]

    current_time = timestep_list[evaluated_timestep-1]

    for current_file in timestep_files:
        timestep_counter += 1
        print("Start creating images for timestep " + str(timestep_counter) +
              ". Time = " + str(timestep_list[timestep_counter - 1]) + " s")
        create_pngs_of_single_timestep(
            timestep_counter,
            current_file,
            current_time,
            image_path)


# This function creates the pngs of a single timestep.
def create_pngs_of_single_timestep(timestep_counter, current_file, current_time, image_path):

    # Set the line width.
    line_width = 2.0

    # Choose color settings for images with transaprent background
    # 1: all black, thicker sonic line
    # 2: blueorange colorstyle
    # 3: grey, black
    # 4: yellow, blue
    # 5: brown, green
    # 6: red, blue
    color_style = 2
    [ subsonic_color, sonic_color, supersonic_color, subsonic_line_width, sonic_line_width, supersonic_line_width ] = get_color_settings( color_style, line_width )

    cell_data = open_data(current_file)

    point_data = pv.CellDatatoPointData(Input=cell_data)
    point_data.CellDataArraytoprocess = ['density', 'pressure', 'velocity']

    self_similar_Ma_field = generate_self_similar_Ma_field(point_data, current_time)
    cell_data = transform_secondary_field(cell_data, current_time)

    self_similar_Ma_range = get_self_similar_Ma_range( self_similar_Ma_field )
    print("self similar Mach number range = " + str(self_similar_Ma_range))

    subsonic_contour_data = generate_subsonic_contours(self_similar_Ma_field)
    sonic_line_data = generate_sonic_line(self_similar_Ma_field)
    supersonic_contour_data = generate_supersonic_contours(self_similar_Ma_field)

    pv.Delete(point_data)
    del point_data

    # === Diagonal Images ===

    view_list = np.linspace(1, 8, 8)

    # Loop over all diagonal viewing angles.
    for k in view_list:
        number_of_view = view_list[int(k) - 1]
        cam_distance = 3/current_time

        if number_of_view == 1:
            view_vector = [1.0, 1.0, 1.0]
            position_of_camera = [cam_distance, cam_distance, cam_distance]
            view_name_tag = '+1_+1_+1'
        elif number_of_view == 2:
            view_vector = [1.0, -1.0, 1.0]
            position_of_camera = [cam_distance, -cam_distance, cam_distance]
            view_name_tag = '+1_-1_+1'
        elif number_of_view == 3:
            view_vector = [-1.0, 1.0, 1.0]
            position_of_camera = [-cam_distance, cam_distance, cam_distance]
            view_name_tag = '-1_+1_+1'
        elif number_of_view == 4:
            view_vector = [-1.0, -1.0, 1.0]
            position_of_camera = [-cam_distance, -cam_distance, cam_distance]
            view_name_tag = '-1_-1_+1'
        elif number_of_view == 5:
            view_vector = [1.0, 1.0, -1.0]
            position_of_camera = [cam_distance, cam_distance, -cam_distance]
            view_name_tag = '+1_+1_-1'
        elif number_of_view == 6:
            view_vector = [1.0, -1.0, -1.0]
            position_of_camera = [cam_distance, -cam_distance, -cam_distance]
            view_name_tag = '+1_-1_-1'
        elif number_of_view == 7:
            view_vector = [-1.0, 1.0, -1.0]
            position_of_camera = [-cam_distance, cam_distance, -cam_distance]
            view_name_tag = '-1_+1_-1'
        else:
            view_vector = [-1.0, -1.0, -1.0]
            position_of_camera = [-cam_distance, -cam_distance, -cam_distance]
            view_name_tag = '-1_-1_-1'

        ##--- Create Slices ---##
        subsonic_slice = pv.Slice(Input=subsonic_contour_data)
        subsonic_slice.SliceType = 'Plane'
        subsonic_slice.SliceOffsetValues = [0.0]
        subsonic_slice.SliceType.Origin = [0.0, 0.0, 0.0]
        subsonic_slice.SliceType.Normal = view_vector

        sonic_slice = pv.Slice(Input=sonic_line_data)
        sonic_slice.SliceType = 'Plane'
        sonic_slice.SliceOffsetValues = [0.0]
        sonic_slice.SliceType.Origin = [0.0, 0.0, 0.0]
        sonic_slice.SliceType.Normal = view_vector

        supersonic_slice = pv.Slice(Input=supersonic_contour_data)
        supersonic_slice.SliceType = 'Plane'
        supersonic_slice.SliceOffsetValues = [0.0]
        supersonic_slice.SliceType.Origin = [0.0, 0.0, 0.0]
        supersonic_slice.SliceType.Normal = view_vector

        cell_data_slice = pv.Slice(Input=cell_data)
        cell_data_slice.SliceType = 'Plane'
        cell_data_slice.SliceOffsetValues = [0.0]
        cell_data_slice.SliceType.Origin = [0.0, 0.0, 0.0]
        cell_data_slice.SliceType.Normal = view_vector


        ##--- Create and split layout. ---##
        layout_2 = pv.CreateLayout('Diagonal Images')
        pv.SetActiveView(None)

        ##--- Create and setup views. ---##
        # Create view.
        view_two = pv.CreateView('RenderView')
        pv.AssignViewToLayout(view=view_two, layout=layout_2, hint=1)

        subsonic_slice_display = pv.Show(subsonic_slice, view_two)
        subsonic_slice_display.SetRepresentationType('Wireframe')
        subsonic_slice_display.AmbientColor = subsonic_color
        subsonic_slice_display.ColorArrayName = ['POINTS', '']
        subsonic_slice_display.DiffuseColor = subsonic_color
        subsonic_slice_display.LineWidth = subsonic_line_width

        sonic_slice_display = pv.Show(sonic_slice, view_two)
        sonic_slice_display.SetRepresentationType('Wireframe')
        sonic_slice_display.AmbientColor = sonic_color
        sonic_slice_display.ColorArrayName = ['POINTS', '']
        sonic_slice_display.DiffuseColor = sonic_color
        sonic_slice_display.LineWidth = sonic_line_width

        supersonic_slice_display = pv.Show(supersonic_slice, view_two)
        supersonic_slice_display.SetRepresentationType('Wireframe')
        supersonic_slice_display.AmbientColor = supersonic_color
        supersonic_slice_display.ColorArrayName = ['POINTS', '']
        supersonic_slice_display.DiffuseColor = supersonic_color
        supersonic_slice_display.LineWidth = supersonic_line_width

        cell_data_two_display = pv.Show(cell_data_slice, view_two)
        cell_data_two_display.SetScalarBarVisibility(view_two, False)
        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 1
        cell_data_two_display.DataAxesGrid.XTitle = '$\\xi$'
        cell_data_two_display.DataAxesGrid.YTitle = '$\\eta$'
        cell_data_two_display.DataAxesGrid.ZTitle = '$\\zeta$'
        cell_data_two_display.DataAxesGrid.XTitleFontSize = 20
        cell_data_two_display.DataAxesGrid.YTitleFontSize = 20
        cell_data_two_display.DataAxesGrid.ZTitleFontSize = 20
        cell_data_two_display.DataAxesGrid.ShowTicks = 0
        cell_data_two_display.DataAxesGrid.XLabelOpacity = 0.0
        cell_data_two_display.DataAxesGrid.YLabelOpacity = 0.0
        cell_data_two_display.DataAxesGrid.ZLabelOpacity = 0.0

        cell_data_two_display.Opacity = 0

        pv.SetActiveView(None)

        # Set camera position for view_two
        view_two.CameraPosition = position_of_camera
        view_two.CameraFocalPoint = [0.0, 0.0, 0.0]
        view_two.CameraViewUp = [0.0, 0.0, 1.0]
        view_two.CameraParallelScale = 0.8660254037844386

        print("Creating images for view " + str(view_name_tag) + '.')

        if timestep_counter < 10:
            timestep_tag = "00" + str(timestep_counter)
        elif timestep_counter >= 10 and timestep_counter < 100:
            timestep_tag = "0" + str(timestep_counter)
        else:
            timestep_tag = str(timestep_counter)

        pv.SetActiveView(view_two)

        image_name = image_path + '/slice_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=0,
                          CompressionLevel='5')
        print("Created slice")

        # Create images with transparent backround
        view_two.OrientationAxesVisibility = 0
        cell_data_two_display.DataAxesGrid.XTitleColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.YTitleColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.GridColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.XLabelColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.YLabelColor = [0.0, 0.0, 0.0]
        cell_data_two_display.DataAxesGrid.ZLabelColor = [0.0, 0.0, 0.0]

        image_name = image_path + '/slice_with_box_transparent_background_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created slice with box and transparent background")

        cell_data_two_display.DataAxesGrid.GridAxesVisibility = 0

        image_name = image_path + '/slice_without_box_transparent_background_' + \
            view_name_tag + '_' + str(timestep_tag) + '.png'

        pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                          ImageResolution=[1920, 1080],
                          FontScaling='Scale fonts proportionally',
                          SeparatorWidth=1,
                          SeparatorColor=[0.937, 0.922, 0.906],
                          OverrideColorPalette='DefaultBackground',
                          StereoMode='No change',
                          TransparentBackground=1,
                          CompressionLevel='5')
        print("Created slice without box and transparent background")

        pv.SetActiveView(None)

        pv.Delete(subsonic_slice)
        del subsonic_slice
        pv.Delete(supersonic_slice)
        del supersonic_slice
        pv.Delete(cell_data_slice)
        del cell_data_slice

    pv.Delete(cell_data)
    del cell_data
    pv.Delete(subsonic_contour_data)
    del subsonic_contour_data
    pv.Delete(supersonic_contour_data)
    del supersonic_contour_data


def main( arguments ):
    case_name = "domain/" + arguments.case + ".xdmf"
    evaluated_timestep = int(arguments.timestep)
    print( "Using timestep " + str(evaluated_timestep) )
    create_pngs_loop_over_time(case_name, evaluated_timestep)
    print("Done")


def ParseArguments() :
   parser = ag.ArgumentParser()
   parser.add_argument( "--case", help = "name of the case")
   parser.add_argument( "--timestep", help = "the timestep which is used to generate the self similar Mach number plots" )
   arguments = parser.parse_args()
   return arguments


if __name__ == "__main__":
    arguments = ParseArguments()
    main( arguments )
