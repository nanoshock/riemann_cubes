import paraview.simple as pv
import argparse as ag
import numpy as np
import glob
import os
import csv
from copy import deepcopy

# This script has to be run in a simulation folder, so that the relative
# path to the timestep data is 'domain/data*.xdmf'.

# This function returns the last 13 characters of a string.


def get_last_13_chars(x):
    return x[-13:]

# This function removes the last 5 characters of a string.


def remove_last_5_chars( x ) :
    return x[:-5]

# This function returns a list with all timestep files.


def get_files():
    files = glob.glob('domain/data*.xdmf')
    files = sorted(files, key=get_last_13_chars)
    print("Found " + str(np.size(files)) + " timestep files.")
    return files

# This function returns a list with all timesteps.


def extract_timestep_list(files, steps):
    iterator_list = np.linspace(0, steps - 1, steps)

    for k in iterator_list:
        time = files[int(k)]
        time = get_last_13_chars( time )
        time = remove_last_5_chars( time )
        files[int(k)] = float(time)

    return files

# This function creates a file with all timesteps


def extract_timesteps( files, steps ) :
    iterator_list = np.linspace(0, steps-1, steps)
    timesteps = np.zeros(( steps, 2 ))

    for k in iterator_list :
        time = files[ int(k) ]
        time = get_last_13_chars( time )
        time = remove_last_5_chars( time )

        timesteps[ int(k), int(0) ] = int( iterator_list[ int(k) ] + 1 )
        timesteps[ int(k), int(1) ] = float(time)

    with open( 'list_of_timesteps.csv', 'w', newline='') as csv_file :
        writer = csv.writer( csv_file )
        writer.writerows( timesteps )

    return 0

# This function opens a timestep file.


def open_data(filename):
    data = pv.XDMFReader(FileNames=[filename])
    data.CellArrayStatus = ['density', 'pressure']
    return data

# This function returns the pressure range.


def get_pressure_range(data_1, data_2):
    cell_data_1 = open_data(data_1)
    cell_data_2 = open_data(data_2)

    min_max_1 = cell_data_1.CellData.GetArray('pressure').GetRange()
    min_max_2 = cell_data_2.CellData.GetArray('pressure').GetRange()

    min_max = [min(min_max_1[0], min_max_2[0]),
               max(min_max_1[1], min_max_2[1])]

    pv.Delete(cell_data_1)
    pv.Delete(cell_data_2)
    del cell_data_1
    del cell_data_2

    return min_max

# This function returns the density range.


def get_density_range(data_1, data_2):
    cell_data_1 = open_data(data_1)
    cell_data_2 = open_data(data_2)

    min_max_1 = cell_data_1.CellData.GetArray('density').GetRange()
    min_max_2 = cell_data_2.CellData.GetArray('density').GetRange()

    min_max = [min(min_max_1[0], min_max_2[0]),
               max(min_max_1[1], min_max_2[1])]

    pv.Delete(cell_data_1)
    pv.Delete(cell_data_2)
    del cell_data_1
    del cell_data_2

    return min_max

# This function writes a file containing the density range


def write_density_and_pressure_range(density_range, pressure_range):
    with open( 'density_and_pressure_range.csv', 'w', newline='') as csv_file :
        writer = csv.writer( csv_file )
        writer.writerow(["density range", density_range])
        writer.writerow(["pressure range", pressure_range])

    return 0

# This function generates density contours.


def generate_density_contours(input_object, density_range):

    contour = pv.Contour(Input=input_object)
    contour.ContourBy = ['POINTS', 'density']

    print("Density range for contours = " + str(density_range))

    contour.Isosurfaces = np.linspace(
        start=density_range[0], stop=density_range[1], num=25)

    return contour


# This function loops over all timesteps and calls the PNG creating
# function for every timestep.
def create_pngs_loop_over_time():

    timestep_files = get_files()
    timestep_counter = 0

    number_of_timesteps = np.size(timestep_files)
    timestep_list = deepcopy(timestep_files)
    timestep_list = extract_timestep_list(timestep_list, number_of_timesteps)

    extract_timesteps_files = deepcopy(timestep_files)
    extract_timesteps( extract_timesteps_files, number_of_timesteps )

    # This is the relative path to the directory, where the created pngs will
    # be stored.
    image_path = 'Images'

    try:
        os.mkdir(image_path)
    except BaseException:
        print(
            "Creation of directory %s failed. Maybe this directory already exists." %
            image_path)
    else:
        print("Successfully created the directory %s." % image_path)

    pressure_range = get_pressure_range(
        timestep_files[0], timestep_files[np.size(timestep_files) - 1])
    density_range = get_density_range(
        timestep_files[0], timestep_files[np.size(timestep_files) - 1])

    write_density_and_pressure_range(density_range, pressure_range)

    for current_file in timestep_files:
        timestep_counter += 1
        print("Start creating images for timestep " + str(timestep_counter) +
              ". Time = " + str(timestep_list[timestep_counter - 1]) + " s")
        create_pngs_of_single_timestep(
            timestep_counter,
            current_file,
            pressure_range,
            density_range,
            image_path)

# This function creates the pngs of a single timestep.


def create_pngs_of_single_timestep(
        timestep_counter, current_file, pressure_range, density_range, image_path):

    # Set the opacity.
    opacity = 1

    # Set the line width.
    line_width = 2.0
    line_width_2 = 1.0

    # Import a file with color scales. Depending on the directory of the file,
    # the path must be adapted.
    pv.ImportPresets(filename='../own_scale.xml')

    # Choose a color scale.
    color_scale = 'blueorange'
    contour_color = [1.0, 1.0, 1.0]

    cell_data = open_data(current_file)

    point_data = pv.CellDatatoPointData(Input=cell_data)
    point_data.CellDataArraytoprocess = ['density', 'pressure']

    pv.Delete(cell_data)
    del cell_data

    contour_data = generate_density_contours(point_data, density_range)

    # === Images ===

    ##--- Create and split layout. ---##
    layout_2 = pv.CreateLayout('Images')
    pv.SetActiveView(None)

    ##--- Create and setup views. ---##
    # Create first view.
    view_one = pv.CreateView('RenderView')
    view_one.InteractionMode = '2D'
    pv.AssignViewToLayout(view=view_one, layout=layout_2, hint=1)
    point_data_display = pv.Show(point_data, view_one)
    pv.ColorBy(point_data_display, ('POINTS', 'density'))
    point_data_display.DataAxesGrid.GridAxesVisibility = 1
    point_data_display.DataAxesGrid.FacesToRender = 4
    point_data_display.SetScalarBarVisibility(view_one, True)
    density_LUT = pv.GetColorTransferFunction('density')
    density_LUT.ApplyPreset(color_scale, True)
    density_LUT.RescaleTransferFunction(density_range[0], density_range[1])
    density_LUT_color_bar_one = pv.GetScalarBar(density_LUT, view_one)
    density_LUT_color_bar_one.TitleFontSize = 5
    density_LUT_color_bar_one.LabelFontSize = 5
    density_LUT_color_bar_one.ScalarBarThickness = 5
    density_LUT_color_bar_one.ScalarBarLength = 0.15
    point_data_display.Opacity = opacity

    pv.SetActiveView(None)

    # Create second view.
    view_two = pv.CreateView('RenderView')
    view_two.InteractionMode = '2D'
    pv.AssignViewToLayout(view=view_two, layout=layout_2, hint=1)

    contour_data_display = pv.Show(contour_data, view_two)
    pv.ColorBy(contour_data_display, ('POINTS', 'pressure'))
    contour_data_display.SetScalarBarVisibility(view_two, True)
    contour_data_display.SetRepresentationType('Wireframe')
    contour_data_display.LineWidth = line_width

    pressure_LUT = pv.GetColorTransferFunction('pressure')
    pressure_LUT.ApplyPreset(color_scale, True)
    pressure_LUT.RescaleTransferFunction(pressure_range[0], pressure_range[1])
    pressure_LUT_color_bar_two = pv.GetScalarBar(pressure_LUT, view_two)
    pressure_LUT_color_bar_two.TitleFontSize = 5
    pressure_LUT_color_bar_two.LabelFontSize = 5
    pressure_LUT_color_bar_two.ScalarBarThickness = 5
    pressure_LUT_color_bar_two.ScalarBarLength = 0.15
    pressure_LUT_color_bar_two.Title = 'pressure'
    pressure_LUT_color_bar_two.ComponentTitle = ''

    clip_two_display = pv.Show(point_data, view_two)
    clip_two_display.SetScalarBarVisibility(view_two, False)
    clip_two_display.DataAxesGrid.GridAxesVisibility = 1
    clip_two_display.DataAxesGrid.FacesToRender = 4
    clip_two_display.Opacity = 0

    pv.SetActiveView(None)

    # Set the camera plosition for view_two.
    view_two.CameraPosition = [0.5, 0.5, 3.0]
    view_two.CameraFocalPoint = [0.5, 0.5, 0.5]
    view_two.CameraViewUp = [0.0, 1.0, 0.0]
    view_two.CameraParallelScale = 0.8660254037844386

    # Set the camera plosition for view_one
    view_one.CameraPosition = [0.5, 0.5, 3.0]
    view_one.CameraFocalPoint = [0.5, 0.5, 0.5]
    view_one.CameraViewUp = [0.0, 1.0, 0.0]
    view_one.CameraParallelScale = 0.8660254037844386

    if timestep_counter < 10:
        timestep_tag = "00" + str(timestep_counter)
    elif timestep_counter >= 10 and timestep_counter < 100:
        timestep_tag = "0" + str(timestep_counter)
    else:
        timestep_tag = str(timestep_counter)

    pv.SetActiveView(view_two)

    image_name = image_path + '/contour_' + \
        color_scale + '_' + str(timestep_tag) + '.png'

    pv.SaveScreenshot(image_name, view_two, SaveAllViews=0,
                      ImageResolution=[1920, 1080],
                      FontScaling='Scale fonts proportionally',
                      SeparatorWidth=1,
                      SeparatorColor=[0.937, 0.922, 0.906],
                      OverrideColorPalette='DefaultBackground',
                      StereoMode='No change',
                      TransparentBackground=0,
                      CompressionLevel='5')
    print("Created Contour")

    pv.SetActiveView(view_one)

    contour_view_one_Display = pv.Show(contour_data, view_one)
    pv.ColorBy(contour_view_one_Display, None)
    contour_view_one_Display.SetRepresentationType('Wireframe')
    contour_view_one_Display.LineWidth = line_width_2
    pv.HideScalarBarIfNotNeeded(pressure_LUT, view_one)
    contour_view_one_Display.AmbientColor = contour_color
    contour_view_one_Display.DiffuseColor = contour_color
    density_LUT.RescaleTransferFunction(density_range[0], density_range[1])

    image_name = image_path + '/allinone_' + \
        color_scale + '_' + str(timestep_tag) + '.png'

    pv.SaveScreenshot(image_name, view_one, SaveAllViews=0,
                      ImageResolution=[1920, 1080],
                      FontScaling='Scale fonts proportionally',
                      SeparatorWidth=1,
                      SeparatorColor=[0.937, 0.922, 0.906],
                      OverrideColorPalette='DefaultBackground',
                      StereoMode='No change',
                      TransparentBackground=0,
                      CompressionLevel='5')
    print("Created All in one")

    pv.Delete(layout_2)
    del layout_2
    pv.Delete(view_one)
    del view_one
    pv.Delete(view_two)
    del view_two

    pv.Delete(point_data)
    del point_data
    pv.Delete(contour_data)
    del contour_data


def main():
    create_pngs_loop_over_time()
    print("Done")


if __name__ == "__main__":
    main()
