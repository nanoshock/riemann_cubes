# Post-Processing
Here, the post-processing routines used to generate the videos and images from the raw simulation data are given.

## Image Creation
We use the scripts in this folder to generate the images in this repository. The scripts are written in python and employ [ParaView](http://www.paraview.org). The scripts are executed by running
```
pvbatch <scriptname>.py
```
from within the simulation result folder. See also the comment in the respective script. Obviously, the script `generate_2d_pngs.py` is substituted for `<scriptname>.py` in the two-dimensional "Square" cases and `generate_3d_pngs.py` for the three-dimensional "Cube" cases, respectively.

## Video creation
The videos are generated from the image output of the ParaView scripts. We use the command 
```
ffmpeg -framerate 5 -pattern_type glob -i '*.png' \
       -vcodec h264 -acodec aac -strict -2 -pix_fmt yuv420p \
       -preset veryslow <output>.mp4
```
to create an MP4 video. With desired name substitution for `<output>.mp4`.

For seemless git integration we convert the MP4 video into OGV format by executing
```
ffmpeg -i input.mp4 -c:v libtheora -q:v 10 -c:a libvorbis -q:a 4 <output>.ogv
```
Again with desired name substitution for `<output>.mp4`.
